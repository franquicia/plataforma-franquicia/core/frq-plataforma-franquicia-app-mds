/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.usuario.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author cescobarh
 */
public class BusquedaUsuario {

    @JsonProperty(value = "numeroEmpleado")
    private Integer numeroEmpleado;

    @JsonProperty(value = "nombre")
    private String nombre;

    public Integer getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(Integer numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "BusquedaUsuario{" + "numeroEmpleado=" + numeroEmpleado + ", nombre=" + nombre + '}';
    }

}
