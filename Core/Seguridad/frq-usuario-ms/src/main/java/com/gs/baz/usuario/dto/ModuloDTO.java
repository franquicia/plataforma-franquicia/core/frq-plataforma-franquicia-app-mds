/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.usuario.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class ModuloDTO {

    @JsonProperty(value = "id_modulo")
    private Integer idModulo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "modulo")
    private String modulo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "estatus")
    private Integer estatus;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "es_web")
    private Boolean esWeb;

    public ModuloDTO() {
    }

    public Integer getIdModulo() {
        return idModulo;
    }

    public void setIdModulo(BigDecimal idModulo) {
        this.idModulo = (idModulo == null ? null : idModulo.intValue());
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }

    public Boolean getEsWeb() {
        return esWeb;
    }

    public void setEsWeb(BigDecimal esWeb) {
        this.esWeb = (esWeb == null ? null : esWeb.intValue() == 1);
    }

    @Override
    public String toString() {
        return "ModuloDTO{" + "idModulo=" + idModulo + ", descripcion=" + descripcion + ", modulo=" + modulo + ", estatus=" + estatus + ", esWeb=" + esWeb + '}';
    }

}
