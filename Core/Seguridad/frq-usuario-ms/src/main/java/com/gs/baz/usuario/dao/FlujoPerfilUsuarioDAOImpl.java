/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.usuario.dao;

/**
 *
 * @author jfernandor
 */
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.usuario.dto.ModuloFlujoDTO;
import com.gs.baz.usuario.dto.FlujoPerfilUsuarioDTO;
import com.gs.baz.usuario.mprs.FlujoPerfilUsuarioRowMapper;
import com.gs.baz.usuario.mprs.ModuloFlujoUsuarioRowMapper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

/**
 * l
 *
 * @author cescobarh
 */
@Component
public class FlujoPerfilUsuarioDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcSelectFinalByUsuario;
    private DefaultJdbcCall jdbcSelectModuloByFlujo;
    private DefaultJdbcCall jdbcSelectAvailable;
    private DefaultJdbcCall jdbcSelectAssociated;
    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();

    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMAUSUA_FLU");
        jdbcInsert.withProcedureName("SPINSAUSUA_FLUJO");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMAUSUA_FLU");
        jdbcSelect.withProcedureName("SPGETAUSUA_FLUJO");
        jdbcSelect.returningResultSet("PA_CDATOS", new FlujoPerfilUsuarioRowMapper(FlujoPerfilUsuarioRowMapper.FlujoPerfilUsuarioType.FLUJO_PERFIL_USUARIO));

        jdbcSelectFinalByUsuario = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectFinalByUsuario.withSchemaName(schema);
        jdbcSelectFinalByUsuario.withCatalogName("PAADMAUSUA_FLU");
        jdbcSelectFinalByUsuario.withProcedureName("SPGETFLUJOXUSU");
        jdbcSelectFinalByUsuario.returningResultSet("PA_CDATOS", new FlujoPerfilUsuarioRowMapper(FlujoPerfilUsuarioRowMapper.FlujoPerfilUsuarioType.FLUJO_PERFIL_USUARIO));

        jdbcSelectModuloByFlujo = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectModuloByFlujo.withSchemaName(schema);
        jdbcSelectModuloByFlujo.withCatalogName("PAADMAUSUA_FLU");
        jdbcSelectModuloByFlujo.withProcedureName("SPGETAFLUJO_MOD_ACC");
        jdbcSelectModuloByFlujo.returningResultSet("PA_CDATOS", new ModuloFlujoUsuarioRowMapper());

        jdbcSelectAssociated = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAssociated.withSchemaName(schema);
        jdbcSelectAssociated.withCatalogName("PAADMAUSUA_FLU");
        jdbcSelectAssociated.withProcedureName("SPGETFLUJOASIG");
        jdbcSelectAssociated.returningResultSet("PA_CDATOS", new FlujoPerfilUsuarioRowMapper(FlujoPerfilUsuarioRowMapper.FlujoPerfilUsuarioType.FLUJO_PERFIL_USUARIO_FULL));

        jdbcSelectAvailable = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAvailable.withSchemaName(schema);
        jdbcSelectAvailable.withCatalogName("PAADMAUSUA_FLU");
        jdbcSelectAvailable.withProcedureName("SPGETFLUJOFALTA");
        jdbcSelectAvailable.returningResultSet("PA_CDATOS", new FlujoPerfilUsuarioRowMapper(FlujoPerfilUsuarioRowMapper.FlujoPerfilUsuarioType.FLUJO_PERFIL_USUARIO_FULL));

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMAUSUA_FLU");
        jdbcUpdate.withProcedureName("SPACTAUSUA_FLUJO");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMAUSUA_FLU");
        jdbcDelete.withProcedureName("SPDELAUSUA_FLUJO");
    }

    public FlujoPerfilUsuarioDTO selectRow(Integer entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_USUAR_FLUJ", entityID);
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", null);
        mapSqlParameterSource.addValue("PA_FIID_PERFIL", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<FlujoPerfilUsuarioDTO> data = (List<FlujoPerfilUsuarioDTO>) out.get("PA_CDATOS");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<FlujoPerfilUsuarioDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_USUAR_FLUJ", null);
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", null);
        mapSqlParameterSource.addValue("PA_FIID_PERFIL", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<FlujoPerfilUsuarioDTO>) out.get("PA_CDATOS");
    }

    public List<FlujoPerfilUsuarioDTO> selectRowsByUsuario(Integer usuarioID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_USUAR_FLUJ", null);
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", usuarioID);
        mapSqlParameterSource.addValue("PA_FIID_PERFIL", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<FlujoPerfilUsuarioDTO>) out.get("PA_CDATOS");
    }

    public List<FlujoPerfilUsuarioDTO> selectRowsByUsuarioPerfil(Integer usuarioID, Integer perfilID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_USUAR_FLUJ", null);
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", usuarioID);
        mapSqlParameterSource.addValue("PA_FIID_PERFIL", perfilID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<FlujoPerfilUsuarioDTO>) out.get("PA_CDATOS");
    }

    public List<FlujoPerfilUsuarioDTO> selectRowsFinalByUsuarioPerfil(Integer usuarioID, Integer perfilID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", usuarioID);
        mapSqlParameterSource.addValue("PA_FIID_PERFIL", perfilID);
        Map<String, Object> out = jdbcSelectFinalByUsuario.execute(mapSqlParameterSource);
        return (List<FlujoPerfilUsuarioDTO>) out.get("PA_CDATOS");
    }

    public List<FlujoPerfilUsuarioDTO> selectModulosByUsuarioPerfilAvailable(Integer usuarioID, Integer perfilID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", usuarioID);
        mapSqlParameterSource.addValue("PA_FIID_PERFIL", perfilID);
        Map<String, Object> out = jdbcSelectAvailable.execute(mapSqlParameterSource);
        return (List<FlujoPerfilUsuarioDTO>) out.get("PA_CDATOS");
    }

    public List<FlujoPerfilUsuarioDTO> selectModulosByUsuarioPerfilAssociated(Integer usuarioID, Integer perfilID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", usuarioID);
        mapSqlParameterSource.addValue("PA_FIID_PERFIL", perfilID);
        Map<String, Object> out = jdbcSelectAssociated.execute(mapSqlParameterSource);
        return (List<FlujoPerfilUsuarioDTO>) out.get("PA_CDATOS");
    }

    public List<FlujoPerfilUsuarioDTO> selectRowsDeepFinalByUsuarioPerfil(Integer usuarioID, Integer perfilID) throws CustomException {
        List<FlujoPerfilUsuarioDTO> flujoUsuarioDTOs = this.selectRowsFinalByUsuarioPerfil(usuarioID, perfilID);
        for (FlujoPerfilUsuarioDTO flujoUsuarioDTO : flujoUsuarioDTOs) {
            List<ModuloFlujoDTO> flujoModuloUsuarioDTOs = this.selectRowsModulosByFlujo(flujoUsuarioDTO.getIdFlujo());
            flujoUsuarioDTO.setModuloFlujoUsuarioDTOs(flujoModuloUsuarioDTOs);
        }
        return flujoUsuarioDTOs;
    }

    public List<ModuloFlujoDTO> selectRowsModulosByFlujo(Integer flujoID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_FLUJO", flujoID);
        Map<String, Object> out = jdbcSelectModuloByFlujo.execute(mapSqlParameterSource);
        return (List<ModuloFlujoDTO>) out.get("PA_CDATOS");
    }

    public FlujoPerfilUsuarioDTO insertRow(FlujoPerfilUsuarioDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_USUAR_FLUJ", null);
            mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", entityDTO.getIdEmpleado());
            mapSqlParameterSource.addValue("PA_FIID_PERFIL", entityDTO.getIdPerfil());
            mapSqlParameterSource.addValue("PA_FIID_FLUJO", entityDTO.getIdFlujo());
            mapSqlParameterSource.addValue("PA_FIESTATUS", entityDTO.getEstatus());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                entityDTO.setIdFlujoUsuario((BigDecimal) out.get("PA_NRESEJECUCION"));
                entityDTO.setInserted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of Flujo Usuario"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception to insert row of Flujo Usuario"), ex);
        }
    }

    public FlujoPerfilUsuarioDTO updateRow(FlujoPerfilUsuarioDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_USUAR_FLUJ", entityDTO.getIdFlujoUsuario());
            mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", entityDTO.getIdEmpleado());
            mapSqlParameterSource.addValue("PA_FIID_PERFIL", entityDTO.getIdPerfil());
            mapSqlParameterSource.addValue("PA_FIID_FLUJO", entityDTO.getIdFlujo());
            mapSqlParameterSource.addValue("PA_FIESTATUS", entityDTO.getEstatus());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error to update row of Flujo Usuario"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception to update row of Flujo Usuario"), ex);
        }
    }

    public FlujoPerfilUsuarioDTO deleteRow(FlujoPerfilUsuarioDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_USUAR_FLUJ", entityDTO.getIdFlujoUsuario());
            Map<String, Object> out = jdbcDelete.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() > 0;
            if (success) {
                entityDTO.setDeleted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error to delete row of Flujo Usuario"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_DELETE_DATA.detalle("Error to delete row of Flujo Usuario"), ex);
        }
    }

}
