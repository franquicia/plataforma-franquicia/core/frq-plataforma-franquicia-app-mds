/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.usuario.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PerfilUsuarioDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_perfil_usuario")
    private Integer idPerfilUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_empleado")
    private Integer idEmpleado;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "perfil")
    private PerfilDTO perfilDTO;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "flujos")
    private List<FlujoPerfilUsuarioDTO> flujoUsuarioDTOs;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "estatus")
    private Integer estatus;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "personalizado")
    private Integer personalizado;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public PerfilUsuarioDTO() {
    }

    public PerfilUsuarioDTO(PerfilDTO perfilDTO) {
        this.perfilDTO = perfilDTO;
    }

    public Integer getIdPerfilUsuario() {
        return idPerfilUsuario;
    }

    public void setIdPerfilUser(BigDecimal idPerfilUsuario) {
        this.idPerfilUsuario = (idPerfilUsuario == null ? null : idPerfilUsuario.intValue());
    }

    public Integer getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(BigDecimal idEmpleado) {
        this.idEmpleado = (idEmpleado == null ? null : idEmpleado.intValue());
    }

    public PerfilDTO getPerfilDTO() {
        return perfilDTO;
    }

    public void setPerfilDTO(PerfilDTO perfilDTO) {
        this.perfilDTO = perfilDTO;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(BigDecimal estatus) {
        this.estatus = (estatus == null ? null : estatus.intValue());
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public List<FlujoPerfilUsuarioDTO> getFlujoUsuarioDTOs() {
        return flujoUsuarioDTOs;
    }

    public void setFlujoUsuarioDTOs(List<FlujoPerfilUsuarioDTO> flujoUsuarioDTOs) {
        this.flujoUsuarioDTOs = flujoUsuarioDTOs;
    }

    public Integer getPersonalizado() {
        return personalizado;
    }

    public void setPersonalizado(BigDecimal personalizado) {
        this.personalizado = (personalizado == null ? null : personalizado.intValue());
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "PerfilUsuarioDTO{" + "idPerfilUsuario=" + idPerfilUsuario + ", idEmpleado=" + idEmpleado + ", perfilDTO=" + perfilDTO + ", flujoUsuarioDTOs=" + flujoUsuarioDTOs + ", estatus=" + estatus + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
