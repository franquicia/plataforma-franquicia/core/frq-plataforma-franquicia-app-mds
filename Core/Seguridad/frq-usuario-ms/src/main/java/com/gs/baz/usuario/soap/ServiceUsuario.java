/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.usuario.soap;

import com.gs.baz.usuario.dao.UsuarioDAOImpl;
import com.gs.baz.usuario.soap.dto.Authorization;
import com.gs.baz.usuario.soap.dto.SOAPOutMessage;
import com.gs.baz.usuario.init.WebApplicationContextLocator;
import com.gs.baz.usuario.soap.dto.Usuario;
import java.util.Map;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author jfernandor
 */
@WebService(serviceName = "service/soap/usuario")
public class ServiceUsuario {

    @Autowired
    private UsuarioDAOImpl usuarioDAOImpl;

    @Resource
    WebServiceContext wsctx;

    public ServiceUsuario() {
        AutowiredAnnotationBeanPostProcessor bpp = new AutowiredAnnotationBeanPostProcessor();
        ApplicationContext currentContext = WebApplicationContextLocator.getCurrentWebApplicationContext();
        if (currentContext != null) {
            bpp.setBeanFactory(currentContext.getAutowireCapableBeanFactory());
            bpp.processInjection(this);
        }
    }

    /**
     * This is a sample web service operation
     *
     * @param authorization
     * @param usuario
     * @return
     */
    @WebMethod
    @WebResult(name = "OutMessage")
    public SOAPOutMessage desbloqueo(@WebParam(name = "Authorization", header = true) Authorization authorization, @WebParam(name = "Usuario") Usuario usuario) {
        MessageContext mctx = wsctx.getMessageContext();
        Map http_headers = (Map) mctx.get(MessageContext.HTTP_REQUEST_HEADERS);
        if (authorization == null) {
            return new SOAPOutMessage(401, "Full authentication is required to access this resource unauthorized");
        } else {
            if (authorization.getPassword() != null && authorization.getUsername() != null) {
                if (authorization.getPassword().equals("pass") && authorization.getUsername().equals("202622")) {
                    return usuarioDAOImpl.desbloquea(usuario.getEmpleado());
                } else {
                    return new SOAPOutMessage(403, "Forbidden");
                }
            } else {
                return new SOAPOutMessage(403, "Forbidden");
            }
        }
    }
}
