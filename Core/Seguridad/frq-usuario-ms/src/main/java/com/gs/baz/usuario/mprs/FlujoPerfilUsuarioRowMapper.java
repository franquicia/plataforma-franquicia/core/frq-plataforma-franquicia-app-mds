package com.gs.baz.usuario.mprs;

import com.gs.baz.usuario.dto.FlujoDTO;
import com.gs.baz.usuario.dto.FlujoPerfilUsuarioDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class FlujoPerfilUsuarioRowMapper implements RowMapper<FlujoPerfilUsuarioDTO> {

    private FlujoPerfilUsuarioDTO usuarioFlujoDTO;
    private FlujoDTO flujoDTO;

    public enum FlujoPerfilUsuarioType {

        FLUJO_PERFIL_USUARIO,
        FLUJO_PERFIL_USUARIO_FULL
    }

    private final FlujoPerfilUsuarioType type;

    public FlujoPerfilUsuarioRowMapper(FlujoPerfilUsuarioType type) {
        this.type = type;
    }

    @Override
    public FlujoPerfilUsuarioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        if (FlujoPerfilUsuarioType.FLUJO_PERFIL_USUARIO.equals(type)) {
            usuarioFlujoDTO = new FlujoPerfilUsuarioDTO();
            usuarioFlujoDTO.setIdFlujoUsuario(((BigDecimal) rs.getObject("FIID_USUAR_FLUJ")));
            usuarioFlujoDTO.setIdEmpleado((BigDecimal) rs.getObject("FIID_EMPLEADO"));
            usuarioFlujoDTO.setIdPerfil((BigDecimal) rs.getObject("FIID_PERFIL"));
            usuarioFlujoDTO.setIdFlujo((BigDecimal) rs.getObject("FIID_FLUJO"));
            usuarioFlujoDTO.setEstatus((BigDecimal) rs.getObject("FIESTATUS"));
        }
        if (FlujoPerfilUsuarioType.FLUJO_PERFIL_USUARIO_FULL.equals(type)) {
            usuarioFlujoDTO = new FlujoPerfilUsuarioDTO();
            flujoDTO = new FlujoDTO();
            usuarioFlujoDTO.setIdFlujoUsuario(((BigDecimal) rs.getObject("FIID_USUAR_FLUJ")));
            flujoDTO.setIdFlujo((BigDecimal) rs.getObject("FIID_FLUJO"));
            flujoDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
            flujoDTO.setTipoApp(rs.getString("FCTIPOAPP"));
            usuarioFlujoDTO.setFlujoDTO(flujoDTO);
            usuarioFlujoDTO.setIdPerfil((BigDecimal) rs.getObject("FIID_PERFIL"));
        }
        return usuarioFlujoDTO;
    }

}
