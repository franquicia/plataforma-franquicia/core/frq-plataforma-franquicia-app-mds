/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.usuario.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.model.bucket.client.services.rest.SubServiceModuloFlujo;
import com.gs.baz.usuario.dao.UsuarioDAOImpl;
import com.gs.baz.usuario.dto.BusquedaUsuario;
import com.gs.baz.usuario.dto.FlujoPerfilUsuarioDTO;
import com.gs.baz.usuario.dto.ModuloFlujoDTO;
import com.gs.baz.usuario.dto.PerfilUsuarioDTO;
import com.gs.baz.usuario.dto.UsuarioDTO;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.QueryParam;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("service/usuario")
public class ServiceUsuario {

    @Autowired
    private UsuarioDAOImpl usuarioDAOImpl;

    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    SubServiceModuloFlujo subServiceModuloFlujo;

    final ObjectMapper objectMapper = new ObjectMapper();

    final VersionBI versionBI = new VersionBI();

    private final Logger logger = LogManager.getLogger();

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() {
        return versionBI.getVersion();
    }

    /**
     *
     * @return
     */
    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    /**
     *
     * @return @throws CustomException
     */
    @RequestMapping(value = "secure/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UsuarioDTO> getUsuarios() throws CustomException {
        return usuarioDAOImpl.selectRows();
    }

    /**
     *
     * @param idUsuario
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/{id_usuario}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioDTO getUsuario(@PathVariable("id_usuario") Integer idUsuario) throws CustomException {
        return usuarioDAOImpl.selectRow(idUsuario);
    }

    /**
     *
     * @param usuarioDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioDTO postUsuario(@RequestBody UsuarioDTO usuarioDTO) throws CustomException {
        if (usuarioDTO.getIdUsuario() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_usuario"));
        }
        if (usuarioDTO.getIdCeco() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_ceco"));
        }
        if (usuarioDTO.getNombreUsuario() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("nombre_usuario"));
        }
        if (usuarioDTO.getEstatusEmpleado() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("estatus_empleado"));
        }
        if (usuarioDTO.getCorreo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("correo"));
        }
        return usuarioDAOImpl.insertRow(usuarioDTO);
    }

    /**
     *
     * @param flujoPerfilUsuarioDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/flujo/perfil/get/associated", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FlujoPerfilUsuarioDTO> getFlujosPerfilAssociated(@RequestBody FlujoPerfilUsuarioDTO flujoPerfilUsuarioDTO) throws CustomException {
        return usuarioDAOImpl.getPerfilUsuarioDAOImpl().getFlujoPerfilUsuarioDAOImpl().selectModulosByUsuarioPerfilAssociated(flujoPerfilUsuarioDTO.getIdEmpleado(), flujoPerfilUsuarioDTO.getIdPerfil());
    }

    /**
     *
     * @param flujoPerfilUsuarioDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/flujo/perfil/get/available", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FlujoPerfilUsuarioDTO> getFlujosPerfilAvailable(@RequestBody FlujoPerfilUsuarioDTO flujoPerfilUsuarioDTO) throws CustomException {
        return usuarioDAOImpl.getPerfilUsuarioDAOImpl().getFlujoPerfilUsuarioDAOImpl().selectModulosByUsuarioPerfilAvailable(flujoPerfilUsuarioDTO.getIdEmpleado(), flujoPerfilUsuarioDTO.getIdPerfil());
    }

    /**
     *
     * @param flujoPerfilUsuarioDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/flujo/perfil/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public FlujoPerfilUsuarioDTO postFlujoPerfil(@RequestBody FlujoPerfilUsuarioDTO flujoPerfilUsuarioDTO) throws CustomException {
        if (flujoPerfilUsuarioDTO.getIdEmpleado() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_usuario"));
        }
        if (flujoPerfilUsuarioDTO.getIdPerfil() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_perfil"));
        }
        if (flujoPerfilUsuarioDTO.getIdFlujo() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_flujo"));
        }
        return usuarioDAOImpl.getPerfilUsuarioDAOImpl().getFlujoPerfilUsuarioDAOImpl().insertRow(flujoPerfilUsuarioDTO);
    }

    /**
     *
     * @param flujoPerfilUsuarioDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/flujo/perfil/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public FlujoPerfilUsuarioDTO deleteFlujoPerfil(@RequestBody FlujoPerfilUsuarioDTO flujoPerfilUsuarioDTO) throws CustomException {
        if (flujoPerfilUsuarioDTO.getIdFlujoUsuario() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_flujo_usuario"));
        }
        return usuarioDAOImpl.getPerfilUsuarioDAOImpl().getFlujoPerfilUsuarioDAOImpl().deleteRow(flujoPerfilUsuarioDTO);
    }

    /**
     *
     * @param usuarioDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/put", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioDTO putUsuario(@RequestBody UsuarioDTO usuarioDTO) throws CustomException {
        if (usuarioDTO.getIdUsuario() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_usuario"));
        }
        return usuarioDAOImpl.updateRow(usuarioDTO);
    }

    /**
     *
     * @param idUsuario
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/perfiles/{id_usuario}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioDTO getPerfilesUsuario(@PathVariable("id_usuario") Integer idUsuario) throws CustomException {
        UsuarioDTO usuarioDTO = usuarioDAOImpl.selectRow(idUsuario);
        if (usuarioDTO != null) {
            usuarioDTO.setPerfilUsuarioDTOs(usuarioDAOImpl.getPerfilUsuarioDAOImpl().selectRowsFinalByEmpleado(idUsuario));
        }
        return usuarioDTO;
    }

    @RequestMapping(value = "secure/get/perfiles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UsuarioDTO> getUsuariosEmpleadoPerfiles(@RequestParam("empleado") String value) throws CustomException {
        List<BusquedaUsuario> busquedaUsuarios = usuarioDAOImpl.selectRows(value);
        List<UsuarioDTO> usuarioDTOs = new ArrayList<>();
        for (BusquedaUsuario busquedaUsuario : busquedaUsuarios) {
            UsuarioDTO usuarioDTO = new UsuarioDTO();
            usuarioDTO.setNombreUsuario(busquedaUsuario.getNombre());
            usuarioDTO.setIdUsuario(new BigDecimal(busquedaUsuario.getNumeroEmpleado()));
            usuarioDTO.setPerfilUsuarioDTOs(usuarioDAOImpl.getPerfilUsuarioDAOImpl().selectRowsFinalByEmpleado(busquedaUsuario.getNumeroEmpleado()));
            usuarioDTOs.add(usuarioDTO);
        }
        return usuarioDTOs;
    }

    /**
     *
     * @param idUsuario
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/deep/perfiles/{id_usuario}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioDTO getDeepPerfilesUsuario(@PathVariable("id_usuario") Integer idUsuario) throws CustomException {
        UsuarioDTO usuarioDTO = usuarioDAOImpl.selectRow(idUsuario);
        usuarioDTO.setPerfilUsuarioDTOs(usuarioDAOImpl.getPerfilUsuarioDAOImpl().selectRowsDeepByEmpleado(idUsuario));
        return usuarioDTO;
    }

    /**
     *
     * @param headers
     * @param idUsuario
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/deep/modulos/{id_usuario}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioDTO getDeepMenuUsuario(@RequestHeader HttpHeaders headers, @PathVariable("id_usuario") Integer idUsuario) throws CustomException {
        final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
        final String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort();
        subServiceModuloFlujo.init(basePath, httpEntity);
        UsuarioDTO usuarioDTO = usuarioDAOImpl.selectRow(idUsuario);
        List<PerfilUsuarioDTO> perfiles = usuarioDAOImpl.getPerfilUsuarioDAOImpl().selectRowsDeepByEmpleado(idUsuario);
        List<ModuloFlujoDTO> modulosFlujo = new ArrayList<>();
        for (PerfilUsuarioDTO perfil : perfiles) {
            List<FlujoPerfilUsuarioDTO> flujosPerfil = perfil.getFlujoUsuarioDTOs();
            for (FlujoPerfilUsuarioDTO flujoPerfil : flujosPerfil) {
                List<ModuloFlujoDTO> modulosFlujoTemp = new ObjectMapper().convertValue(subServiceModuloFlujo.getModulosFlujo(flujoPerfil.getIdFlujo()), objectMapper.getTypeFactory().constructCollectionLikeType(List.class, ModuloFlujoDTO.class));
                modulosFlujoTemp.forEach(moduloFlujo -> {
                    ModuloFlujoDTO moduloFlujoFilter = modulosFlujo.stream().filter(moduloFlujoTemp -> moduloFlujoTemp.getIdFlujoModulo().equals(moduloFlujo.getIdFlujoModulo())).findFirst().orElse(null);
                    if (moduloFlujoFilter == null) {
                        modulosFlujo.add(moduloFlujo);
                    } else if (moduloFlujoFilter.getIdParent() != null) {
                        modulosFlujo.add(moduloFlujo);
                    }
                });
            }
        }
        usuarioDTO.setModuloFlujoDTOs(modulosFlujo);
        return usuarioDTO;
    }

    /**
     *
     * @param idUsuario
     * @param idPerfil
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/perfiles/{id_usuario}/{id_perfil}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FlujoPerfilUsuarioDTO> getFlujosUsuarioPerfil(@PathVariable("id_usuario") Integer idUsuario, @PathVariable("id_perfil") Integer idPerfil) throws CustomException {
        UsuarioDTO usuarioDTO = usuarioDAOImpl.selectRow(idUsuario);
        List<FlujoPerfilUsuarioDTO> flujoUsuarioDTOs = usuarioDAOImpl.getPerfilUsuarioDAOImpl().getFlujoPerfilUsuarioDAOImpl().selectRowsByUsuarioPerfil(idUsuario, idPerfil);
        usuarioDTO.setFlujoUsuarioDTOs(flujoUsuarioDTOs);
        return flujoUsuarioDTOs;
    }

    /**
     *
     * @param idUsuario
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/perfiles/associated/{id_usuario}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PerfilUsuarioDTO> getPerfilesAssociatedUsuario(@PathVariable("id_usuario") Integer idUsuario) throws CustomException {
        return usuarioDAOImpl.getPerfilUsuarioDAOImpl().selectPerfilesByUsuarioAssociated(idUsuario);
    }

    /**
     *
     * @param idUsuario
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/perfiles/available/{id_usuario}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PerfilUsuarioDTO> getPerfilesAvailableUsuario(@PathVariable("id_usuario") Integer idUsuario) throws CustomException {
        return usuarioDAOImpl.getPerfilUsuarioDAOImpl().selectPerfilesByUsuarioAvailable(idUsuario);
    }

    /**
     *
     * @param idUsuario
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/perfiles/fixed/{id_usuario}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PerfilUsuarioDTO> getPerfilesFijosUsuario(@PathVariable("id_usuario") Integer idUsuario) throws CustomException {
        return usuarioDAOImpl.getPerfilUsuarioDAOImpl().selectPerfilesByUsuarioFixed(idUsuario);
    }

    /**
     *
     * @param perfilUsuarioDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/post/perfiles", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PerfilUsuarioDTO postPerfilUsuario(@RequestBody PerfilUsuarioDTO perfilUsuarioDTO) throws CustomException {
        return usuarioDAOImpl.getPerfilUsuarioDAOImpl().insertRow(perfilUsuarioDTO);
    }

    /**
     *
     * @param perfilUsuarioDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/delete/perfiles", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PerfilUsuarioDTO deletePerfilUsuario(@RequestBody PerfilUsuarioDTO perfilUsuarioDTO) throws CustomException {
        return usuarioDAOImpl.getPerfilUsuarioDAOImpl().deleteRow(perfilUsuarioDTO);
    }

    /**
     *
     * @param value
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/nombre", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UsuarioDTO> getUsuariosByNombre(@QueryParam("value") String value) throws CustomException {
        return usuarioDAOImpl.selectRows(null, value);
    }

    /**
     *
     * @param value
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/get/empleado", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UsuarioDTO> getUsuariosByEmpleado(@QueryParam("value") String value) throws CustomException {
        return usuarioDAOImpl.selectRows(value, null);
    }

    /**
     *
     * @param usuarioDTO
     * @return
     * @throws CustomException
     */
    @RequestMapping(value = "secure/baja", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioDTO bajaUsuario(@RequestBody UsuarioDTO usuarioDTO) throws CustomException {
        if (usuarioDTO.getIdUsuario() == null) {
            throw new CustomException(ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.detalle("id_usuario"));
        }
        return usuarioDAOImpl.bajaUsuario(usuarioDTO);
    }

}
