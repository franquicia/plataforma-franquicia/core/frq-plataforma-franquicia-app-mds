/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.usuario.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ModuloFlujoDTO {

    @JsonProperty(value = "id_flujo_modulo")
    private Integer idFlujoModulo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_flujo")
    private Integer idFlujo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_modulo")
    private Integer idModulo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "modulo")
    private ModuloDTO moduloDTO;

    @JsonProperty(value = "id_parent")
    private Integer idParent;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "children")
    private List<ModuloFlujoDTO> children;

    @JsonProperty(value = "oculto_menu")
    private Boolean ocultoMenu;

    public ModuloFlujoDTO() {
    }

    public Integer getIdFlujoModulo() {
        return idFlujoModulo;
    }

    public void setIdFlujoModulo(BigDecimal idFlujoModulo) {
        this.idFlujoModulo = (idFlujoModulo == null ? null : idFlujoModulo.intValue());
    }

    public Integer getIdFlujo() {
        return idFlujo;
    }

    public void setIdFlujo(BigDecimal idFlujo) {
        this.idFlujo = (idFlujo == null ? null : idFlujo.intValue());
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdModulo() {
        return idModulo;
    }

    public void setIdModulo(BigDecimal idModulo) {
        this.idModulo = (idModulo == null ? null : idModulo.intValue());
    }

    public ModuloDTO getModuloDTO() {
        return moduloDTO;
    }

    public void setModuloDTO(ModuloDTO moduloDTO) {
        this.moduloDTO = moduloDTO;
    }

    public Integer getIdParent() {
        return idParent;
    }

    public void setIdParent(BigDecimal idParent) {
        this.idParent = (idParent == null ? null : idParent.intValue());
    }

    public List<ModuloFlujoDTO> getChildren() {
        return children;
    }

    public void setChildren(List<ModuloFlujoDTO> children) {
        this.children = children;
    }

    public Boolean getOcultoMenu() {
        return ocultoMenu;
    }

    public void setOcultoMenu(Boolean ocultoMenu) {
        this.ocultoMenu = ocultoMenu;
    }

    @Override
    public String toString() {
        return "ModuloFlujoDTO{" + "idFlujoModulo=" + idFlujoModulo + ", idFlujo=" + idFlujo + ", descripcion=" + descripcion + ", idModulo=" + idModulo + ", moduloDTO=" + moduloDTO + ", idParent=" + idParent + ", children=" + children + ", ocultoMenu=" + ocultoMenu + '}';
    }

}
