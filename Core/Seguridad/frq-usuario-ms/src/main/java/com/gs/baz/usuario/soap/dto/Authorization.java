/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.usuario.soap.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cescobarh
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Authorization", namespace = "http://your.namespace.com")
public class Authorization implements Serializable {

    @XmlElement(name = "username", required = true)
    private String username;

    @XmlElement(name = "password", required = true)
    protected String password;

    public Authorization() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
