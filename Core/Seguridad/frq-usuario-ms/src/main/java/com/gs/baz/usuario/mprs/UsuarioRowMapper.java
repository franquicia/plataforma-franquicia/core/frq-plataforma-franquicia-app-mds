package com.gs.baz.usuario.mprs;

import com.gs.baz.usuario.dto.UsuarioDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class UsuarioRowMapper implements RowMapper<UsuarioDTO> {

    private UsuarioDTO usuario;

    @Override
    public UsuarioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        usuario = new UsuarioDTO();
        usuario.setIdUsuario(((BigDecimal) rs.getObject("FIID_EMPLEADO")));
        usuario.setEstatusEmpleado((BigDecimal) rs.getObject("FIID_ESTATUSUSU"));
        usuario.setTipoEmpleado(rs.getString("FCTIPOEMPLEADO"));
        usuario.setNombreUsuario(rs.getString("FCNOMBRE"));
        usuario.setUltimaFechaIngreso(rs.getString("FDFECHAULTIMAI"));
        usuario.setIdCeco(rs.getString("FCID_CECO"));
        usuario.setCorreo(rs.getString("FCCORREO"));
        return usuario;
    }
}
