package com.gs.baz.usuario.mprs;

import com.gs.baz.usuario.dto.ModuloDTO;
import com.gs.baz.usuario.dto.ModuloFlujoDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class ModuloFlujoUsuarioRowMapper implements RowMapper<ModuloFlujoDTO> {

    private ModuloFlujoDTO moduloFlujoDTO;
    private ModuloDTO moduloDTO;

    @Override
    public ModuloFlujoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        moduloFlujoDTO = new ModuloFlujoDTO();
        moduloDTO = new ModuloDTO();
        moduloFlujoDTO.setIdFlujo(((BigDecimal) rs.getObject("FIID_FLUJO")));
        moduloFlujoDTO.setIdParent((BigDecimal) rs.getObject("FIID_PARENT"));
        moduloDTO.setIdModulo((BigDecimal) rs.getObject("FIID_MODULO"));
        moduloDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
        moduloDTO.setModulo(rs.getString("FCMODULO"));
        moduloDTO.setEsWeb(((BigDecimal) rs.getObject("FIESWEB")));
        moduloFlujoDTO.setModuloDTO(moduloDTO);
        return moduloFlujoDTO;
    }
}
