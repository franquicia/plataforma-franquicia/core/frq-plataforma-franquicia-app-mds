package com.gs.baz.usuario.mprs;

import com.gs.baz.usuario.dto.PerfilDTO;
import com.gs.baz.usuario.dto.PerfilUsuarioDTO;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class PerfilUsuarioRowMapper implements RowMapper<PerfilUsuarioDTO> {

    @Override
    public PerfilUsuarioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        PerfilUsuarioDTO usuarioFlujoDTO = new PerfilUsuarioDTO();
        PerfilDTO perfilDTO = new PerfilDTO();
        usuarioFlujoDTO.setIdPerfilUser(((BigDecimal) rs.getObject("FIIDPERFIL_USR")));
        usuarioFlujoDTO.setIdEmpleado((BigDecimal) rs.getObject("FIID_EMPLEADO"));
        perfilDTO.setIdPerfil((BigDecimal) rs.getObject("FIID_PERFIL"));
        perfilDTO.setDescripcion(rs.getString("FC_PERFIL"));
        usuarioFlujoDTO.setPerfilDTO(perfilDTO);
        usuarioFlujoDTO.setPersonalizado((BigDecimal) rs.getObject("FIPERSONALIZADO"));
        return usuarioFlujoDTO;
    }
}
