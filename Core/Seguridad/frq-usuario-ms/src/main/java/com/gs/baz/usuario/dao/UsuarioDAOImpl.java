package com.gs.baz.usuario.dao;

import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.usuario.dto.BusquedaUsuario;
import com.gs.baz.usuario.dto.UsuarioDTO;
import com.gs.baz.usuario.mprs.BusquedaUsuarioRowMapper;
import com.gs.baz.usuario.mprs.UsuarioRowMapper;
import com.gs.baz.usuario.soap.dto.SOAPOutMessage;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

/**
 * l
 *
 * @author cescobarh
 */
@Component
public class UsuarioDAOImpl extends DefaultDAO {

    private DefaultJdbcCall jdbcSelectLogin;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcDelete;
    private DefaultJdbcCall jdbcSelectAll;
    private DefaultJdbcCall jdbcSelectLike;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();

    @Autowired
    private PerfilUsuarioDAOImpl perfilUsuarioDAOImpl;

    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMAUSUARIO");
        jdbcInsert.withProcedureName("SPINSAUSUARIO");

        jdbcSelectLogin = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectLogin.withSchemaName(schema);
        jdbcSelectLogin.withCatalogName("PAADMAUSUARIO");
        jdbcSelectLogin.withProcedureName("SPGETAUSUARIOLOGIN");
        jdbcSelectLogin.returningResultSet("PA_CDATOS", new UsuarioRowMapper());

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMAUSUARIO");
        jdbcSelect.withProcedureName("SPGETAUSUARIO");
        jdbcSelect.returningResultSet("PA_CDATOS", new UsuarioRowMapper());

        jdbcSelectAll = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectAll.withSchemaName(schema);
        jdbcSelectAll.withCatalogName("PAADMAUSUARIO");
        jdbcSelectAll.withProcedureName("SPGETAUSUARIOALL");
        jdbcSelectAll.returningResultSet("PA_CDATOS", new UsuarioRowMapper());

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMAUSUARIO");
        jdbcUpdate.withProcedureName("SPACTAUSUARIO");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMAUSUARIO");
        jdbcDelete.withProcedureName("SPDELAUSUARIO");

        jdbcSelectLike = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelectLike.withSchemaName(schema);
        jdbcSelectLike.withCatalogName("PAMNCONSULTANEG");
        jdbcSelectLike.withProcedureName("SPGETUSUARIOS");
        jdbcSelectLike.returningResultSet("PA_CDATOS", new BusquedaUsuarioRowMapper());

    }

    public PerfilUsuarioDAOImpl getPerfilUsuarioDAOImpl() {
        return perfilUsuarioDAOImpl;
    }

    public UsuarioDTO selectLoginRow(Integer entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", entityID);
        Map<String, Object> out = jdbcSelectLogin.execute(mapSqlParameterSource);
        List<UsuarioDTO> data = (List<UsuarioDTO>) out.get("PA_CDATOS");
        if (data != null && data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public UsuarioDTO selectRow(Integer entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<UsuarioDTO> data = (List<UsuarioDTO>) out.get("PA_CDATOS");
        if (data != null && data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    public List<UsuarioDTO> selectRows() throws CustomException {
        List<UsuarioDTO> lista;
        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("PA_FIID_EMPLEADO", null);
        Map<String, Object> out = jdbcSelect.execute(in);
        lista = (List<UsuarioDTO>) out.get("PA_CDATOS");
        return lista;
    }

    public List<BusquedaUsuario> selectRows(String empleado) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_USUARIO", empleado);
        Map<String, Object> out = jdbcSelectLike.execute(mapSqlParameterSource);
        return (List<BusquedaUsuario>) out.get("PA_CDATOS");
    }

    public List<UsuarioDTO> selectRows(String empleado, String name) throws CustomException {
        List<UsuarioDTO> lista;
        MapSqlParameterSource in = new MapSqlParameterSource();
        in.addValue("PA_FIIDUSUARIO", empleado);
        in.addValue("PA_FCNOMBRE", name);
        Map<String, Object> out = jdbcSelectAll.execute(in);
        lista = (List<UsuarioDTO>) out.get("PA_CDATOS");
        return lista;
    }

    public UsuarioDTO insertRow(UsuarioDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", entityDTO.getIdUsuario());
            mapSqlParameterSource.addValue("PA_FIID_ESTATUSUSU", entityDTO.getEstatusEmpleado());
            mapSqlParameterSource.addValue("PA_FCTIPOEMPLEADO", entityDTO.getTipoEmpleado());
            mapSqlParameterSource.addValue("PA_FCPUESTO", entityDTO.getPuesto());
            mapSqlParameterSource.addValue("PA_FCNOMBRE", entityDTO.getNombreUsuario());
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_FCCORREO", entityDTO.getCorreo());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of Usuario"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error to insert row of Usuario"), ex);
        }
    }

    public UsuarioDTO updateRow(UsuarioDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", entityDTO.getIdUsuario());
            mapSqlParameterSource.addValue("PA_FIID_ESTATUSUSU", entityDTO.getEstatusEmpleado());
            mapSqlParameterSource.addValue("PA_FCTIPOEMPLEADO", entityDTO.getTipoEmpleado());
            mapSqlParameterSource.addValue("PA_FCPUESTO", entityDTO.getPuesto());
            mapSqlParameterSource.addValue("PA_FCNOMBRE", entityDTO.getNombreUsuario());
            mapSqlParameterSource.addValue("PA_FDFECHAULTIMAI", entityDTO.getUltimaFechaIngreso());
            mapSqlParameterSource.addValue("PA_FCID_CECO", entityDTO.getIdCeco());
            mapSqlParameterSource.addValue("PA_FCCORREO", entityDTO.getCorreo());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error to update row of Usuario"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception to update row of Usuario"), ex);
        }
    }

    public SOAPOutMessage desbloquea(Integer idUsuario) {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", idUsuario);
            mapSqlParameterSource.addValue("PA_FIID_ESTATUSUSU", 1);
            mapSqlParameterSource.addValue("PA_FCTIPOEMPLEADO", null);
            mapSqlParameterSource.addValue("PA_FCPUESTO", null);
            mapSqlParameterSource.addValue("PA_FCNOMBRE", null);
            mapSqlParameterSource.addValue("PA_FDFECHAULTIMAI", null);
            mapSqlParameterSource.addValue("PA_FCID_CECO", null);
            mapSqlParameterSource.addValue("PA_FCCORREO", null);
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 1) {
                return new SOAPOutMessage(101, "Desboqueo aplicada exitosamente");
            } else if (success == 2) {
                return new SOAPOutMessage(102, "El usuario ya se encontraba desbloqueado");
            } else {
                return new SOAPOutMessage(103, "No existe usuario en el sistema");
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return new SOAPOutMessage(104, "Fail");
        }
    }

    public UsuarioDTO bajaUsuario(UsuarioDTO entityDTO) {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIID_EMPLEADO", entityDTO.getIdUsuario());
            mapSqlParameterSource.addValue("PA_FIID_ESTATUSUSU", 2);
            mapSqlParameterSource.addValue("PA_FCTIPOEMPLEADO", null);
            mapSqlParameterSource.addValue("PA_FCPUESTO", null);
            mapSqlParameterSource.addValue("PA_FCNOMBRE", null);
            mapSqlParameterSource.addValue("PA_FDFECHAULTIMAI", null);
            mapSqlParameterSource.addValue("PA_FCID_CECO", null);
            mapSqlParameterSource.addValue("PA_FCCORREO", null);
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            int success = ((BigDecimal) out.get("PA_NRESEJECUCION")).intValue();
            if (success == 1) {
                entityDTO.setDeleted(true);
                entityDTO.setCgSalida("CI-101");
                entityDTO.setDescSalida("Baja aplicada exitosamente");
                return entityDTO;
            } else if (success == 2) {
                entityDTO.setDeleted(false);
                entityDTO.setCgSalida("CI-102");
                entityDTO.setDescSalida("El usuario ya se encontraba dado de baja");
                return entityDTO;
            } else {
                entityDTO.setDeleted(false);
                entityDTO.setCgSalida("CI-103");
                entityDTO.setDescSalida("No existe usuario en el sistema");
                return entityDTO;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return entityDTO;
        }
    }

}
