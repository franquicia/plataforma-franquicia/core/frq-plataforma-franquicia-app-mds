/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.usuario.soap.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cescobarh
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Usuario", namespace = "http://your.namespace.com")
public class Usuario implements Serializable {

    @XmlElement(name = "empleado", required = true)
    private Integer empleado;

    public Integer getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Integer empleado) {
        this.empleado = empleado;
    }

}
