/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.usuario.soap.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cescobarh
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SOAPOutMessage", namespace = "http://your.namespace.com")
public class SOAPOutMessage {

    @XmlElement(name = "codigo1")
    private Integer codigo;

    @XmlElement(name = "mensaje")
    private String mensaje;

    public SOAPOutMessage() {
    }

    public SOAPOutMessage(Integer codigo, String mensaje) {
        this.codigo = codigo;
        this.mensaje = mensaje;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return "SOAPOutMessage{" + "codigo=" + codigo + ", mensaje=" + mensaje + '}';
    }

}
