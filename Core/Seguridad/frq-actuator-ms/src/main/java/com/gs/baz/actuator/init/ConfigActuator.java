package com.gs.baz.actuator.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.actuator")
public class ConfigActuator {

    private final Logger logger = LogManager.getLogger();

    public ConfigActuator() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

}
