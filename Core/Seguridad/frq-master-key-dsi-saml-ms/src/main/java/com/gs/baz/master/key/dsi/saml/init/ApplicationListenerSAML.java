/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.master.key.dsi.saml.init;

import com.gs.baz.master.key.dsi.saml.local.login.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 *
 * @author cescobarh
 */
@Component
public class ApplicationListenerSAML {

    @Autowired
    UsuarioService usuarioService;

    @EventListener({ContextRefreshedEvent.class})
    void contextRefreshedEvent() {
        System.out.println("frq-master-key-dsi-saml-ms is loaded...!");
    }
}
