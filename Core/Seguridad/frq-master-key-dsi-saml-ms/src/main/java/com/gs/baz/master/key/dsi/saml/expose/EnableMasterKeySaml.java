/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.master.key.dsi.saml.expose;

import com.gs.baz.master.key.dsi.saml.init.ConfigMasterKeySaml;
import com.gs.dsi.saml.expose.EnableDsiSaml;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 *
 * @author cescobarh
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@EnableDsiSaml
@Import({ConfigMasterKeySaml.class})
@Configuration
public @interface EnableMasterKeySaml {
}
