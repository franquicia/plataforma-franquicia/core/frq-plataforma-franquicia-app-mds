/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.master.key.dsi.saml.init;

import com.gs.baz.frq.model.commons.Environment;
import com.gs.baz.master.key.dsi.saml.local.login.UsuarioService;
import com.gs.baz.read.propiedades.commons.Propiedades;
import com.gs.baz.usuario.expose.EnableUsuario;
import com.gs.dsi.saml.props.SamlProperties;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.master.key.dsi.saml")
@EnableUsuario
public class ConfigMasterKeySaml {

    private final Logger logger = LogManager.getLogger();

    public ConfigMasterKeySaml() {
        logger.info("Loading ConfigMasterKeySaml....!");

    }

    /**
     *
     * @return
     */
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public UsuarioService usuarioService() {
        return new UsuarioService();
    }

    /**
     *
     * @return
     */
    @Bean
    public SamlProperties sAMLProperties() {
        SamlProperties props = new SamlProperties();
        String entityBaseURL;
        System.out.println("Environment:" + Environment.CURRENT.valueReal());
        if (Environment.CURRENT.valueReal().equals(Environment.DEVELOPING) || Environment.CURRENT.valueReal().equals(Environment.LOCAL) || Environment.CURRENT.valueReal().equals(Environment.QUALITY)) {
            props.setStoreFilePath("classpath:/saml/PlataformaFranquiciaAppAWS.jks");
            props.setKeyName("PlataformaFranquiciaAppAWS");
            props.setStorePass("GrupoSalinas2021");
            props.setEntityId("com:gs:baz:frq:plataformafranquiciaappaws");
            props.setAutenticationSuccessTargetUrl("/");
            props.setAutenticationFailureTargetUrl("/error/saml");
            props.setIdpSSOMetadataFilePath("/saml/idp_dev.xml");
            props.setLogoutSuccessTargetUrl("/saml/logout");
            props.setEntityBaseURL(null);
        } else {
            entityBaseURL = "http://10.53.33.83:82/plataforma-franquicia-app";
            props.setStoreFilePath("classpath:/saml/PlataformaFranquiciaApp.jks");
            props.setStorePass("cua7QRSVwYZPj5gdyPLWtDsHy");
            props.setEntityId("com:gs:baz:frq:plataformafranquiciaapp");
            props.setIdpSSOMetadataURL("https://auth.socio.gs/nidp/saml2/metadata?PID=com:gs:baz:frq:plataformafranquiciaapp");
            props.setLogoutSuccessTargetUrl("https://auth.socio.gs/nidp/jsp/logoutSuccess_latest.jsp?rp=" + entityBaseURL);
            props.setAutenticationSuccessTargetUrl(entityBaseURL);
            props.setAutenticationFailureTargetUrl(entityBaseURL + "/error/saml");
            props.setEntityBaseURL(entityBaseURL);
            props.setKeyName("PlataformaFranquiciaApp");
        }
        if (Environment.CURRENT.valueReal().equals(Environment.QUALITY)) {
            entityBaseURL = "http://10.89.86.56:8082/plataforma-franquicia-app";
            props.setEntityBaseURL(entityBaseURL);
        }
        if (Environment.CURRENT.valueReal().equals(Environment.DEVELOPING)) {
            entityBaseURL = "http://dev.franquiciags.com.mx/plataforma-franquicia-app";
            props.setEntityBaseURL(entityBaseURL);
        }
        props.setsAMLUserDetails(usuarioService());
        List<AntPathRequestMatcher> antPathRequestMatchers = new ArrayList<>();
        antPathRequestMatchers.add(new AntPathRequestMatcher("/**/*.css"));
        antPathRequestMatchers.add(new AntPathRequestMatcher("/**/*.js"));
        antPathRequestMatchers.add(new AntPathRequestMatcher("/**/*.png"));
        antPathRequestMatchers.add(new AntPathRequestMatcher("/**/*.jpg"));
        antPathRequestMatchers.add(new AntPathRequestMatcher("/**/*.svg"));
        antPathRequestMatchers.add(new AntPathRequestMatcher("/**/*.woff2"));
        antPathRequestMatchers.add(new AntPathRequestMatcher("/**/*.otf"));
        antPathRequestMatchers.add(new AntPathRequestMatcher("/**/*.map"));
        antPathRequestMatchers.add(new AntPathRequestMatcher("/**/*.json"));
        antPathRequestMatchers.add(new AntPathRequestMatcher("/**/current-tag-version.info"));
        antPathRequestMatchers.add(new AntPathRequestMatcher("/**/service/session/**"));
        antPathRequestMatchers.add(new AntPathRequestMatcher("/**/actuator/**"));
        //antPathRequestMatchers.add(new AntPathRequestMatcher("/**/login/alternative/**"));
        props.setAntPathRequestMatchers(antPathRequestMatchers);
        props.setRequestMatcher(requestMatcherCustomSession());
        logger.info("Environment " + Environment.CURRENT.valueReal());
        logger.info("PropsSAML " + props.toString());
        return props;
    }

    @Bean
    public RequestMatcher requestMatcherCustomSession() {
        return (HttpServletRequest request) -> {
            HttpSession session = request.getSession(false);
            if (request.getRequestURI().contains("login/alternative")) {
                String val = (String) propiedades.getByKey("login_alternative_enable");
                if (val != null && val.equals("true")) {
                    return false;
                } else if (request.getParameter("empleado") != null) {
                    String empleado = request.getParameter("empleado");
                    List<String> values = (List<String>) propiedades.getByKey("allow_users_alternative");
                    if (values != null) {
                        return !values.contains(empleado);
                    }
                }
            }
            return !(session != null && session.getAttribute("gs_user") != null);
        };
    }

    @Autowired
    private Propiedades propiedades;

    /*@Bean
     public RequestMatcher requestMatcherAllowResources() {
     UrlPathHelper urlPathHelper = new UrlPathHelper();
     AntPathMatcher antPathMatcher = new AntPathMatcher();

     List<String> protectedUrlPatterns = Arrays.asList("/api/**", "/logout");

     return (HttpServletRequest request) -> {
     String uri = urlPathHelper.getPathWithinApplication(request);
     if (protectedUrlPatterns.stream().anyMatch((pattern) -> (antPathMatcher.match(pattern, uri)))) {
     return true;
     }
     return false;
     };
     }*/
}
