/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.master.key.dsi.saml.local.login;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.usuario.dao.UsuarioDAOImpl;
import com.gs.baz.usuario.dto.PerfilDTO;
import com.gs.baz.usuario.dto.PerfilUsuarioDTO;
import com.gs.baz.usuario.dto.UsuarioDTO;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.opensaml.DefaultBootstrap;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.io.Marshaller;
import org.opensaml.xml.io.MarshallerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.saml.SAMLCredential;
import org.springframework.security.saml.userdetails.SAMLUserDetailsService;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;

/**
 *
 * @author cescobarh
 */
@Service
public class UsuarioService implements SAMLUserDetailsService {

    private final Logger logger = LogManager.getLogger();

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private UsuarioDAOImpl usuarioDAOImpl;

    private final Cookie userLogged = new Cookie("user", null);

    private final Cookie loginSaml = new Cookie("login_saml", "true");

    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    HttpServletResponse httpServletResponse;

    public static final String DOCUMENT_BUILDER_FACTORY = "javax.xml.parsers.DocumentBuilderFactory";
    public static final String DOCUMENT_BUILDER_FACTORY_IMPL = "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl";
    private static boolean isBootStrapped = false;

    /**
     *
     * @param credential
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public Object loadUserBySAML(SAMLCredential credential) throws UsernameNotFoundException {
        String idUsuario = credential.getAttributeAsString("idEmpleado");
        String puestoEmpleado = credential.getAttributeAsString("puestoEmpleado");
        String cecoEmpleado = credential.getAttributeAsString("cecoEmpleado");
        String numeroEmpleado = credential.getAttributeAsString("numeroEmpleado");
        String correo = credential.getAttributeAsString("correo");
        String tipoEmpleado = credential.getAttributeAsString("tipoEmpleado");
        List<PerfilUsuarioDTO> perfilUsuarioDTOs = new ArrayList<>();
        if (credential.getAttribute("idPerfil") != null) {
            credential.getAttribute("idPerfil").getAttributeValues().forEach(attr -> {
                String idPerfilSAML = marshall(attr);
                if (idPerfilSAML != null) {
                    perfilUsuarioDTOs.add(new PerfilUsuarioDTO(new PerfilDTO(new Integer(idPerfilSAML))));
                }
            });
        }
        try {
            UsuarioDTO usuario = usuarioDAOImpl.selectLoginRow(new Integer(idUsuario));
            if (usuario == null) {
                usuario = new UsuarioDTO();
                usuario.setIdUsuario(new BigDecimal(idUsuario));
                usuario.setPuesto(puestoEmpleado);
                usuario.setNombreUsuario(numeroEmpleado);
                if (cecoEmpleado != null && !cecoEmpleado.equals("") && cecoEmpleado.length() > 6) {
                    cecoEmpleado = cecoEmpleado.substring(Math.max(0, cecoEmpleado.length() - 6));
                }
                usuario.setIdCeco(cecoEmpleado);
                usuario.setTipoEmpleado(tipoEmpleado);
                usuario.setCorreo(correo);
                usuario.setEstatusEmpleado(new BigDecimal(1));
                usuario = usuarioDAOImpl.insertRow(usuario);
                if (usuario == null) {
                    logger.error("Error en el login, no existe el usuario '" + idUsuario + "' en el sistema");
                    throw new UsernameNotFoundException("Error en el login, no existe el usuario '" + idUsuario + "' en el sistema");
                }
            }
            List<GrantedAuthority> authorities = new ArrayList<>();
            try {
                List<PerfilUsuarioDTO> perfilesPerfilUsuarioDTOs = usuarioDAOImpl.getPerfilUsuarioDAOImpl().pushPerfiles(new Integer(idUsuario), perfilUsuarioDTOs);
                usuario.setPerfilUsuarioDTOs(perfilesPerfilUsuarioDTOs);
                authorities = usuario.getPerfilUsuarioDTOs().stream().map((PerfilUsuarioDTO perfilUsuarioDTO) -> new SimpleGrantedAuthority(perfilUsuarioDTO.getPerfilDTO().getDescripcion())).peek(authority -> {
                    logger.info("Role: " + authority.getAuthority());
                }).collect(Collectors.toList());
            } catch (CustomException ex) {
                logger.warn(ex.getMessage(), ex);
            }
            this.setCookieUser(httpServletRequest, httpServletResponse, idUsuario);
            this.setLoginSaml(httpServletRequest, httpServletResponse);
            logger.info("Usuario autenticado: " + idUsuario);
            boolean enabled = (usuario.getEstatusEmpleado() != null ? usuario.getEstatusEmpleado().equals(1) : false);
            return new User(usuario.getIdUsuario().toString(), passwordEncoder.encode(usuario.getIdUsuario().toString()), enabled, true, true, true, authorities);
        } catch (CustomException ex) {
            throw new UsernameNotFoundException("Error to login user " + idUsuario + " ", ex);
        }
    }

    private String marshall(XMLObject xmlObject) {
        try {
            if (!isBootStrapped) {
                try {
                    DefaultBootstrap.bootstrap();
                    isBootStrapped = true;
                } catch (ConfigurationException e) {
                    logger.error("Error in bootstrapping the OpenSAML2 library", e);
                }
            }
            System.setProperty(DOCUMENT_BUILDER_FACTORY, DOCUMENT_BUILDER_FACTORY_IMPL);
            MarshallerFactory marshallerFactory = org.opensaml.xml.Configuration.getMarshallerFactory();
            Marshaller marshaller = marshallerFactory.getMarshaller(xmlObject);
            Element element = marshaller.marshall(xmlObject);
            return element.getFirstChild().getNodeValue();
        } catch (Exception e) {
            logger.error("Error Serializing the SAML Response", e);
            return null;
        }
    }

    private void setCookieUser(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, String noEmpleado) {
        userLogged.setValue(noEmpleado);
        userLogged.setPath(httpServletRequest.getContextPath());
        httpServletResponse.addCookie(userLogged);
    }

    private void setLoginSaml(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        loginSaml.setValue("true");
        loginSaml.setPath(httpServletRequest.getContextPath());
        httpServletResponse.addCookie(loginSaml);
    }

}
