/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.dsi.saml.props;

import java.util.List;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

/**
 *
 * @author cescobarh
 */
public class SamlProperties {

    private String storeFilePath;

    private String storePass;

    private String keyName;

    private String idpSSOMetadataURL;

    private String idpSSOMetadataFilePath;

    private String entityId;

    private String autenticationSuccessTargetUrl;

    private String autenticationFailureTargetUrl;

    private String logoutSuccessTargetUrl;

    private String entityBaseURL;

    private ExpressionUrlAuthorizationConfigurer.ExpressionInterceptUrlRegistry expressionInterceptUrlRegistry;

    private Object sAMLUserDetails;

    private List<AntPathRequestMatcher> antPathRequestMatchers;

    private RequestMatcher requestMatcher;

    public String getStoreFilePath() {
        return storeFilePath;
    }

    public void setStoreFilePath(String storeFilePath) {
        this.storeFilePath = storeFilePath;
    }

    public String getStorePass() {
        return storePass;
    }

    public void setStorePass(String storePass) {
        this.storePass = storePass;
    }

    public String getKeyName() {
        return keyName;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public String getIdpSSOMetadataURL() {
        return idpSSOMetadataURL;
    }

    public void setIdpSSOMetadataURL(String idpSSOMetadataURL) {
        this.idpSSOMetadataURL = idpSSOMetadataURL;
    }

    public String getIdpSSOMetadataFilePath() {
        return idpSSOMetadataFilePath;
    }

    public void setIdpSSOMetadataFilePath(String idpSSOMetadataFilePath) {
        this.idpSSOMetadataFilePath = idpSSOMetadataFilePath;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getAutenticationSuccessTargetUrl() {
        return autenticationSuccessTargetUrl;
    }

    public void setAutenticationSuccessTargetUrl(String autenticationSuccessTargetUrl) {
        this.autenticationSuccessTargetUrl = autenticationSuccessTargetUrl;
    }

    public String getAutenticationFailureTargetUrl() {
        return autenticationFailureTargetUrl;
    }

    public void setAutenticationFailureTargetUrl(String autenticationFailureTargetUrl) {
        this.autenticationFailureTargetUrl = autenticationFailureTargetUrl;
    }

    public String getLogoutSuccessTargetUrl() {
        return logoutSuccessTargetUrl;
    }

    public void setLogoutSuccessTargetUrl(String logoutSuccessTargetUrl) {
        this.logoutSuccessTargetUrl = logoutSuccessTargetUrl;
    }

    public Object getsAMLUserDetails() {
        return sAMLUserDetails;
    }

    public void setsAMLUserDetails(Object sAMLUserDetails) {
        this.sAMLUserDetails = sAMLUserDetails;
    }

    public String getEntityBaseURL() {
        return entityBaseURL;
    }

    public void setEntityBaseURL(String entityBaseURL) {
        this.entityBaseURL = entityBaseURL;
    }

    public ExpressionUrlAuthorizationConfigurer.ExpressionInterceptUrlRegistry getExpressionInterceptUrlRegistry() {
        return expressionInterceptUrlRegistry;
    }

    public void setExpressionInterceptUrlRegistry(ExpressionUrlAuthorizationConfigurer.ExpressionInterceptUrlRegistry expressionInterceptUrlRegistry) {
        this.expressionInterceptUrlRegistry = expressionInterceptUrlRegistry;
    }

    public List<AntPathRequestMatcher> getAntPathRequestMatchers() {
        return antPathRequestMatchers;
    }

    public void setAntPathRequestMatchers(List<AntPathRequestMatcher> antPathRequestMatchers) {
        this.antPathRequestMatchers = antPathRequestMatchers;
    }

    public RequestMatcher getRequestMatcher() {
        return requestMatcher;
    }

    public void setRequestMatcher(RequestMatcher requestMatcher) {
        this.requestMatcher = requestMatcher;
    }

    @Override
    public String toString() {
        return "SamlProperties{" + "storeFilePath=" + storeFilePath + ", storePass=" + storePass + ", keyName=" + keyName + ", idpSSOMetadataURL=" + idpSSOMetadataURL + ", idpSSOMetadataFilePath=" + idpSSOMetadataFilePath + ", entityId=" + entityId + ", autenticationSuccessTargetUrl=" + autenticationSuccessTargetUrl + ", autenticationFailureTargetUrl=" + autenticationFailureTargetUrl + ", logoutSuccessTargetUrl=" + logoutSuccessTargetUrl + ", entityBaseURL=" + entityBaseURL + ", expressionInterceptUrlRegistry=" + expressionInterceptUrlRegistry + ", sAMLUserDetails=" + sAMLUserDetails + ", antPathRequestMatchers=" + antPathRequestMatchers + ", requestMatcher=" + requestMatcher + '}';
    }

}
