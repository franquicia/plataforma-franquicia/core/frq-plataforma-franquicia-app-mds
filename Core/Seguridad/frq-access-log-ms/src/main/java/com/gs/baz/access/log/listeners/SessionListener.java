/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.access.log.listeners;

import com.gs.baz.access.log.dao.AccessLogDAOImpl;
import com.gs.baz.access.log.dto.AccessLogDTO;
import com.gs.baz.frq.model.commons.Util;
import eu.bitwalker.useragentutils.UserAgent;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author cescobarh
 */
@Component
public class SessionListener implements HttpSessionListener {

    private final ArrayList<HttpSession> httpSessions = new ArrayList<>();

    @Autowired
    private AccessLogDAOImpl accessLogDAOImpl;

    @Autowired
    private HttpServletRequest httpServletRequest;

    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private final Logger logger = LogManager.getLogger();

    public ArrayList<HttpSession> getAllSessions() {
        return this.httpSessions;
    }

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        HttpSession httpSession = se.getSession();
        if (httpSession != null && !httpSessions.contains(httpSession)) {
            Calendar calendario = Calendar.getInstance();
            String ipOrigin = httpServletRequest.getHeader("x-forwarded-for");
            if (ipOrigin == null) {
                ipOrigin = httpServletRequest.getRemoteAddr();
            }
            AccessLogDTO accessLogDTO = new AccessLogDTO();
            UserAgent userAgent = UserAgent.parseUserAgentString(httpServletRequest.getHeader("User-Agent"));
            accessLogDTO.setUserAgent(userAgent);
            accessLogDTO.setFechaCreacion(simpleDateFormat.format(new Date()));
            accessLogDTO.setHttpSession(httpSession.getId());
            accessLogDTO.setIpLocal(Util.getIPLocal());
            accessLogDTO.setIpOrigin(ipOrigin);
            httpSession.setAttribute("access_log", accessLogDTO);
            httpSessions.add(httpSession);
            logger.log(Level.DEBUG, "Session created: '" + se.getSession().getId() + "' at " + calendario.getTime());
        }
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        try {
            HttpSession httpSession = se.getSession();
            if (httpSessions.contains(httpSession)) {
                httpSessions.remove(httpSession);
                Calendar calendario = Calendar.getInstance();
                if (httpSession != null && httpSession.getAttribute("access_log") != null) {
                    AccessLogDTO accessLogDTO = (AccessLogDTO) httpSession.getAttribute("access_log");
                    if (httpSession.getAttribute("estado_cierre") != null) {
                        accessLogDTO.setIdTipoCierre(new BigDecimal(1));
                    } else {
                        accessLogDTO.setIdTipoCierre(new BigDecimal(2));
                    }
                    accessLogDTO.setFechaFin(simpleDateFormat.format(new Date()));
                    try {
                        accessLogDAOImpl.updateRow(accessLogDTO);
                    } catch (Exception ex) {
                        logger.trace(Level.TRACE, ex);
                    }
                } else {
                    logger.log(Level.DEBUG, "AccessLogDTO was null");
                }
                logger.log(Level.DEBUG, "Session destroyed: '" + se.getSession().getId() + "' at " + calendario.getTime());
            }
        } catch (Exception ex) {
            logger.trace(Level.TRACE, ex);
        }

    }
}
