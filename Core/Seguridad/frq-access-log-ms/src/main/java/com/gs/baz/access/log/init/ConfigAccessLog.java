package com.gs.baz.access.log.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.access.log.dao.AccessLogDAOImpl;
import com.gs.baz.access.log.listeners.SessionListener;
import com.gs.baz.access.log.rest.client.SubServiceAccessLog;
import com.gs.baz.read.propiedades.expose.EnableReadPropiedades;
import java.time.Duration;
import javax.servlet.ServletContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.access.log")
@EnableReadPropiedades
public class ConfigAccessLog {

    private final Logger logger = LogManager.getLogger();

    public ConfigAccessLog() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public AccessLogDAOImpl accessLogDAOImpl() {
        return new AccessLogDAOImpl();
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        builder.setConnectTimeout(Duration.ofSeconds(2));
        builder.setReadTimeout(Duration.ofSeconds(2));
        return builder.build();
    }

    @Bean
    public ServletListenerRegistrationBean<SessionListener> servletListenerRegistrationBean(ServletContext sc) {
        sc.addListener(this.sessionListener());
        ServletListenerRegistrationBean<SessionListener> listenerRegBean = new ServletListenerRegistrationBean<>();
        listenerRegBean.setListener(this.sessionListener());
        return listenerRegBean;
    }

    @Bean
    public SessionListener sessionListener() {
        return new SessionListener();
    }

    @Bean
    public SubServiceAccessLog subServiceSession() {
        return new SubServiceAccessLog();
    }
}
