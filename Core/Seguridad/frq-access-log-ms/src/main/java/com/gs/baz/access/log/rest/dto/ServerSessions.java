/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.access.log.rest.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.access.log.dto.AccessLogDTO;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class ServerSessions {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ip_server")
    private String ipServer;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "current_access")
    private List<AccessLogDTO> allAccessLogs;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "total_active_sessions")
    private Integer totalActiveSessions;

    public List<AccessLogDTO> getAllAccessLogs() {
        return allAccessLogs;
    }

    public void setAllAccessLogs(List<AccessLogDTO> allAccessLogs) {
        this.allAccessLogs = allAccessLogs;
    }

    public Integer getTotalActiveSessions() {
        if (this.allAccessLogs != null) {
            return allAccessLogs.size();
        }
        return totalActiveSessions;
    }

    public String getIpServer() {
        return ipServer;
    }

    public void setIpServer(String ipServer) {
        this.ipServer = ipServer;
    }

    @Override
    public String toString() {
        return "ServerSessions{" + "ipServer=" + ipServer + ", allAccessLogs=" + allAccessLogs + ", totalActiveSessions=" + totalActiveSessions + '}';
    }

}
