/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.access.log.rest.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gs.baz.access.log.dto.AccessLogDTO;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class AccessLogsReporterDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "current_access")
    private AccessLogDTO accessLog;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "other_access")
    private List<AccessLogDTO> accessLogs;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "server_sessions")
    private ServerSessions serverSessions;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "servers_sessions")
    private List<ServerSessions> serversSessions;

    public AccessLogDTO getAccessLog() {
        return accessLog;
    }

    public void setAccessLog(AccessLogDTO accessLog) {
        this.accessLog = accessLog;
    }

    public List<AccessLogDTO> getAccessLogs() {
        return accessLogs;
    }

    public void setAccessLogs(List<AccessLogDTO> accessLogs) {
        this.accessLogs = accessLogs;
    }

    public ServerSessions getServerSessions() {
        return serverSessions;
    }

    public void setServerSessions(ServerSessions serverSessions) {
        this.serverSessions = serverSessions;
    }

    public List<ServerSessions> getServersSessions() {
        return serversSessions;
    }

    public void setServersSessions(List<ServerSessions> serversSessions) {
        this.serversSessions = serversSessions;
    }

    @Override
    public String toString() {
        return "AccessLogsDTO{" + "accessLog=" + accessLog + ", accessLogs=" + accessLogs + ", serverSessions=" + serverSessions + ", serversSessions=" + serversSessions + '}';
    }

}
