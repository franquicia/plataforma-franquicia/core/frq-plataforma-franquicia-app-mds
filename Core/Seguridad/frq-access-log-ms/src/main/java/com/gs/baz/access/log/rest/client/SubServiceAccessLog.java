/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.access.log.rest.client;

import com.gs.baz.access.log.rest.dto.AccessLogsReporterDTO;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

/**
 *
 * @author MADA
 */
public class SubServiceAccessLog extends DefaultRestTemplate {

    private final Logger logger = LogManager.getLogger();

    public AccessLogsReporterDTO getAllAccessLogs() {
        try {
            responseString = restTemplate.exchange(Relasionship.ALL_ACCESS_LOGS.url(basePath), HttpMethod.GET, httpEntity, String.class);
            if (responseString.getStatusCode().equals(HttpStatus.OK)) {
                return objectMapper.readValue((String) responseString.getBody(), AccessLogsReporterDTO.class);
            } else {
                throw new CustomException(ModelCodes.EMPTY_RESPONSE_DATA.detalle("AccessLogs response code error from service"));
            }
        } catch (Exception ex) {
            logger.log(Level.TRACE, ex);
            return null;
        }
    }

    public void logoutOthers(Integer idEmpleado, String idSession) {
        try {
            responseString = restTemplate.exchange(Relasionship.LOGOUT_OTHER_ACCESS_LOGS.url(basePath) + "/" + idEmpleado + "/" + idSession + "?fromNode", HttpMethod.GET, httpEntity, String.class);
            if (!responseString.getStatusCode().equals(HttpStatus.OK)) {
                throw new CustomException(ModelCodes.EMPTY_RESPONSE_DATA.detalle("AccessLogs response code error from service"));
            }
        } catch (Exception ex) {
            logger.log(Level.TRACE, ex);
        }
    }

}
