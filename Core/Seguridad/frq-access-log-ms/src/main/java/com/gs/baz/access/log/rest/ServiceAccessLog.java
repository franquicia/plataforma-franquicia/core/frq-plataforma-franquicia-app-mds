/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.access.log.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.access.log.dao.AccessLogDAOImpl;
import com.gs.baz.access.log.dto.AccessLogDTO;
import com.gs.baz.access.log.listeners.SessionListener;
import com.gs.baz.access.log.rest.client.SubServiceAccessLog;
import com.gs.baz.access.log.rest.dto.AccessLogsReporterDTO;
import com.gs.baz.access.log.rest.dto.ServerSessions;
import com.gs.baz.frq.model.commons.GlobalValues;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.Util;
import com.gs.baz.frq.model.commons.ver.VersionBI;
import com.gs.baz.frq.model.commons.ver.VersionDTO;
import com.gs.baz.read.propiedades.commons.Propiedades;
import eu.bitwalker.useragentutils.UserAgent;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/service/session")
public class ServiceAccessLog {

    final VersionBI versionBI = new VersionBI();

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private HttpServletResponse httpServletResponse;

    @Autowired
    AccessLogDAOImpl accessLogDAOImpl;

    @Autowired
    Propiedades propiedades;

    @Autowired
    private SubServiceAccessLog subServiceAccessLog;

    @Autowired
    SessionListener sessionListener;

    private final String ipLocal = Util.getIPLocal();

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @RequestMapping(value = "/version/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() throws CustomException {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/version/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getVersionNodes() throws CustomException {
        String jsonOut = versionBI.infoNodes();
        return jsonOut;
    }

    @RequestMapping(value = "secure/get/history/{id_empleado}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public AccessLogDTO getAccessLog(@PathVariable("id_empleado") Long idEmpleado) throws CustomException {
        return accessLogDAOImpl.selectRow(idEmpleado);
    }

    @RequestMapping(value = "/register/{id_empleado}", method = RequestMethod.GET)
    public AccessLogsReporterDTO register(@RequestHeader HttpHeaders headers, @PathVariable(name = "id_empleado") Integer idEmpleado) throws CustomException {
        AccessLogsReporterDTO accessLogsReporterDTO;
        HttpSession httpSession = httpServletRequest.getSession();
        if (httpSession.getAttribute("gs_user") == null) {
            httpSession.setMaxInactiveInterval(new Integer(GlobalValues.TIME_OUT_SESSION_SECONDS.value()) * 60);
            httpSession.setAttribute("gs_user", idEmpleado);
            String ipOrigin = httpServletRequest.getHeader("x-forwarded-for");
            if (ipOrigin == null) {
                ipOrigin = httpServletRequest.getRemoteAddr();
            }
            ipOrigin = ipOrigin.replace(", ::1", "");
            if (ipOrigin.length() > 15) {
                ipOrigin = ipOrigin.substring(0, 15);
            }
            AccessLogDTO accessLogDTO = new AccessLogDTO();
            UserAgent userAgent = UserAgent.parseUserAgentString(httpServletRequest.getHeader("User-Agent"));
            accessLogDTO.setUserAgent(userAgent);
            accessLogDTO.setHttpSession(httpSession.getId());
            accessLogDTO.setIpLocal(ipLocal);
            accessLogDTO.setIpOrigin(ipOrigin);
            accessLogDTO.setIdUsuario(new BigDecimal(idEmpleado));
            accessLogDTO.setFechaInicio(simpleDateFormat.format(new Date()));
            accessLogDTO = accessLogDAOImpl.insertRow(accessLogDTO);
            if (accessLogDTO.getInserted() != null && accessLogDTO.getInserted()) {
                accessLogDTO.setInserted(null);
                httpSession.setAttribute("access_log", accessLogDTO);
            }
        }
        accessLogsReporterDTO = this.getAllAccessLogsByUser(headers, idEmpleado);
        return accessLogsReporterDTO;
    }

    @RequestMapping(value = "/logout/others/{id_empleado}/{id_session}", method = RequestMethod.GET)
    public void logoutOthers(@RequestHeader HttpHeaders headers, @RequestParam(name = "fromNode", required = false) String fromNode, @PathVariable(name = "id_empleado") Integer idEmpleado, @PathVariable(name = "id_session") String idSession) throws CustomException {
        ArrayList<HttpSession> httpSessions = sessionListener.getAllSessions();
        httpSessions.stream().filter((itemHttpSession) -> (itemHttpSession != null && !itemHttpSession.getId().equals(idSession) && itemHttpSession.getAttribute("gs_user") != null && itemHttpSession.getAttribute("gs_user").equals(idEmpleado))).forEach((itemHttpSession) -> {
            itemHttpSession.invalidate();
        });
        if (fromNode == null) {
            final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
            Object ipsServer = propiedades.getByKey("IPS_SERVER");
            if (ipsServer instanceof List) {
                List<String> listIPsServer = (List<String>) ipsServer;
                for (String ipServer : listIPsServer) {
                    if (!ipLocal.equals(ipServer)) {
                        final String basePath = httpServletRequest.getScheme() + "://" + ipServer + ":" + httpServletRequest.getLocalPort();
                        subServiceAccessLog.init(basePath, httpEntity);
                        subServiceAccessLog.logoutOthers(idEmpleado, idSession);
                    }
                }
            }
        }
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public void logout() throws CustomException {
        HttpSession httpSession = httpServletRequest.getSession(false);
        boolean isLoginSaml = false;
        if (httpSession != null) {
            isLoginSaml = httpSession.getAttribute("SPRING_SECURITY_CONTEXT") != null;
            httpSession.setAttribute("estado_cierre", 1);
            httpSession.invalidate();
        }
        if (isLoginSaml) {
            try {
                httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/saml/logout");
            } catch (IOException ex) {
                throw new CustomException(ex);
            }
        }
    }

    @RequestMapping(value = "/get/user", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ObjectNode user() throws CustomException {
        ObjectNode userOut = objectMapper.createObjectNode();
        HttpSession session = httpServletRequest.getSession();
        if (session.getAttribute("gs_user") != null) {
            userOut.put("CustUsrMasterKey", (Long) session.getAttribute("gs_user"));
        }
        return userOut;
    }

    @RequestMapping(value = "/get/all/current/access/logs/{id_empleado}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public AccessLogsReporterDTO getAllAccessLogsByUser(@RequestHeader HttpHeaders headers, @PathVariable(name = "id_empleado") Integer idEmpleado) throws CustomException {
        AccessLogsReporterDTO accessLogsReporterDTO = this.getAllAccessLogs(headers, null);
        List<ServerSessions> serversSessions = accessLogsReporterDTO.getServersSessions();
        List<AccessLogDTO> allAccessLogsByUser = new ArrayList<>();
        HttpSession httpSession = httpServletRequest.getSession(false);
        serversSessions.forEach((ServerSessions serverSessions) -> {
            if (serverSessions.getAllAccessLogs() != null) {
                List<AccessLogDTO> accessLogs = serverSessions.getAllAccessLogs().stream().filter(item -> item.getIdUsuario() != null && item.getIdUsuario().equals(idEmpleado)).collect(Collectors.toList());
                allAccessLogsByUser.addAll(accessLogs);
            }
        });
        if (httpSession != null) {
            if (allAccessLogsByUser.size() > 0) {
                List<AccessLogDTO> accessLogs = allAccessLogsByUser.stream().filter(item -> item.getHttpSession().equals(httpSession.getId())).collect(Collectors.toList());
                if (accessLogs.size() > 0) {
                    AccessLogDTO accessLogDTO = accessLogs.get(0);
                    accessLogsReporterDTO.setAccessLog(accessLogDTO);
                }
            }
            accessLogsReporterDTO.setAccessLogs(allAccessLogsByUser.stream().filter(item -> !item.getHttpSession().equals(httpSession.getId())).collect(Collectors.toList()));
        } else {
            accessLogsReporterDTO.setAccessLogs(allAccessLogsByUser);
        }
        accessLogsReporterDTO.setServersSessions(null);
        return accessLogsReporterDTO;
    }

    @RequestMapping(value = "/get/all/current/access/logs", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public AccessLogsReporterDTO getAllAccessLogs(@RequestHeader HttpHeaders headers, @RequestParam(name = "fromNode", required = false) String fromNode) throws CustomException {
        AccessLogsReporterDTO accessLogsReporterDTO = new AccessLogsReporterDTO();
        ArrayList<HttpSession> httpSessions = sessionListener.getAllSessions();
        List<AccessLogDTO> accessLogs = new ArrayList<>();
        List<ServerSessions> serversSessions = new ArrayList<>();
        ServerSessions serverSessions = new ServerSessions();
        serverSessions.setIpServer(ipLocal);
        for (HttpSession othersHttpSession : httpSessions) {
            if (othersHttpSession.getAttribute("access_log") != null) {
                AccessLogDTO accessLog = (AccessLogDTO) othersHttpSession.getAttribute("access_log");
                accessLogs.add(accessLog);
            }
        }
        serverSessions.setAllAccessLogs(accessLogs);
        if (fromNode == null) {
            serversSessions.add(serverSessions);
            final HttpEntity<Object> httpEntity = new HttpEntity<>("body", headers);
            Object ipsServer = propiedades.getByKey("IPS_SERVER");
            if (ipsServer instanceof List) {
                List<String> listIPsServer = (List<String>) ipsServer;
                for (String ipServer : listIPsServer) {
                    if (!ipLocal.equals(ipServer)) {
                        final String basePath = httpServletRequest.getScheme() + "://" + ipServer + ":" + httpServletRequest.getLocalPort();
                        subServiceAccessLog.init(basePath, httpEntity);
                        AccessLogsReporterDTO accessLogsNodeDTO = subServiceAccessLog.getAllAccessLogs();
                        if (accessLogsNodeDTO != null) {
                            ServerSessions serverSessionsNode = accessLogsNodeDTO.getServerSessions();
                            serversSessions.add(serverSessionsNode);
                        } else {
                            serverSessions = new ServerSessions();
                            serverSessions.setIpServer(ipServer);
                            serverSessions.setAllAccessLogs(new ArrayList<>());
                            serversSessions.add(serverSessions);
                        }
                    }
                }
            }
            accessLogsReporterDTO.setServersSessions(serversSessions);
        } else {
            accessLogsReporterDTO.setServerSessions(serverSessions);
        }
        return accessLogsReporterDTO;
    }

}
