/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.access.log.filters;

import com.gs.baz.access.log.dto.AccessLogDTO;
import com.gs.baz.access.log.listeners.SessionListener;
import eu.bitwalker.useragentutils.UserAgent;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author cescobarh
 */
@Component
public class SecurityAccessLogFilter implements Filter {

    @Autowired
    SessionListener sessionListener;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        HttpSession httpSession = httpServletRequest.getSession(false);
        String ipOrigin = httpServletRequest.getHeader("x-forwarded-for");
        if (ipOrigin == null) {
            ipOrigin = httpServletRequest.getRemoteAddr();
        }
        boolean isSessionCloned = false;
        String typeSessionCloned = "";
        if (httpSession != null) {
            HttpSession httpSessionFunded = this.accessLogBySessionId(httpSession.getId());
            AccessLogDTO accessLog = (AccessLogDTO) httpSessionFunded.getAttribute("access_log");
            if (accessLog != null) {
                UserAgent userAgent = UserAgent.parseUserAgentString(httpServletRequest.getHeader("User-Agent"));
                AccessLogDTO currentAccessLogDTO = new AccessLogDTO();
                currentAccessLogDTO.setUserAgent(userAgent);
                currentAccessLogDTO.setIpOrigin(ipOrigin);
                if (!accessLog.getIpOrigin().equals(currentAccessLogDTO.getIpOrigin())) {
                    isSessionCloned = true;
                    typeSessionCloned += "ip";
                }
                if (!accessLog.getIdDispositivo().equals(currentAccessLogDTO.getIdDispositivo())) {
                    isSessionCloned = true;
                    typeSessionCloned += (typeSessionCloned.equals("") ? "device" : "&&device");
                }
                if (isSessionCloned) {
                    httpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
                }
            }
        }
        chain.doFilter(httpServletRequest, httpServletResponse);
    }

    private HttpSession accessLogBySessionId(String sessionId) {
        ArrayList<HttpSession> httpSessions = sessionListener.getAllSessions();
        if (httpSessions.size() > 0) {
            if (httpSessions.size() > 0) {
                for (HttpSession httpSession : httpSessions) {
                    if (httpSession != null && httpSession.getId().equals(sessionId)) {
                        return httpSession;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public void destroy() {
    }

}
