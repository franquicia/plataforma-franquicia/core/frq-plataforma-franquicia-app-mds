package com.gs.baz.access.log.dao;

import com.gs.baz.access.log.dao.util.GenericDAO;
import com.gs.baz.access.log.dto.AccessLogDTO;
import com.gs.baz.access.log.mprs.AccessLogRowMapper;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author cescobarh
 */
public class AccessLogDAOImpl extends DefaultDAO implements GenericDAO<AccessLogDTO> {

    private DefaultJdbcCall jdbcInsert;
    private DefaultJdbcCall jdbcUpdate;
    private DefaultJdbcCall jdbcSelect;
    private DefaultJdbcCall jdbcDelete;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcInsert = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcInsert.withSchemaName(schema);
        jdbcInsert.withCatalogName("PAADMINACCESLOG");
        jdbcInsert.withProcedureName("SP_INS_ACCESLOG");

        jdbcUpdate = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcUpdate.withSchemaName(schema);
        jdbcUpdate.withCatalogName("PAADMINACCESLOG");
        jdbcUpdate.withProcedureName("SP_ACT_ACCESLOG");

        jdbcDelete = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcDelete.withSchemaName(schema);
        jdbcDelete.withCatalogName("PAADMINACCESLOG");
        jdbcDelete.withProcedureName("SP_DEL_ACCESLOG");

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMINACCESLOG");
        jdbcSelect.withProcedureName("SP_SEL_ACCESLOG");
        jdbcSelect.returningResultSet("RCL_INFO", new AccessLogRowMapper());
    }

    @Override
    public AccessLogDTO selectRow(Long entityID) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDUSUARIO", entityID);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<AccessLogDTO> data = (List<AccessLogDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<AccessLogDTO> selectRows() throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FIIDUSUARIO", null);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        List<AccessLogDTO> data = (List<AccessLogDTO>) out.get("RCL_INFO");
        if (data.size() > 0) {
            return data;
        } else {
            return null;
        }
    }

    @Override
    public AccessLogDTO insertRow(AccessLogDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDUSUARIO", entityDTO.getIdUsuario());
            mapSqlParameterSource.addValue("PA_FCHTTPSESSION", entityDTO.getHttpSession());
            mapSqlParameterSource.addValue("PA_FCIPORIGEN", entityDTO.getIpOrigin());
            mapSqlParameterSource.addValue("PA_FCIPLOCAL", entityDTO.getIpLocal());
            mapSqlParameterSource.addValue("PA_FIIDDISPOSITIV", entityDTO.getIdDispositivo());
            mapSqlParameterSource.addValue("PA_FIIDNAVEGADOR", entityDTO.getIdNavegador());
            mapSqlParameterSource.addValue("PA_FCNOMNAVEGA", entityDTO.getNombreNavegador());
            mapSqlParameterSource.addValue("PA_FCTIPONAVEGA", entityDTO.getTipoNavegador());
            mapSqlParameterSource.addValue("PA_FCGRUPONAVEGA", entityDTO.getGrupoNavegador());
            mapSqlParameterSource.addValue("PA_FCMANONAVEGA", entityDTO.getManofacturaNavegador());
            mapSqlParameterSource.addValue("PA_FCMTRENDONAVEG", entityDTO.getMotorRenderizacionNavegador());
            mapSqlParameterSource.addValue("PA_FCVERSIONNAVEG", entityDTO.getVersionNavegador());
            mapSqlParameterSource.addValue("PA_FIIDSISTEMA", entityDTO.getIdSistemaOperativo());
            mapSqlParameterSource.addValue("PA_FCNOMBRESO", entityDTO.getNombreSistemaOperativo());
            mapSqlParameterSource.addValue("PA_FCTIPODISP", entityDTO.getTipoDispositivo());
            mapSqlParameterSource.addValue("PA_FCGRUPOSO", entityDTO.getGrupoSistemaOperativo());
            mapSqlParameterSource.addValue("PA_FCMANOSO", entityDTO.getManofacturaSistemaOperativo());
            mapSqlParameterSource.addValue("PA_FIID", entityDTO.getIdAccessLog());
            Map<String, Object> out = jdbcInsert.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setInserted(success);
                entityDTO.setIdAccessLog((BigDecimal) out.get("PA_FIID"));
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Error not success to insert row of  Access Log"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_INSERT_DATA.detalle("Exception error to insert row of  Access Log"), ex);
        }
    }

    @Override
    public AccessLogDTO updateRow(AccessLogDTO entityDTO) throws CustomException {
        try {
            mapSqlParameterSource = new MapSqlParameterSource();
            mapSqlParameterSource.addValue("PA_FIIDTIPOCIERRE", entityDTO.getIdTipoCierre());
            mapSqlParameterSource.addValue("PA_FIID", entityDTO.getIdAccessLog());
            Map<String, Object> out = jdbcUpdate.execute(mapSqlParameterSource);
            boolean success = ((BigDecimal) out.get("PA_EJECUCION")).intValue() == 1;
            if (success) {
                entityDTO.setUpdated(success);
                return entityDTO;
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Error not success to update row of Access Log"));
            }
        } catch (Exception ex) {
            throw new CustomException(ModelCodes.ERROR_TO_UPDATE_DATA.detalle("Exception error to update row of Access Log"), ex);
        }
    }

}
