/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.access.log.rest.client;

/**
 *
 * @author cescobarh
 */
public enum Relasionship {

    /**
     *
     */
    ALL_ACCESS_LOGS("/plataforma-franquicia-app/service/session/get/all/current/access/logs?fromNode"),
    LOGOUT_OTHER_ACCESS_LOGS("/plataforma-franquicia-app/service/session/logout/others");

    private final String urlService;

    private Relasionship(String urlService) {
        this.urlService = urlService;
    }

    /**
     *
     * @param path
     * @return
     */
    public String url(String path) {
        return path + urlService;
    }

}
