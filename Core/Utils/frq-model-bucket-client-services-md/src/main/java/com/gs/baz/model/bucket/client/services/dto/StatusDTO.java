/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class StatusDTO {

    @JsonProperty(value = "id_status")
    private Integer idStatus;

    @JsonProperty(value = "descripcion")
    private String descripcion;

    public StatusDTO() {
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(BigDecimal idStatus) {
        this.idStatus = (idStatus == null ? null : idStatus.intValue());
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "StatusDTO{" + "idStatus=" + idStatus + ", descripcion=" + descripcion + '}';
    }

}
