
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author MADA
 */
public class FileGenericDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "path")
    private String path;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "name_file")
    private String nameFile;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "content_b64")
    private String contentB64;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "content_type")
    private String contentType;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "writed")
    private Boolean writed;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getNameFile() {
        return nameFile;
    }

    public void setNameFile(String nameFile) {
        this.nameFile = nameFile;
    }

    public String getContentB64() {
        return contentB64;
    }

    public void setContentB64(String contentB64) {
        this.contentB64 = contentB64;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Boolean getWrited() {
        return writed;
    }

    public void setWrited(Boolean writed) {
        this.writed = writed;
    }

    @Override
    public String toString() {
        return "FileGenericDTO{" + "path=" + path + ", nameFile=" + nameFile + ", contentB64=" + contentB64 + ", contentType=" + contentType + ", writed=" + writed + '}';
    }

}
