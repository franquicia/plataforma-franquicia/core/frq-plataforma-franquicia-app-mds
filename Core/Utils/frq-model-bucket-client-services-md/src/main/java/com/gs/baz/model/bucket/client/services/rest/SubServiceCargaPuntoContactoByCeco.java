/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.rest;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.model.bucket.client.services.dto.CargaPuntoContactoByCecoDTO;
import com.gs.baz.model.bucket.client.services.util.DefaultRestTemplate;
import com.gs.baz.model.bucket.client.services.util.Relasionship;
import java.io.IOException;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

/**
 *
 * @author B73601
 */
public class SubServiceCargaPuntoContactoByCeco extends DefaultRestTemplate {

    public CargaPuntoContactoByCecoDTO cargaPuntoContactoByCecoDTO(Integer idCeco) throws CustomException {
        try {
            responseString = restTemplate.exchange(Relasionship.CARGA_PUNTOS_CONTACTO_BY_CECO.url(basePath) + "?idCeco=" + idCeco, HttpMethod.GET, httpEntity, String.class);
            if (responseString.getStatusCode().equals(HttpStatus.OK)) {
                return objectMapper.readValue(responseString.getBody(), CargaPuntoContactoByCecoDTO.class);
            } else {
                throw new CustomException(ModelCodes.EMPTY_RESPONSE_DATA.detalle("CargaPuntoContactoByCecoDTO idCeco " + idCeco + " response code error from service"));
            }
        } catch (IOException | CustomException ex) {
            throw new CustomException(ex);
        }
    }

}
