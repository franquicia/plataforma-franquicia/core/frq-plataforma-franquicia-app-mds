/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProtocoloDisciplinaDTO {

    @JsonProperty(value = "id_protocolo_disciplina")
    private Integer idProtocoloDisciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_disciplina_negocio")
    private Integer idDisciplinaNegocio;

    @JsonProperty(value = "id_protocolo")
    private Integer idProtocolo;

    public ProtocoloDisciplinaDTO() {
    }

    public Integer getIdProtocoloDisciplina() {
        return idProtocoloDisciplina;
    }

    public void setIdProtocoloDisciplina(BigDecimal idProtocoloDisciplina) {
        this.idProtocoloDisciplina = (idProtocoloDisciplina == null ? null : idProtocoloDisciplina.intValue());
    }

    public Integer getIdDisciplinaNegocio() {
        return idDisciplinaNegocio;
    }

    public void setIdDisciplinaNegocio(BigDecimal idDisciplinaNegocio) {
        this.idDisciplinaNegocio = (idDisciplinaNegocio == null ? null : idDisciplinaNegocio.intValue());
    }

    public Integer getIdProtocolo() {
        return idProtocolo;
    }

    public void setIdProtocolo(BigDecimal idProtocolo) {
        this.idProtocolo = (idProtocolo == null ? null : idProtocolo.intValue());
    }

    @Override
    public String toString() {
        return "ProtocoloDisciplinaDTO{" + "idProtocoloDisciplina=" + idProtocoloDisciplina + ", idDisciplinaNegocio=" + idDisciplinaNegocio + ", idProtocolo=" + idProtocolo + '}';
    }

}
