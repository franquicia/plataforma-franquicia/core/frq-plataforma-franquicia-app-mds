/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EjeDTO {

    @JsonProperty(value = "id_eje")
    private Integer idEje;

    @JsonProperty(value = "descripcion")
    private String descripcion;

    public EjeDTO() {
    }

    public Integer getIdEje() {
        return idEje;
    }

    public void setIdEje(BigDecimal idEje) {
        this.idEje = (idEje == null ? null : idEje.intValue());
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "EjeDTO{" + "idEje=" + idEje + ", descripcion=" + descripcion + '}';
    }

}
