/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author cescobarh
 */
public class ModeloFranquiciaDTO {

    @JsonProperty(value = "id_modelo")
    private Integer idModelo;

    @JsonProperty(value = "id_franquicia")
    private Integer idFranquicia;

    @JsonProperty(value = "nombre_modelo")
    private String nombreModelo;

    @JsonProperty(value = "fecha_creacion")
    private Date fechaCreacion;

    @JsonProperty(value = "id_status")
    private Integer idStatus;

    @JsonProperty(value = "id_empleado_crea")
    private Integer idEmpleadoCrea;

    @JsonProperty(value = "fecha_modificacion")
    private Date fechaModificacion;

    @JsonProperty(value = "id_empleado_modifica")
    private Integer idEmpleadoModifica;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ejes_modelo")
    private Object ejesModelo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "empleado_crea")
    private Object empleadoCrea;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "empleado_modifica")
    private Object empleadoModificacion;

    public ModeloFranquiciaDTO() {
    }

    public Integer getIdModelo() {
        return idModelo;
    }

    public void setIdModelo(BigDecimal idModelo) {
        this.idModelo = (idModelo == null ? null : idModelo.intValue());
    }

    public Integer getIdFranquicia() {
        return idFranquicia;
    }

    public void setIdFranquicia(BigDecimal idFranquicia) {
        this.idFranquicia = (idFranquicia == null ? null : idFranquicia.intValue());
    }

    public String getNombreModelo() {
        return nombreModelo;
    }

    public void setNombreModelo(String nombreModelo) {
        this.nombreModelo = nombreModelo;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(BigDecimal idStatus) {
        this.idStatus = (idStatus == null ? null : idStatus.intValue());
    }

    public Integer getIdEmpleadoCrea() {
        return idEmpleadoCrea;
    }

    public void setIdEmpleadoCrea(BigDecimal idEmpleadoCrea) {
        this.idEmpleadoCrea = (idEmpleadoCrea == null ? null : idEmpleadoCrea.intValue());
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Integer getIdEmpleadoModifica() {
        return idEmpleadoModifica;
    }

    public void setIdEmpleadoModifica(BigDecimal idEmpleadoModifica) {
        this.idEmpleadoModifica = (idEmpleadoModifica == null ? null : idEmpleadoModifica.intValue());
    }

    public Object getEjesModelo() {
        return ejesModelo;
    }

    public void setEjesModelo(Object ejesModelo) {
        this.ejesModelo = ejesModelo;
    }

    public Object getEmpleadoCrea() {
        return empleadoCrea;
    }

    public void setEmpleadoCrea(Object empleadoCrea) {
        this.empleadoCrea = empleadoCrea;
    }

    public Object getEmpleadoModificacion() {
        return empleadoModificacion;
    }

    public void setEmpleadoModificacion(Object empleadoModificacion) {
        this.empleadoModificacion = empleadoModificacion;
    }

    @Override
    public String toString() {
        return "ModeloFranquiciaDTO{" + "idModelo=" + idModelo + ", idFranquicia=" + idFranquicia + ", nombreModelo=" + nombreModelo + ", fechaCreacion=" + fechaCreacion + ", idStatus=" + idStatus + ", idEmpleadoCrea=" + idEmpleadoCrea + ", fechaModificacion=" + fechaModificacion + ", idEmpleadoModifica=" + idEmpleadoModifica + ", ejesModelo=" + ejesModelo + ", empleadoCrea=" + empleadoCrea + ", empleadoModificacion=" + empleadoModificacion + '}';
    }

}
