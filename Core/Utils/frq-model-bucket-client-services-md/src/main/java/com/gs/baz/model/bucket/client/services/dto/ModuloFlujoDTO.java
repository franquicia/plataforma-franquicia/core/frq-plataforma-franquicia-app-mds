/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ModuloFlujoDTO {

    @JsonProperty(value = "id_flujo_modulo")
    private Integer idFlujoModulo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_flujo")
    private Integer idFlujo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_modulo")
    private Integer idModulo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "modulo")
    private ModuloDTO moduloDTO;

    @JsonProperty(value = "id_parent")
    private Integer idParent;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "parent")
    private ModuloFlujoDTO parent;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "children")
    private List<ModuloFlujoDTO> children;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "estatus")
    private Integer estatus;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "orden")
    private Integer orden;

    @JsonProperty(value = "oculto_menu")
    private Boolean ocultoMenu;

    public ModuloFlujoDTO() {
    }

    public Integer getIdFlujoModulo() {
        return idFlujoModulo;
    }

    public void setIdFlujoModulo(Integer idFlujoModulo) {
        this.idFlujoModulo = idFlujoModulo;
    }

    public Integer getIdFlujo() {
        return idFlujo;
    }

    public void setIdFlujo(Integer idFlujo) {
        this.idFlujo = idFlujo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdModulo() {
        return idModulo;
    }

    public void setIdModulo(Integer idModulo) {
        this.idModulo = idModulo;
    }

    public ModuloDTO getModuloDTO() {
        return moduloDTO;
    }

    public void setModuloDTO(ModuloDTO moduloDTO) {
        this.moduloDTO = moduloDTO;
    }

    public Integer getIdParent() {
        return idParent;
    }

    public void setIdParent(Integer idParent) {
        this.idParent = idParent;
    }

    public ModuloFlujoDTO getParent() {
        return parent;
    }

    public void setParent(ModuloFlujoDTO parent) {
        this.parent = parent;
    }

    public List<ModuloFlujoDTO> getChildren() {
        return children;
    }

    public void setChildren(List<ModuloFlujoDTO> children) {
        this.children = children;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public Boolean getOcultoMenu() {
        return ocultoMenu;
    }

    public void setOcultoMenu(Boolean ocultoMenu) {
        this.ocultoMenu = ocultoMenu;
    }

    @Override
    public String toString() {
        return "ModuloFlujoDTO{" + "idFlujoModulo=" + idFlujoModulo + ", idFlujo=" + idFlujo + ", descripcion=" + descripcion + ", idModulo=" + idModulo + ", moduloDTO=" + moduloDTO + ", idParent=" + idParent + ", parent=" + parent + ", children=" + children + ", estatus=" + estatus + ", orden=" + orden + ", ocultoMenu=" + ocultoMenu + '}';
    }

}
