/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author B73601
 */
public class DataModeloFranquiciaDTO {

    @JsonProperty(value = "metadata")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Metadata metadata;

    @JsonProperty(value = "modelo")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private ModeloFranquiciaDTO modeloDTO;

    @JsonProperty(value = "modelos_franquicia")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<ModeloFranquiciaDTO> modelosFranquicia;

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public ModeloFranquiciaDTO getModeloDTO() {
        return modeloDTO;
    }

    public void setModeloDTO(ModeloFranquiciaDTO modeloDTO) {
        this.modeloDTO = modeloDTO;
    }

    public List<ModeloFranquiciaDTO> getModelosFranquicia() {
        return modelosFranquicia;
    }

    public void setModelosFranquicia(List<ModeloFranquiciaDTO> modelosFranquicia) {
        this.modelosFranquicia = modelosFranquicia;
    }

    @Override
    public String toString() {
        return "DataModeloFranquiciaDTO{" + "metadata=" + metadata + ", modeloDTO=" + modeloDTO + ", modelosFranquicia=" + modelosFranquicia + '}';
    }

}
