/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UsuarioDTO {

    @JsonProperty(value = "id_usuario")
    private Integer idUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_ceco")
    private String idCeco;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "nombre_usuario")
    private String nombreUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_tipo_empleado")
    private Integer tipoEmpleado;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "estatus_empleado")
    private Integer estatusEmpleado;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ultima_fecha_ingreso")
    private String ultimaFechaIngreso;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "perfiles")
    private Object perfiles;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "codigo_salida")
    private String cgSalida;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion_salida")
    private String descSalida;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "flujos")
    private Object flujos;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "correo")
    private String correo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public UsuarioDTO() {
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(BigDecimal idUsuario) {
        this.idUsuario = (idUsuario == null ? null : idUsuario.intValue());
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public Object getPerfiles() {
        return perfiles;
    }

    public void setPerfiles(Object perfiles) {
        this.perfiles = perfiles;
    }

    public String getUltimaFechaIngreso() {
        return ultimaFechaIngreso;
    }

    public void setUltimaFechaIngreso(String ultimaFechaIngreso) {
        this.ultimaFechaIngreso = ultimaFechaIngreso;
    }

    public Object getFlujos() {
        return flujos;
    }

    public void setFlujos(Object flujos) {
        this.flujos = flujos;
    }

    public Integer getTipoEmpleado() {
        return tipoEmpleado;
    }

    public void setTipoEmpleado(BigDecimal tipoEmpleado) {
        this.tipoEmpleado = (tipoEmpleado == null ? null : tipoEmpleado.intValue());
    }

    public Integer getEstatusEmpleado() {
        return estatusEmpleado;
    }

    public void setEstatusEmpleado(BigDecimal estatusEmpleado) {
        this.estatusEmpleado = (estatusEmpleado == null ? null : estatusEmpleado.intValue());
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getCgSalida() {
        return cgSalida;
    }

    public void setCgSalida(String cgSalida) {
        this.cgSalida = cgSalida;
    }

    public String getDescSalida() {
        return descSalida;
    }

    public void setDescSalida(String descSalida) {
        this.descSalida = descSalida;
    }

    @Override
    public String toString() {
        return "UsuarioDTO{" + "idUsuario=" + idUsuario + ", idCeco=" + idCeco + ", nombreUsuario=" + nombreUsuario + ", tipoEmpleado=" + tipoEmpleado + ", estatusEmpleado=" + estatusEmpleado + ", ultimaFechaIngreso=" + ultimaFechaIngreso + ", perfiles=" + perfiles + ", cgSalida=" + cgSalida + ", descSalida=" + descSalida + ", flujos=" + flujos + ", correo=" + correo + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
