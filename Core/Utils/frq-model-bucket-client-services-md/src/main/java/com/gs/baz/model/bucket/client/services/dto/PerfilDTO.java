/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class PerfilDTO {

    @JsonProperty(value = "id_perfil")
    private Integer idPerfil;

    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonProperty(value = "activo")
    private Integer activo;

    @JsonProperty(value = "web_movil")
    private Integer esWeb;

    public PerfilDTO() {
    }

    public Integer getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(BigDecimal idPerfil) {
        this.idPerfil = (idPerfil == null ? null : idPerfil.intValue());
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(BigDecimal activo) {
        this.activo = (activo == null ? null : activo.intValue());
    }

    public Integer getEsWeb() {
        return esWeb;
    }

    public void setEsWeb(BigDecimal esWeb) {
        this.esWeb = (esWeb == null ? null : esWeb.intValue());
    }

    @Override
    public String toString() {
        return "PerfilDTO{" + "idPerfil=" + idPerfil + ", descripcion=" + descripcion + ", activo=" + activo + ", esWeb=" + esWeb + '}';
    }

}
