/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CargaPuntoContactoByCecoDTO {

    @JsonProperty(value = "resultadoOk")
    private Boolean resultadoOk;

    @JsonProperty(value = "usuarios_modificados")
    private Integer usuariosModificados;

    public Boolean getResultadoOk() {
        return resultadoOk;
    }

    public void setResultadoOk(Boolean resultadoOk) {
        this.resultadoOk = resultadoOk;
    }

    public Integer getUsuariosModificados() {
        return usuariosModificados;
    }

    public void setUsuariosModificados(Integer usuariosModificados) {
        this.usuariosModificados = usuariosModificados;
    }

    @Override
    public String toString() {
        return "CargaPuntoContactoByCecoDTO{" + "resultadoOk=" + resultadoOk + ", usuariosModificados=" + usuariosModificados + '}';
    }

}
