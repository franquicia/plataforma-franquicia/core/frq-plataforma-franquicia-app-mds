/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author B73601
 */
public class Metadata {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "disciplinas")
    private List<DisciplinaDTO> disciplinas;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ejes")
    private List<EjeDTO> ejes;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ejecuta")
    private List<EjecutaDTO> ejecuta;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ejes_modelo")
    private List<EjeModeloDTO> ejesModelo;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "frecuencia_medicion")
    private List<FrecuenciaMedicionDTO> frecuenciaMedicion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "status")
    private List<StatusDTO> status;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "tipo_medicion")
    private List<TipoMedicionDTO> tipoMedicion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "categorias")
    private List<CategoriaDTO> categorias;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "franquicias")
    private List<FranquiciaDTO> franquicias;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "colorimetria")
    private ColorimetriaDTO colorimetria;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "perfiles")
    private List<PerfilDTO> perfiles;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "negocios")
    private List<NegocioDTO> negocios;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "driver_experiencia")
    private List<DriverExperienciaDTO> driverExperienciaDTO;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "protocolos")
    private List<ProtocoloDTO> protocolos;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "modelo")
    private List<ModeloFranquiciaDTO> modelo;

    public List<DisciplinaDTO> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<DisciplinaDTO> disciplinas) {
        this.disciplinas = disciplinas;
    }

    public List<EjeDTO> getEjes() {
        return ejes;
    }

    public void setEjes(List<EjeDTO> ejes) {
        this.ejes = ejes;
    }

    public List<EjecutaDTO> getEjecuta() {
        return ejecuta;
    }

    public void setEjecuta(List<EjecutaDTO> ejecuta) {
        this.ejecuta = ejecuta;
    }

    public List<EjeModeloDTO> getEjesModelo() {
        return ejesModelo;
    }

    public void setEjesModelo(List<EjeModeloDTO> ejesModelo) {
        this.ejesModelo = ejesModelo;
    }

    public List<FrecuenciaMedicionDTO> getFrecuenciaMedicion() {
        return frecuenciaMedicion;
    }

    public void setFrecuenciaMedicion(List<FrecuenciaMedicionDTO> frecuenciaMedicion) {
        this.frecuenciaMedicion = frecuenciaMedicion;
    }

    public List<StatusDTO> getStatus() {
        return status;
    }

    public void setStatus(List<StatusDTO> status) {
        this.status = status;
    }

    public List<TipoMedicionDTO> getTipoMedicion() {
        return tipoMedicion;
    }

    public void setTipoMedicion(List<TipoMedicionDTO> tipoMedicion) {
        this.tipoMedicion = tipoMedicion;
    }

    public List<CategoriaDTO> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<CategoriaDTO> categorias) {
        this.categorias = categorias;
    }

    public List<FranquiciaDTO> getFranquicias() {
        return franquicias;
    }

    public void setFranquicias(List<FranquiciaDTO> franquicias) {
        this.franquicias = franquicias;
    }

    public ColorimetriaDTO getColorimetria() {
        return colorimetria;
    }

    public void setColorimetria(ColorimetriaDTO colorimetria) {
        this.colorimetria = colorimetria;
    }

    public List<PerfilDTO> getPerfiles() {
        return perfiles;
    }

    public void setPerfiles(List<PerfilDTO> perfiles) {
        this.perfiles = perfiles;
    }

    public List<NegocioDTO> getNegocios() {
        return negocios;
    }

    public void setNegocios(List<NegocioDTO> negocios) {
        this.negocios = negocios;
    }

    public List<DriverExperienciaDTO> getDriverExperienciaDTO() {
        return driverExperienciaDTO;
    }

    public void setDriverExperienciaDTO(List<DriverExperienciaDTO> driverExperienciaDTO) {
        this.driverExperienciaDTO = driverExperienciaDTO;
    }

    public List<ProtocoloDTO> getProtocolos() {
        return protocolos;
    }

    public void setProtocolos(List<ProtocoloDTO> protocolos) {
        this.protocolos = protocolos;
    }

    public List<ModeloFranquiciaDTO> getModelo() {
        return modelo;
    }

    public void setModelo(List<ModeloFranquiciaDTO> modelo) {
        this.modelo = modelo;
    }

    @Override
    public String toString() {
        return "Metadata{" + "disciplinas=" + disciplinas + ", ejes=" + ejes + ", ejecuta=" + ejecuta + ", ejesModelo=" + ejesModelo + ", frecuenciaMedicion=" + frecuenciaMedicion + ", status=" + status + ", tipoMedicion=" + tipoMedicion + ", categorias=" + categorias + ", franquicias=" + franquicias + ", colorimetria=" + colorimetria + ", perfiles=" + perfiles + ", negocios=" + negocios + ", driverExperienciaDTO=" + driverExperienciaDTO + ", protocolos=" + protocolos + ", modelo=" + modelo + '}';
    }

}
