/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author cescobarh
 */
public class DriverExperienciaDTO {

    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonProperty(value = "value")
    private Integer value;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "DriverExperienciaDTO{" + "descripcion=" + descripcion + ", value=" + value + '}';
    }

}
