/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EjeModeloDTO {

    @JsonProperty(value = "id_eje_modelo")
    private Integer idModEje;

    @JsonProperty(value = "id_eje")
    private Integer idEje;

    @JsonProperty(value = "id_modelo")
    private Integer idModelo;

    @JsonProperty(value = "ponderacion")
    private Integer ponderacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_status")
    private Integer idStatus;

    @JsonProperty(value = "disciplinas_eje")
    private List<DisciplinaEjeDTO> disciplinasEje;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public EjeModeloDTO() {
    }

    public Integer getIdModEje() {
        return idModEje;
    }

    public void setIdModEje(BigDecimal idModEje) {
        this.idModEje = (idModEje == null ? null : idModEje.intValue());
    }

    public Integer getIdModelo() {
        return idModelo;
    }

    public void setIdModelo(BigDecimal idModelo) {
        this.idModelo = (idModelo == null ? null : idModelo.intValue());
    }

    public Integer getIdEje() {
        return idEje;
    }

    public void setIdEje(BigDecimal idEje) {
        this.idEje = (idEje == null ? null : idEje.intValue());
    }

    public Integer getPonderacion() {
        return ponderacion;
    }

    public void setPonderacion(BigDecimal ponderacion) {
        this.ponderacion = (ponderacion == null ? null : ponderacion.intValue());
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(BigDecimal idStatus) {
        this.idStatus = (idStatus == null ? null : idStatus.intValue());
    }

    public List<DisciplinaEjeDTO> getDisciplinasEje() {
        return disciplinasEje;
    }

    public void setDisciplinasEje(List<DisciplinaEjeDTO> disciplinasEje) {
        this.disciplinasEje = disciplinasEje;
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "EjeModeloDTO{" + "idModEje=" + idModEje + ", idEje=" + idEje + ", idModelo=" + idModelo + ", ponderacion=" + ponderacion + ", idStatus=" + idStatus + ", disciplinasEje=" + disciplinasEje + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
