/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.util;

/**
 *
 * @author cescobarh
 */
public enum Relasionship {

    /**
     *
     */
    USUARIO("/plataforma-franquicia-core/service/usuario/secure/get"),
    MODELO("/plataforma-franquicia-core/service/modelo/secure/get"),
    EJE("/plataforma-franquicia-core/service/eje/secure/get"),
    TIPO_MEDICION("/plataforma-franquicia-core/service/tipo/medicion/secure/get"),
    FRECUENCIA_MEDICION("/plataforma-franquicia-core/service/frecuencia/medicion/secure/get"),
    DISCIPLINA("/plataforma-franquicia-core/service/disciplina/secure/get/"),
    STATUS("/plataforma-franquicia-core/service/status/secure/get/"),
    EJECUTA("/plataforma-franquicia-core/service/ejecuta/secure/get"),
    EJES_MODELO("/plataforma-franquicia-core/service/ejes/modelo/secure/get"),
    DISCIPLINAS_MODELO("/plataforma-franquicia-core/service/disciplinas/eje/secure/get"),
    COLORIMETRIA("/plataforma-franquicia-core/service/colorimetria/secure/get"),
    CATEGORIA("/plataforma-franquicia-core/service/categoria/secure/get/negocio"),
    CARGA_PUNTOS_CONTACTO_BY_CECO("/checklist/cargaServices/cargaPuntoContactoByCeco.json"),
    FORMATO("/plataforma-franquicia-core/service/formato/secure/get/"),
    PERFILES_USUARIO("/plataforma-franquicia-core/service/perfil/secure/get/usuario"),
    PROTOCOLO("/plataforma-franquicia-core/service/protocolo/secure/get"),
    NEGOCIO("/plataforma-franquicia-core/service/negocio/secure/get"),
    NEGOCIOS_FRANQUICIA("/plataforma-franquicia-core/service/negocio/secure/franquicia/get"),
    FRANQUICIA("/plataforma-franquicia-core/service/franquicia/secure/get"),
    FRANQUICIADATA("/plataforma-franquicia-core/service/franquicia/secure/get/data"),
    ESCRIBIR_ARCHIVO("/plataforma-franquicia-core/service/file/secure/write"),
    ESCRIBIR_DIRECTORIO("/plataforma-franquicia-core/service/file/secure/write/directory"),
    LEER_DIRECTORIO("/plataforma-franquicia-core/service/file/secure/read/directory"),
    LEER_TREE_DIRECTORIO("/plataforma-franquicia-core/service/file/secure/read/tree/directory"),
    LEER_ARCHIVO_B64("/plataforma-franquicia-core/service/file/secure/read/b64"),
    LEER_ARCHIVO("/plataforma-franquicia-core/service/file/secure/read"),
    MODELOS_FRANQUICIA_DATA("/plataforma-franquicia-core/service/modelos/franquicia/secure/get/data"),
    DISCIPLINAS_NEGOCIOS("/plataforma-franquicia-core/service/disciplinas/negocios/secure/get"),
    PROTOCOLO_DISCIPLINA("/plataforma-franquicia-core/service/protocolo/secure/disciplina/get"),
    JSON_CONVERT("/plataforma-franquicia-core/service/json/convert/secure"),
    MODULO("/plataforma-franquicia-core/service/modulo/secure/get/"),
    MODULO_FLUJO("/plataforma-franquicia-core/service/modulo/flujo/secure/get/"),
    DISCIPLINA_FRANQUICIA("/plataforma-franquicia-core/service/franquicia/disciplina/secure/get/"),
    DASHBOARD_FRANQUICIA("/plataforma-franquicia-core/service/franquicia/dashboard/secure/parametro/");

    private final String urlService;

    private Relasionship(String urlService) {
        this.urlService = urlService;
    }

    /**
     *
     * @param path
     * @return
     */
    public String url(String path) {
        return path + urlService;
    }

}
