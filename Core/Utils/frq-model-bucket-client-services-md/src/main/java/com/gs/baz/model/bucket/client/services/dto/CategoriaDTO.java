/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CategoriaDTO {

    @JsonProperty(value = "id_categoria")
    private Integer idCategoria;

    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonProperty(value = "id_negocio")
    private Integer idNegocio;

    @JsonProperty(value = "id_status")
    private Integer idStatus;

    public CategoriaDTO() {
    }

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(BigDecimal idCategoria) {
        this.idCategoria = (idCategoria == null ? null : idCategoria.intValue());
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(BigDecimal idNegocio) {
        this.idNegocio = (idNegocio == null ? null : idNegocio.intValue());
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(BigDecimal idStatus) {
        this.idStatus = (idStatus == null ? null : idStatus.intValue());
    }

    @Override
    public String toString() {
        return "CategoriaDTO{" + "idCategoria=" + idCategoria + ", descripcion=" + descripcion + ", idNegocio=" + idNegocio + ", idStatus=" + idStatus + '}';
    }

}
