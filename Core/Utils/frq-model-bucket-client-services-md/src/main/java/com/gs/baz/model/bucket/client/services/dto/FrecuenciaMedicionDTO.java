/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FrecuenciaMedicionDTO {

    @JsonProperty(value = "id_frecuencia_medicion")
    private Integer idFrecuenciaMedicion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "descripcion")
    private String descripcion;

    public FrecuenciaMedicionDTO() {
    }

    public Integer getIdFrecuenciaMedicion() {
        return idFrecuenciaMedicion;
    }

    public void setIdFrecuenciaMedicion(BigDecimal idFrecuenciaMedicion) {
        this.idFrecuenciaMedicion = (idFrecuenciaMedicion == null ? null : idFrecuenciaMedicion.intValue());
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "FrecuenciaMedicionDTO{" + "idFrecuenciaMedicion=" + idFrecuenciaMedicion + ", descripcion=" + descripcion + '}';
    }

}
