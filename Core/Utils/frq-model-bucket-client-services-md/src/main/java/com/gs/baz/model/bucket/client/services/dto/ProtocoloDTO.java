/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
public class ProtocoloDTO {

    @JsonProperty(value = "id_protocolo")
    private Integer idProtocolo;

    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonProperty(value = "observacion")
    private String observacion;

    @JsonProperty(value = "status")
    private Integer status;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_protocolo_disciplina")
    private Integer idProtocoloDisciplina;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "id_disciplina_negocio")
    private Integer idDisciplinaNegocio;

    public ProtocoloDTO() {
    }

    public Integer getIdProtocolo() {
        return idProtocolo;
    }

    public void setIdProtocolo(BigDecimal idProtocolo) {
        this.idProtocolo = (idProtocolo == null ? null : idProtocolo.intValue());
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(BigDecimal status) {
        this.status = (status == null ? null : status.intValue());
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getIdProtocoloDisciplina() {
        return idProtocoloDisciplina;
    }

    public void setIdProtocoloDisciplina(BigDecimal idProtocoloDisciplina) {
        this.idProtocoloDisciplina = (idProtocoloDisciplina == null ? null : idProtocoloDisciplina.intValue());
    }

    public Integer getIdDisciplinaNegocio() {
        return idDisciplinaNegocio;
    }

    public void setIdDisciplinaNegocio(BigDecimal idDisciplinaNegocio) {
        this.idDisciplinaNegocio = (idDisciplinaNegocio == null ? null : idDisciplinaNegocio.intValue());
    }

    @Override
    public String toString() {
        return "ProtocoloDTO{" + "idProtocolo=" + idProtocolo + ", descripcion=" + descripcion + ", observacion=" + observacion + ", status=" + status + ", idProtocoloDisciplina=" + idProtocoloDisciplina + ", idDisciplinaNegocio=" + idDisciplinaNegocio + '}';
    }

}
