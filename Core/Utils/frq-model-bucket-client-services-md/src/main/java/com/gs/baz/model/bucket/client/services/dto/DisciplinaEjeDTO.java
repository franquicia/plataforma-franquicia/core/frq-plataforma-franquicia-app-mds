/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DisciplinaEjeDTO {

    @JsonProperty(value = "id_disciplina_eje")
    private Integer idDisciplinaEje;

    @JsonProperty(value = "id_eje_modelo")
    private Integer idModeloEje;

    @JsonProperty(value = "id_disciplina")
    private Integer idDisciplina;

    @JsonProperty(value = "id_status")
    private Integer idStatus;

    @JsonProperty(value = "id_tipo_medicion")
    private Integer idTipoMedicion;

    @JsonProperty(value = "id_frecuencia_medicion")
    private Integer idFrecuenciaMedicion;

    @JsonProperty(value = "id_ejecuta")
    private Integer idEjecuta;

    @JsonProperty(value = "driver_experiencia")
    private Integer driverExperiencia;

    @JsonProperty(value = "ponderacion")
    private Integer ponderacion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "action")
    private String action;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_usuario")
    private String modUsuario;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "mod_fecha")
    private String modFecha;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "inserted")
    private Boolean inserted;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "updated")
    private Boolean updated;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "deleted")
    private Boolean deleted;

    public DisciplinaEjeDTO() {
    }

    public Integer getIdDisciplinaEje() {
        return idDisciplinaEje;
    }

    public void setIdDisciplinaEje(BigDecimal idDisciplinaEje) {
        this.idDisciplinaEje = (idDisciplinaEje == null ? null : idDisciplinaEje.intValue());
    }

    public Integer getIdModeloEje() {
        return idModeloEje;
    }

    public void setIdModeloEje(BigDecimal idModeloEje) {
        this.idModeloEje = (idModeloEje == null ? null : idModeloEje.intValue());
    }

    public Integer getIdDisciplina() {
        return idDisciplina;
    }

    public void setIdDisciplina(BigDecimal idDisciplina) {
        this.idDisciplina = (idDisciplina == null ? null : idDisciplina.intValue());
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(BigDecimal idStatus) {
        this.idStatus = (idStatus == null ? null : idStatus.intValue());
    }

    public Integer getIdTipoMedicion() {
        return idTipoMedicion;
    }

    public void setIdTipoMedicion(BigDecimal idTipoMedicion) {
        this.idTipoMedicion = (idTipoMedicion == null ? null : idTipoMedicion.intValue());
    }

    public Integer getIdFrecuenciaMedicion() {
        return idFrecuenciaMedicion;
    }

    public void setIdFrecuenciaMedicion(BigDecimal idFrecuenciaMedicion) {
        this.idFrecuenciaMedicion = (idFrecuenciaMedicion == null ? null : idFrecuenciaMedicion.intValue());
    }

    public Integer getIdEjecuta() {
        return idEjecuta;
    }

    public void setIdEjecuta(BigDecimal idEjecuta) {
        this.idEjecuta = (idEjecuta == null ? null : idEjecuta.intValue());
    }

    public Integer getDriverExperiencia() {
        return driverExperiencia;
    }

    public void setDriverExperiencia(BigDecimal driverExperiencia) {
        this.driverExperiencia = (driverExperiencia == null ? null : driverExperiencia.intValue());
    }

    public Integer getPonderacion() {
        return ponderacion;
    }

    public void setPonderacion(BigDecimal ponderacion) {
        this.ponderacion = (ponderacion == null ? null : ponderacion.intValue());
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getModUsuario() {
        return modUsuario;
    }

    public void setModUsuario(String modUsuario) {
        this.modUsuario = modUsuario;
    }

    public String getModFecha() {
        return modFecha;
    }

    public void setModFecha(String modFecha) {
        this.modFecha = modFecha;
    }

    public Boolean getInserted() {
        return inserted;
    }

    public void setInserted(Boolean inserted) {
        this.inserted = inserted;
    }

    public Boolean getUpdated() {
        return updated;
    }

    public void setUpdated(Boolean updated) {
        this.updated = updated;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "DisciplinaEjeDTO{" + "idDisciplinaEje=" + idDisciplinaEje + ", idModeloEje=" + idModeloEje + ", idDisciplina=" + idDisciplina + ", idStatus=" + idStatus + ", idTipoMedicion=" + idTipoMedicion + ", idFrecuenciaMedicion=" + idFrecuenciaMedicion + ", idEjecuta=" + idEjecuta + ", driverExperiencia=" + driverExperiencia + ", ponderacion=" + ponderacion + ", action=" + action + ", modUsuario=" + modUsuario + ", modFecha=" + modFecha + ", inserted=" + inserted + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

}
