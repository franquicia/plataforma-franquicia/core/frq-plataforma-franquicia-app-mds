/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author B73601
 */
public class DefaultRestTemplate {

    @Autowired
    protected RestTemplate restTemplate;
    protected final ObjectMapper objectMapper = new ObjectMapper();
    protected ResponseEntity<String> responseString;
    protected ResponseEntity<byte[]> responseBytes;
    protected JsonNode responseStringBody;
    protected String basePath;
    protected HttpEntity<Object> httpEntity;

    public DefaultRestTemplate() {
        super();
    }

    /**
     *
     * @param basePath
     * @param httpEntity
     */
    public void init(String basePath, HttpEntity<Object> httpEntity) {
        this.basePath = basePath;
        this.httpEntity = httpEntity;
    }

}
