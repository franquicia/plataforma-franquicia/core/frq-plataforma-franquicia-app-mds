/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DisciplinaDTO {

    @JsonProperty(value = "id_disciplina")
    private Integer idDisciplina;

    @JsonProperty(value = "descripcion")
    private String descripcion;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "new_disciplina")
    private DisciplinaDTO newDisciplinaDTO;

    public DisciplinaDTO() {
    }

    public Integer getIdDisciplina() {
        return idDisciplina;
    }

    public void setIdDisciplina(BigDecimal idDisciplina) {
        this.idDisciplina = (idDisciplina == null ? null : idDisciplina.intValue());
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public DisciplinaDTO getNewDisciplinaDTO() {
        return newDisciplinaDTO;
    }

    public void setNewDisciplinaDTO(DisciplinaDTO newDisciplinaDTO) {
        this.newDisciplinaDTO = newDisciplinaDTO;
    }

    @Override
    public String toString() {
        return "DisciplinaDTO{" + "idDisciplina=" + idDisciplina + ", descripcion=" + descripcion + ", newDisciplinaDTO=" + newDisciplinaDTO + '}';
    }

}
