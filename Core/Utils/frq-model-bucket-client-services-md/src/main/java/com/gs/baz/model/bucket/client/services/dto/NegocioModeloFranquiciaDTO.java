/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author cescobarh
 */
public class NegocioModeloFranquiciaDTO {

    @JsonProperty(value = "id_negocio")
    private Integer idNegocio;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "ejes_modelo")
    private Object ejesNegocio;

    public NegocioModeloFranquiciaDTO() {
    }

    public Integer getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(Integer idNegocio) {
        this.idNegocio = idNegocio;
    }

    public Object getEjesNegocio() {
        return ejesNegocio;
    }

    public void setEjesNegocio(Object ejesNegocio) {
        this.ejesNegocio = ejesNegocio;
    }

    @Override
    public String toString() {
        return "NegocioDTO{" + "idNegocio=" + idNegocio + ", ejesNegocio=" + ejesNegocio + '}';
    }

}
