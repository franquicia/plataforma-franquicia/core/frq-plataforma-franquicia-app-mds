/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.rest;

import com.fasterxml.jackson.databind.JsonNode;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.ModelCodes;
import com.gs.baz.model.bucket.client.services.dto.FileGenericDTO;
import com.gs.baz.model.bucket.client.services.util.DefaultRestTemplate;
import com.gs.baz.model.bucket.client.services.util.Relasionship;
import java.io.IOException;
import java.util.List;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author B73601
 */
public class SubServiceArchivo extends DefaultRestTemplate {

    public List<FileGenericDTO> getDirectoryGeneric(boolean forceDeveloping) throws CustomException {
        try {
            if (forceDeveloping) {
                basePath += "?force_developing";
            }
            responseString = restTemplate.exchange(Relasionship.LEER_DIRECTORIO.url(basePath), HttpMethod.POST, httpEntity, String.class);
            if (responseString.getStatusCode().equals(HttpStatus.OK)) {
                responseStringBody = objectMapper.readValue((String) responseString.getBody(), JsonNode.class);
                if (responseStringBody.get("status_code").asInt() == HttpStatus.OK.value()) {
                    return objectMapper.readValue(responseStringBody.get("data").toString(), objectMapper.getTypeFactory().constructCollectionLikeType(List.class, FileGenericDTO.class));
                } else {
                    throw new CustomException(ModelCodes.ERROR_TO_WRITE_FILE.detalle("archivo response code error from service"));
                }
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_WRITE_FILE.detalle("archivo response code error from service"));
            }
        } catch (IOException | CustomException ex) {
            throw new CustomException(ex);
        }
    }

    public FileGenericDTO postDirectoryGeneric(boolean forceDeveloping) throws CustomException {
        try {
            if (forceDeveloping) {
                basePath += "?force_developing";
            }
            responseString = restTemplate.exchange(Relasionship.ESCRIBIR_DIRECTORIO.url(basePath), HttpMethod.POST, httpEntity, String.class);
            if (responseString.getStatusCode().equals(HttpStatus.OK)) {
                responseStringBody = objectMapper.readValue((String) responseString.getBody(), JsonNode.class);
                if (responseStringBody.get("status_code").asInt() == HttpStatus.OK.value()) {
                    return objectMapper.readValue(responseStringBody.get("data").toString(), FileGenericDTO.class);
                } else {
                    throw new CustomException(ModelCodes.ERROR_TO_WRITE_FILE.detalle("archivo response code error from service"));
                }
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_WRITE_FILE.detalle("archivo response code error from service"));
            }
        } catch (IOException | CustomException ex) {
            throw new CustomException(ex);
        }
    }

    public FileGenericDTO postFileGeneric(boolean forceDeveloping) throws CustomException {
        try {
            if (forceDeveloping) {
                basePath += "?force_developing";
            }
            responseString = restTemplate.exchange(Relasionship.ESCRIBIR_ARCHIVO.url(basePath), HttpMethod.POST, httpEntity, String.class);
            if (responseString.getStatusCode().equals(HttpStatus.OK)) {
                responseStringBody = objectMapper.readValue((String) responseString.getBody(), JsonNode.class);
                if (responseStringBody.get("status_code").asInt() == HttpStatus.OK.value()) {
                    return objectMapper.readValue(responseStringBody.get("data").toString(), FileGenericDTO.class);
                } else {
                    throw new CustomException(ModelCodes.ERROR_TO_WRITE_FILE.detalle("archivo response code error from service"));
                }
            } else {
                throw new CustomException(ModelCodes.ERROR_TO_WRITE_FILE.detalle("archivo response code error from service"));
            }
        } catch (IOException | CustomException ex) {
            throw new CustomException(ex);
        }
    }

    public FileGenericDTO getFileGeneric(String fullPath, boolean forceDeveloping) throws CustomException {
        try {
            if (forceDeveloping) {
                basePath += "?force_developing";
            }
            responseString = restTemplate.exchange(Relasionship.LEER_ARCHIVO_B64.url(basePath) + "/" + fullPath, HttpMethod.GET, httpEntity, String.class);
            if (responseString.getStatusCode().equals(HttpStatus.OK)) {
                responseStringBody = objectMapper.readValue(responseString.getBody(), JsonNode.class);
                if (responseStringBody.get("status_code").asInt() == HttpStatus.OK.value()) {
                    return objectMapper.readValue(responseStringBody.get("data").toString(), FileGenericDTO.class);
                } else {
                    throw new CustomException(ModelCodes.ERROR_TO_READ_FILE.detalle("archivo " + fullPath + " response code error from service"));
                }
            } else {
                throw new CustomException(ModelCodes.EMPTY_RESPONSE_DATA.detalle("archivo " + fullPath + " response code error from service"));
            }
        } catch (IOException | CustomException ex) {
            throw new CustomException(ex);
        }
    }

    public ResponseEntity<byte[]> getFileBytes(String fullPath, boolean forceDeveloping) throws CustomException {
        try {
            if (forceDeveloping) {
                basePath += "?force_developing";
            }
            responseBytes = restTemplate.exchange(Relasionship.LEER_ARCHIVO.url(basePath) + "/" + fullPath, HttpMethod.GET, httpEntity, byte[].class);
            if (responseBytes.getStatusCode().equals(HttpStatus.OK)) {
                return responseBytes;
            } else {
                throw new CustomException(ModelCodes.EMPTY_RESPONSE_DATA.detalle("archivo " + fullPath + " response code error from service"));
            }
        } catch (CustomException ex) {
            throw new CustomException(ex);
        }
    }
}
