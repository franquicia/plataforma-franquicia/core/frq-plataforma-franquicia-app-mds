/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TipoMedicionDTO {

    @JsonProperty(value = "id_tipo_medicion")
    private Integer idTipoMedicion;

    @JsonProperty(value = "descripcion")
    private String descripcion;

    public TipoMedicionDTO() {
    }

    public Integer getIdTipoMedicion() {
        return idTipoMedicion;
    }

    public void setIdTipoMedicion(BigDecimal idTipoMedicion) {
        this.idTipoMedicion = (idTipoMedicion == null ? null : idTipoMedicion.intValue());
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "TipoMedicionDTO{" + "idTipoMedicion=" + idTipoMedicion + ", descripcion=" + descripcion + '}';
    }

}
