/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.model.bucket.client.services.init;

import com.gs.baz.model.bucket.client.services.rest.SubServiceArchivo;
import com.gs.baz.model.bucket.client.services.rest.SubServiceCargaPuntoContactoByCeco;
import com.gs.baz.model.bucket.client.services.rest.SubServiceCategoria;
import com.gs.baz.model.bucket.client.services.rest.SubServiceColorimetria;
import com.gs.baz.model.bucket.client.services.rest.SubServiceDisciplina;
import com.gs.baz.model.bucket.client.services.rest.SubServiceDisciplinaFranquicia;
import com.gs.baz.model.bucket.client.services.rest.SubServiceDisciplinasEje;
import com.gs.baz.model.bucket.client.services.rest.SubServiceDisciplinasNegocio;
import com.gs.baz.model.bucket.client.services.rest.SubServiceDriverExperiencia;
import com.gs.baz.model.bucket.client.services.rest.SubServiceEje;
import com.gs.baz.model.bucket.client.services.rest.SubServiceEjecuta;
import com.gs.baz.model.bucket.client.services.rest.SubServiceEjesModelo;
import com.gs.baz.model.bucket.client.services.rest.SubServiceFormato;
import com.gs.baz.model.bucket.client.services.rest.SubServiceFranquicia;
import com.gs.baz.model.bucket.client.services.rest.SubServiceFrecuenciaMedicion;
import com.gs.baz.model.bucket.client.services.rest.SubServiceJsonConverter;
import com.gs.baz.model.bucket.client.services.rest.SubServiceModelosFranquicia;
import com.gs.baz.model.bucket.client.services.rest.SubServiceModulo;
import com.gs.baz.model.bucket.client.services.rest.SubServiceModuloFlujo;
import com.gs.baz.model.bucket.client.services.rest.SubServiceNegocio;
import com.gs.baz.model.bucket.client.services.rest.SubServicePerfil;
import com.gs.baz.model.bucket.client.services.rest.SubServiceProtocolo;
import com.gs.baz.model.bucket.client.services.rest.SubServiceStatus;
import com.gs.baz.model.bucket.client.services.rest.SubServiceTipoMedicion;
import com.gs.baz.model.bucket.client.services.rest.SubServiceUsuario;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author B73601
 */
@Configuration
@ComponentScan("com.gs.baz.model.bucket.client.services")
public class ConfigBucketServices {

    private final Logger logger = LogManager.getLogger();

    public ConfigBucketServices() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public SubServiceDisciplina subServiceDisciplina() {
        return new SubServiceDisciplina();
    }

    @Bean
    public SubServiceEje subServiceEje() {
        return new SubServiceEje();
    }

    @Bean
    public SubServiceEjecuta subServiceEjecuta() {
        return new SubServiceEjecuta();
    }

    @Bean
    public SubServiceFrecuenciaMedicion subServiceFrecuenciaMedicion() {
        return new SubServiceFrecuenciaMedicion();
    }

    @Bean
    public SubServiceStatus subServiceStatus() {
        return new SubServiceStatus();
    }

    @Bean
    public SubServiceTipoMedicion subServiceTipoMedicion() {
        return new SubServiceTipoMedicion();
    }

    @Bean
    public SubServiceEjesModelo subServiceEjesModelo() {
        return new SubServiceEjesModelo();
    }

    @Bean
    public SubServiceDisciplinasEje subServiceDisciplinasEje() {
        return new SubServiceDisciplinasEje();
    }

    @Bean
    public SubServiceCategoria subServiceCategoria() {
        return new SubServiceCategoria();
    }

    @Bean
    public SubServiceColorimetria subServiceColorimetria() {
        return new SubServiceColorimetria();
    }

    @Bean
    public SubServicePerfil subServicePerfil() {
        return new SubServicePerfil();
    }

    @Bean
    public SubServiceModelosFranquicia subServiceModelo() {
        return new SubServiceModelosFranquicia();
    }

    @Bean
    public SubServiceNegocio subServiceNegocio() {
        return new SubServiceNegocio();
    }

    @Bean
    public SubServiceUsuario subServiceUsuario() {
        return new SubServiceUsuario();
    }

    @Bean
    public SubServiceDriverExperiencia subServiceDriverExperiencia() {
        return new SubServiceDriverExperiencia();
    }

    @Bean
    public SubServiceProtocolo subServiceProtocolo() {
        return new SubServiceProtocolo();
    }

    @Bean
    public SubServiceArchivo subServiceArchivo() {
        return new SubServiceArchivo();
    }

    @Bean
    public SubServiceFranquicia subServiceFranquicia() {
        return new SubServiceFranquicia();
    }

    @Bean
    public SubServiceDisciplinasNegocio subServiceDisciplinasNegocio() {
        return new SubServiceDisciplinasNegocio();
    }

    @Bean
    public SubServiceFormato subServiceFormato() {
        return new SubServiceFormato();
    }

    @Bean
    public SubServiceCargaPuntoContactoByCeco subServiceCargaPuntoContactoByCeco() {
        return new SubServiceCargaPuntoContactoByCeco();
    }

    @Bean
    public SubServiceJsonConverter subServiceJsonConverter() {
        return new SubServiceJsonConverter();
    }

    @Bean
    public SubServiceDisciplinaFranquicia subServiceDisciplinaFranquicia() {
        return new SubServiceDisciplinaFranquicia();
    }

    @Bean
    public SubServiceModulo subServiceModulo() {
        return new SubServiceModulo();
    }

    @Bean
    public SubServiceModuloFlujo subServiceModuloFlujo() {
        return new SubServiceModuloFlujo();
    }

}
