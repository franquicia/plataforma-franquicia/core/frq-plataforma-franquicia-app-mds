/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.read.propiedades.commons;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.read.propiedades.dao.ReadPropiedadesDAOImpl;
import com.gs.baz.read.propiedades.dto.ReadPropiedadesDTO;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author cescobarh
 */
public class Propiedades {

    @Autowired
    private ReadPropiedadesDAOImpl readPropiedadesDAOImpl;

    private final Logger logger = LogManager.getLogger();

    public Object getByKey(String key) {
        try {
            List<ReadPropiedadesDTO> propiedades = readPropiedadesDAOImpl.selectRows(key);
            if (propiedades.size() == 1) {
                return propiedades.get(0).getValor();
            } else {
                List<String> values = new ArrayList<>();
                propiedades.forEach(item -> {
                    values.add(item.getValor());
                });
                return values;
            }
        } catch (CustomException ex) {
            logger.log(Level.TRACE, ex);
        }
        return null;
    }
}
