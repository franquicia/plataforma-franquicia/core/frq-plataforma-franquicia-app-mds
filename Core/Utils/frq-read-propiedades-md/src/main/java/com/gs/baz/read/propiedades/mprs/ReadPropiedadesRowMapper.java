package com.gs.baz.read.propiedades.mprs;

import com.gs.baz.read.propiedades.dto.ReadPropiedadesDTO;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author cescobarh
 */
public class ReadPropiedadesRowMapper implements RowMapper<ReadPropiedadesDTO> {

    private ReadPropiedadesDTO propiedades;

    @Override
    public ReadPropiedadesDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        propiedades = new ReadPropiedadesDTO();
        propiedades.setIdPropiedad(((BigDecimal) rs.getObject("FIIDPROP")));
        propiedades.setClave(rs.getString("FCCLAVE"));
        propiedades.setValor(rs.getString("FCVALOR"));
        propiedades.setEncriptado(((BigDecimal) rs.getObject("FIENCRIPTADO")));
        return propiedades;
    }
}
