package com.gs.baz.read.propiedades.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.read.propiedades.commons.Propiedades;
import com.gs.baz.read.propiedades.dao.ReadPropiedadesDAOImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.read.propiedades")
public class ConfigReadPropiedades {

    private final Logger logger = LogManager.getLogger();

    public ConfigReadPropiedades() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean(initMethod = "init")
    public ReadPropiedadesDAOImpl readPropiedadesDAOImpl() {
        return new ReadPropiedadesDAOImpl();
    }

    @Bean
    public Propiedades propiedades() {
        return new Propiedades();
    }

}
