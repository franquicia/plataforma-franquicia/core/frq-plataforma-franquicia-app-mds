package com.gs.baz.read.propiedades.dao;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.data.sources.dao.DefaultDAO;
import com.gs.baz.frq.data.sources.jbdc.DefaultJdbcCall;
import com.gs.baz.read.propiedades.dto.ReadPropiedadesDTO;
import com.gs.baz.read.propiedades.mprs.ReadPropiedadesRowMapper;
import com.gs.baz.read.propiedades.dao.util.GenericDAO;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

/**
 *
 * @author cescobarh
 */
@Component
public class ReadPropiedadesDAOImpl extends DefaultDAO implements GenericDAO<ReadPropiedadesDTO> {

    private DefaultJdbcCall jdbcSelect;
    private MapSqlParameterSource mapSqlParameterSource;
    private final Logger logger = LogManager.getLogger();
    private String schema;

    public void init() {

        schema = "MODFRANQ";

        jdbcSelect = (DefaultJdbcCall) new DefaultJdbcCall(this.mdfqrJdbcTemplate);
        jdbcSelect.withSchemaName(schema);
        jdbcSelect.withCatalogName("PAADMINPROP");
        jdbcSelect.withProcedureName("SP_SEL_PROP_VALOR");
        jdbcSelect.returningResultSet("RCL_INFO", new ReadPropiedadesRowMapper());
    }

    @Override
    public List<ReadPropiedadesDTO> selectRows(String key) throws CustomException {
        mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("PA_FCCLAVE", key);
        Map<String, Object> out = jdbcSelect.execute(mapSqlParameterSource);
        return (List<ReadPropiedadesDTO>) out.get("RCL_INFO");
    }

}
