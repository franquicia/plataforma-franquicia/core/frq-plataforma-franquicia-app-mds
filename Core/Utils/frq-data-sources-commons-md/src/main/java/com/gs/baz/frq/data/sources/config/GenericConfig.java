/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.data.sources.config;

import com.gs.baz.frq.data.sources.pg.JdbcTemplatePg;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jndi.JndiObjectFactoryBean;

/**
 *
 * @author cescobarh
 */
@Configuration
@PropertySource("classpath:enviroment.properties")
public class GenericConfig {

    @Value("${frq.motordb}")
    private String motordb;

    @Value("${frq.motordb.mdfqr-name-ds}")
    private String mdfqrNameDS;

    @Value("${frq.motordb.frq-name-ds}")
    private String frqNameDS;

    @Value("${frq.motordb.gtn-name-ds}")
    private String gtnNameDS;

    private final Logger logger = LogManager.getLogger();

    public GenericConfig() {
        logger.info("Loading GenericConfig " + getClass().getName() + "...!");
    }

    @Bean(name = "mdfqrDS")
    public DataSource mdfqrDS() throws IllegalArgumentException, NamingException {
        JndiObjectFactoryBean jndiObjectFactoryBean = new JndiObjectFactoryBean();
        jndiObjectFactoryBean.setJndiName("java:jboss/datasources/" + mdfqrNameDS);
        jndiObjectFactoryBean.setProxyInterface(DataSource.class);
        jndiObjectFactoryBean.setResourceRef(true);
        jndiObjectFactoryBean.setLookupOnStartup(false);
        jndiObjectFactoryBean.afterPropertiesSet();
        DataSource dataSource = (DataSource) jndiObjectFactoryBean.getObject();
        return dataSource;
    }

    @Bean(name = "mdfqrJdbcTemplate")
    public Object mdfqrJdbcTemplate(@Qualifier("mdfqrDS") DataSource mdfqrDS) {
        if (motordb.equals("pg")) {
            JdbcTemplatePg jdbcTemplatePg = new JdbcTemplatePg(mdfqrDS);
            jdbcTemplatePg.setResultsMapCaseInsensitive(true);
            return jdbcTemplatePg;
        } else {
            return new JdbcTemplate(mdfqrDS);
        }
    }

    @Bean(name = "frqDS")
    public DataSource frqDS() throws IllegalArgumentException, NamingException {
        JndiObjectFactoryBean jndiObjectFactoryBean = new JndiObjectFactoryBean();
        jndiObjectFactoryBean.setJndiName("java:jboss/datasources/" + frqNameDS);
        jndiObjectFactoryBean.setProxyInterface(DataSource.class);
        jndiObjectFactoryBean.setResourceRef(true);
        jndiObjectFactoryBean.setLookupOnStartup(false);
        jndiObjectFactoryBean.afterPropertiesSet();
        return (DataSource) jndiObjectFactoryBean.getObject();
    }

    @Bean(name = "frqJdbcTemplate")
    public Object frqJdbcTemplate(@Qualifier("frqDS") DataSource frqDS) {
        if (motordb.equals("pg")) {
            JdbcTemplatePg jdbcTemplatePg = new JdbcTemplatePg(frqDS);
            jdbcTemplatePg.setResultsMapCaseInsensitive(true);
            return jdbcTemplatePg;
        } else {
            return new JdbcTemplate(frqDS);
        }
    }

    @Bean(name = "gtnDS")
    public DataSource gtnDS() throws IllegalArgumentException, NamingException {
        JndiObjectFactoryBean jndiObjectFactoryBean = new JndiObjectFactoryBean();
        jndiObjectFactoryBean.setJndiName("java:jboss/datasources/" + gtnNameDS);
        jndiObjectFactoryBean.setProxyInterface(DataSource.class);
        jndiObjectFactoryBean.setResourceRef(true);
        jndiObjectFactoryBean.setLookupOnStartup(false);
        jndiObjectFactoryBean.afterPropertiesSet();
        return (DataSource) jndiObjectFactoryBean.getObject();
    }

    @Bean(name = "gtnJdbcTemplate")
    public Object gtnJdbcTemplate(@Qualifier("gtnDS") DataSource gtnDS) {
        if (motordb.equals("pg")) {
            JdbcTemplatePg jdbcTemplatePg = new JdbcTemplatePg(gtnDS);
            jdbcTemplatePg.setResultsMapCaseInsensitive(true);
            return jdbcTemplatePg;
        } else {
            return new JdbcTemplate(gtnDS);
        }
    }

}
