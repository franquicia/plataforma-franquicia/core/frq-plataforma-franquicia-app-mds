package com.gs.baz.frq.data.sources.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 *
 * @author cescobarh
 */
public class DefaultDAO {

    @Autowired
    @Qualifier("mdfqrJdbcTemplate")
    protected Object mdfqrJdbcTemplate;

    @Autowired
    @Qualifier("gtnJdbcTemplate")
    protected Object gtnJdbcTemplate;

    @Autowired
    @Qualifier("frqJdbcTemplate")
    protected Object frqJdbcTemplate;

    public DefaultDAO() {
        super();
    }

}
