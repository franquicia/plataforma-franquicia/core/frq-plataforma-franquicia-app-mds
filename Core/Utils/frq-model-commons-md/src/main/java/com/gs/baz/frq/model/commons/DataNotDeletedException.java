/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.model.commons;

/**
 *
 * @author cescobarh
 */
public class DataNotDeletedException extends Exception {

    public enum Cause {

        ROW_NOT_FOUND("Regitro a borrar no encontrado");

        private final String value;

        Cause(String value) {
            this.value = value;
        }

        public String value() {
            return value;
        }
    }

    private Cause customCause;

    public DataNotDeletedException() {
        super();
    }

    public DataNotDeletedException(Cause cause) {
        super(cause.value());
        customCause = cause;
    }

    public DataNotDeletedException(String message) {
        super(message);
    }

    public DataNotDeletedException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataNotDeletedException(Throwable cause) {
        super(cause);
    }

    public Cause getCustomCause() {
        return this.customCause;
    }

}
