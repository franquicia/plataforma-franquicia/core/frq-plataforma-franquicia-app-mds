/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.model.commons;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;

/**
 *
 * @author cescobarh
 */
public class ErrorMessageDTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "status_code")
    private Integer statusCode;

    @JsonProperty(value = "track_code")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer trackCode;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "message")
    private String message;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "detail")
    private Object detail;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "time")
    private Date timestamp;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "details")
    private String details;

    public ErrorMessageDTO(Date timestamp, String message, String details) {
        super();
        this.timestamp = timestamp;
        this.message = message;
        this.details = details;
    }

    public ErrorMessageDTO() {
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getTrackCode() {
        return trackCode;
    }

    public void setTrackCode(Integer trackCode) {
        this.trackCode = trackCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getDetail() {
        return detail;
    }

    public void setDetail(Object detail) {
        this.detail = detail;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return "ErrorMessage{" + "statusCode=" + statusCode + ", trackCode=" + trackCode + ", message=" + message + ", detail=" + detail + ", timestamp=" + timestamp + ", details=" + details + '}';
    }

}
