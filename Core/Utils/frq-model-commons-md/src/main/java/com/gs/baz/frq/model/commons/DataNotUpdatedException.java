/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.model.commons;

/**
 *
 * @author cescobarh
 */
public class DataNotUpdatedException extends Exception {

    public enum Cause {

        VIOLATION_KEY("Datos duplicados"),
        ROW_NOT_FOUND("Registro a actualizar no encontrado");

        private final String value;

        Cause(String value) {
            this.value = value;
        }

        public String value() {
            return value;
        }
    }

    private Cause customCause;

    public DataNotUpdatedException() {
        super();
    }

    public DataNotUpdatedException(Cause cause) {
        super(cause.value());
        customCause = cause;
    }

    public DataNotUpdatedException(String message) {
        super(message);
    }

    public DataNotUpdatedException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataNotUpdatedException(Throwable cause) {
        super(cause);
    }

    public Cause getCustomCause() {
        return this.customCause;
    }

}
