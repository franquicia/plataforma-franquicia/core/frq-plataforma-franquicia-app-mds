/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.model.commons;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author cescobarh
 */
@Target({ElementType.METHOD, ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Specifications {

    public enum TypeOfString {

        SimpleString, Base64, NumericFloat, NumericDouble, NumericInteger, NumericBigDecimal;
    }

    boolean isSensible() default false;

    TypeOfString typeOfString() default TypeOfString.SimpleString;
}
