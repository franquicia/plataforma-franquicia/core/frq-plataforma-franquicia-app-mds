/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.model.commons;

/**
 *
 * @author MADA
 */
public enum PathsResources {

    DEFAULT("/"),
    NEGOCIOS("/negocios/");

    private final String root = "/franquicia";
    private final String enviroment = "";
    private final String dir = "/plataforma_franquicia";
    private final String resources = "/resources";
    private final String module;

    PathsResources(String module) {
        this.module = module;
    }

    public String getRootPath() {
        return root + enviroment + module;
    }

    public String getPath() {
        return root + enviroment + dir + resources + module;
    }

}
