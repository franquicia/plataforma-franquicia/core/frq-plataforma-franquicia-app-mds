/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.model.commons;

/**
 *
 * @author cescobarh
 */
public enum GlobalValues {

    /**
     *
     */
    TIME_OUT_SESSION_SECONDS;

    /**
     *
     * @return
     */
    public String value() {
        if (this.equals(TIME_OUT_SESSION_SECONDS)) {
            return "15";
        }
        return null;
    }
}
