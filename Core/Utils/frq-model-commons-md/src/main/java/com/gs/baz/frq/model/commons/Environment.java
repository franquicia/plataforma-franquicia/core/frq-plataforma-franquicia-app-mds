/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.model.commons;

/**
 *
 * @author cescobarh
 */
public enum Environment {

    /**
     *
     */
    CURRENT,
    /**
     *
     */
    LOCAL,
    /**
     *
     */
    DEVELOPING,
    /**
     *
     */
    QUALITY,
    /**
     *
     */
    PRODUCTION;
    final String ipHost = Util.getIPLocal();
    final String hostName = Util.getHostName();

    /**
     *
     * @return
     */
    public Environment value() {
        if (ipHost.endsWith(".74") || ipHost.endsWith(".75") || ipHost.endsWith(".76") || ipHost.endsWith(".77")) {
            return PRODUCTION;
        } else if (hostName.contains("10-96-140")) {
            return DEVELOPING;
        } else if (ipHost.contains("172.17.0")) {
            return QUALITY;
        } else {
            return LOCAL;
        }
    }

    public Environment valueReal() {
        if (ipHost.endsWith(".74") || ipHost.endsWith(".75") || ipHost.endsWith(".76") || ipHost.endsWith(".77")) {
            return PRODUCTION;
        } else if (hostName.contains("10-96-140")) {
            return DEVELOPING;
        } else if (ipHost.contains("172.17.0")) {
            return QUALITY;
        } else {
            return LOCAL;
        }
    }

}
