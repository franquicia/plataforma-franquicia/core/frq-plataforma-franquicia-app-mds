/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.model.commons.ver;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 *
 * @author cescobarh
 */
public class Version {

    final private transient SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
    final private transient static Logger logger = LogManager.getLogger();
    private VersionDTO version;
    private Integer buildNumber;
    private Calendar buildDate;
    private Calendar deployedDate;

    public VersionDTO getVersion() {
        buildDate = Calendar.getInstance();
        deployedDate = Calendar.getInstance();
        Resource resource = new ClassPathResource("build.properties");
        try (InputStream build = resource.getInputStream()) {
            version = new VersionDTO();
            Path file = resource.getFile().toPath();
            BasicFileAttributes attr = Files.readAttributes(file, BasicFileAttributes.class);
            Properties prop = new Properties();
            prop.load(build);
            buildDate.setTimeInMillis(Long.parseLong(prop.getProperty("timestamp")));
            deployedDate.setTimeInMillis(attr.creationTime().toMillis());
            buildNumber = Integer.parseInt(prop.getProperty("buildNumber"));
            version.setAppName(prop.getProperty("nameApp"));
            version.setBuildDate(formatter.format(buildDate.getTime()));
            version.setDeployedDate(formatter.format(deployedDate.getTime()));
            version.setBuildNumber(buildNumber);
            version.setVersion(this.calculateVersion(buildNumber));
        } catch (Exception ex) {
            logger.info("No se pudo encontrar o leer el archivo build.properties " + ex.getMessage());
        }
        return version;
    }

    private String calculateVersion(long buildNumber) {
        int majorVersion = 0;
        int minorVersion = 0;
        int patchVersion = 0;
        long buildNumberAux = buildNumber;
        patchVersion = (short) (buildNumberAux % 51);
        buildNumberAux /= 51;
        minorVersion = (short) (buildNumberAux % 21);
        buildNumberAux /= 21;
        majorVersion = (short) (buildNumberAux % 11);
        return (majorVersion + "." + minorVersion + "." + patchVersion + "." + buildNumber);
    }

}
