/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.model.commons;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author cescobarh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserTokenDTO {

    @JsonProperty(value = "ceco")
    private String idCeco;

    @JsonProperty(value = "user_name")
    private String nombreUsuario;

    @JsonProperty(value = "nombre")
    private String nombre;

    public UserTokenDTO() {
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "UserTokenDTO{" + "idCeco=" + idCeco + ", nombreUsuario=" + nombreUsuario + ", nombre=" + nombre + '}';
    }

}
