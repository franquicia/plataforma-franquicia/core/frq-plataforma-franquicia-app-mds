/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.model.commons;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.Enumeration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author cescobarh
 */
public class Util {

    final private static Logger logger = LogManager.getLogger();

    public static String getIPLocal() {
        String localIP = null;
        try {
            Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
            for (NetworkInterface netint : Collections.list(nets)) {
                if (!netint.isLoopback()) {
                    Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
                    for (InetAddress inetAddress : Collections.list(inetAddresses)) {
                        if (!inetAddress.getHostAddress().contains(":")) {
                            localIP = inetAddress.getHostAddress();
                        }
                    }
                }
            }
        } catch (SocketException ex) {
            logger.info("No se logro leer la ip del host " + ex.getMessage());
        }
        if (localIP == null) {
            localIP = "127.0.0.1";
        }
        return localIP;
    }

    public static String getHostName() {
        String hostname = "localhost";
        try {
            InetAddress ip = InetAddress.getLocalHost();
            hostname = ip.getHostName();
        } catch (UnknownHostException ex) {
            logger.info("No se logro leer hostname " + ex.getMessage());
        }
        return hostname.toLowerCase();
    }

}
