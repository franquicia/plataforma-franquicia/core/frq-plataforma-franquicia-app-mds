/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.model.commons;

/**
 *
 * @author cescobarh
 */
public class DataNotInsertedException extends Exception {

    public enum Cause {

        VIOLATION_KEY("Datos duplicados");

        private final String value;

        Cause(String value) {
            this.value = value;
        }

        public String value() {
            return value;
        }
    }

    private Cause customCause;

    public DataNotInsertedException() {
        super();
    }

    public DataNotInsertedException(Cause cause) {
        super(cause.value());
        customCause = cause;
    }

    public DataNotInsertedException(String message) {
        super(message);
    }

    public DataNotInsertedException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataNotInsertedException(Throwable cause) {
        super(cause);
    }

    public Cause getCustomCause() {
        return this.customCause;
    }

}
