/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.model.commons.ver;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author cescobarh
 */
public class VersionDTO {

    @JsonProperty(value = "AppName")
    private String appName;

    @JsonProperty(value = "BuildNumber")
    private Integer buildNumber;

    @JsonProperty(value = "BuildDate")
    private String buildDate;

    @JsonProperty(value = "DeployedDate")
    private String deployedDate;

    @JsonProperty(value = "Version")
    private String version;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public Integer getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber(Integer buildNumber) {
        this.buildNumber = buildNumber;
    }

    public String getBuildDate() {
        return buildDate;
    }

    public void setBuildDate(String buildDate) {
        this.buildDate = buildDate;
    }

    public String getDeployedDate() {
        return deployedDate;
    }

    public void setDeployedDate(String deployedDate) {
        this.deployedDate = deployedDate;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "VersionDTO{" + "appName=" + appName + ", buildNumber=" + buildNumber + ", buildDate=" + buildDate + ", deployedDate=" + deployedDate + ", version=" + version + '}';
    }

}
