package com.gs.baz.frq.model.commons;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;

public class OutMessageDTO {

    private final transient ObjectMapper mapperToObject = new ObjectMapper();

    @JsonProperty(value = "status_code")
    private Integer statusCode;

    @JsonProperty(value = "track_code")
    @JsonInclude(Include.NON_NULL)
    private Integer trackCode;

    @JsonProperty(value = "message")
    @JsonInclude(Include.NON_NULL)
    private String message;

    @JsonProperty(value = "details")
    @JsonInclude(Include.NON_NULL)
    private String details;

    @JsonProperty(value = "description")
    @JsonInclude(Include.NON_NULL)
    private String description;

    @JsonProperty(value = "error")
    @JsonInclude(Include.NON_NULL)
    private ErrorMessageDTO error;

    @JsonProperty(value = "errors")
    @JsonInclude(Include.NON_NULL)
    private ArrayList<ErrorMessageDTO> errors;

    @JsonProperty(value = "data")
    @JsonInclude(Include.NON_NULL)
    private Object data;

    public OutMessageDTO() {
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getTrackCode() {
        return trackCode;
    }

    public void setTrackCode(Integer trackCode) {
        this.trackCode = trackCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ErrorMessageDTO getError() {
        return error;
    }

    public void setError(ErrorMessageDTO error) {
        this.error = error;
    }

    public ArrayList<ErrorMessageDTO> getErrors() {
        return errors;
    }

    public void setErrors(ArrayList<ErrorMessageDTO> errors) {
        this.errors = errors;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        try {
            return mapperToObject.writeValueAsString(this);
        } catch (JsonProcessingException ex) {
            LogManager.getLogger().trace(ex);
        }
        return "{}";
    }

}
