/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.model.commons;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Base64;
import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author cescobarh
 */
public class UserToken {

    private final HttpServletRequest request;
    private final ObjectMapper objectMapper = new ObjectMapper();

    public UserToken(HttpServletRequest request) {
        this.request = request;
    }

    public UserTokenDTO getUserToken() {
        String token = extractToken(request);
        if (token != null) {
            String infoJsonToken = this.getJsonInfoToken(token);
            try {
                return objectMapper.readValue(infoJsonToken, UserTokenDTO.class);
            } catch (JsonProcessingException ex) {
                return null;
            }
        }
        return null;
    }

    private String getJsonInfoToken(String token) {
        String[] tokenSplit = token.split("\\.");
        return new String(Base64.getDecoder().decode(tokenSplit[1]));
    }

    private String extractToken(HttpServletRequest request) {
        String token = extractHeaderToken(request);
        if (token == null) {
            token = request.getParameter("access_token");
        }
        return token;
    }

    private String extractHeaderToken(HttpServletRequest request) {
        Enumeration<String> headers = request.getHeaders("Authorization");
        while (headers.hasMoreElements()) {
            String value = headers.nextElement();
            if ((value.toLowerCase().startsWith("bearer"))) {
                String authHeaderValue = value.substring("bearer".length()).trim();
                int commaIndex = authHeaderValue.indexOf(',');
                if (commaIndex > 0) {
                    authHeaderValue = authHeaderValue.substring(0, commaIndex);
                }
                return authHeaderValue;
            }
        }
        return null;
    }

}
