/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.model.commons;

/**
 *
 * @author cescobarh
 */
public enum ModelCodes {

    /**
     *
     */
    TOKEN_NOT_VALID(2001),
    TOKEN_EXPIRED(2002),
    TOKEN_MISSING_IN_REQUEST(2003),
    TOKEN_FORMAT_NOT_VALID(2004),
    TOKEN_ERROR_TO_LOAD_FROM_STORAGE(2005),
    TOKEN_ERROR_TO_VERIFY(2006),
    JSON_INPUT_VALUE_NOT_VALID(2007),
    ATTRIBUTE_DATA_NOT_FOUND(2008),
    IMPOSSIBLE_DECRYPT_DATA(2009),
    JSON_MUST_BE_OBJECT_VALUE(2010),
    ERROR_TO_INSERT_DATA(3001),
    ERROR_TO_UPDATE_DATA(3002),
    ERROR_TO_DELETE_DATA(3003),
    ERROR_MISSING_DATA_ATTRIBUTE(3004),
    EMPTY_RESPONSE_DATA(3005),
    DATA_NOT_FOUND(3006),
    ERROR_TO_GENERATE_TOKEN_LOGIN_5(3007),
    ERROR_TO_REDIRECT_LOGIN_5(3008),
    ERROR_RESPONSE_TOKEN_REDIRECT_LOGIN_5(3009),
    ERROR_CODE_TOKEN_REDIRECT_LOGIN_5(3010),
    ERROR_DECRYPTING_DATA_RESPONSE_LOGIN_5(3011),
    ERROR_TO_WRITE_FILE(4001),
    ERROR_TO_READ_FILE(4002),
    ERROR_RESPONSE_SERVICE(4003);

    private final int code;
    private String descriptionCode;
    private String detalle;

    private ModelCodes(int code) {
        this.code = code;
    }

    /**
     *
     * @return
     */
    public int code() {
        return code;
    }

    /**
     *
     * @return
     */
    public String description() {
        if (ModelCodes.TOKEN_NOT_VALID.equals(this)) {
            descriptionCode = "The token was not valid";
        }
        if (ModelCodes.TOKEN_EXPIRED.equals(this)) {
            descriptionCode = "Token expired";
        }
        if (ModelCodes.TOKEN_MISSING_IN_REQUEST.equals(this)) {
            descriptionCode = "Token missing in this request";
        }
        if (ModelCodes.TOKEN_FORMAT_NOT_VALID.equals(this)) {
            descriptionCode = "Token format is not valid";
        }
        if (ModelCodes.TOKEN_ERROR_TO_LOAD_FROM_STORAGE.equals(this)) {
            descriptionCode = "Error to load token from storage";
        }
        if (ModelCodes.JSON_INPUT_VALUE_NOT_VALID.equals(this)) {
            descriptionCode = "JSON input value in body is not valid";
        }
        if (ModelCodes.ATTRIBUTE_DATA_NOT_FOUND.equals(this)) {
            descriptionCode = "Data attribute not found";
        }
        if (ModelCodes.IMPOSSIBLE_DECRYPT_DATA.equals(this)) {
            descriptionCode = "It was not possible to decrypt data";
        }
        if (ModelCodes.IMPOSSIBLE_DECRYPT_DATA.equals(this)) {
            descriptionCode = "JSON input value in body must be Object value";
        }
        if (ModelCodes.ERROR_TO_INSERT_DATA.equals(this)) {
            descriptionCode = "Error to insert data of entity";
        }
        if (ModelCodes.ERROR_TO_UPDATE_DATA.equals(this)) {
            descriptionCode = "Error to update data of entity";
        }
        if (ModelCodes.ERROR_TO_DELETE_DATA.equals(this)) {
            descriptionCode = "Error to delete data of entity";
        }
        if (ModelCodes.ERROR_MISSING_DATA_ATTRIBUTE.equals(this)) {
            descriptionCode = "Error data attribute is missing";
        }
        if (ModelCodes.EMPTY_RESPONSE_DATA.equals(this)) {
            descriptionCode = "Empty response data";
        }
        if (ModelCodes.DATA_NOT_FOUND.equals(this)) {
            descriptionCode = "Data not found";
        }
        if (ModelCodes.ERROR_TO_GENERATE_TOKEN_LOGIN_5.equals(this)) {
            descriptionCode = "Error to generate token Login5";
        }
        if (ModelCodes.ERROR_TO_REDIRECT_LOGIN_5.equals(this)) {
            descriptionCode = "Error to redirect Login5";
        }
        if (ModelCodes.ERROR_RESPONSE_TOKEN_REDIRECT_LOGIN_5.equals(this)) {
            descriptionCode = "Error response token redirect Login5";
        }
        if (ModelCodes.ERROR_CODE_TOKEN_REDIRECT_LOGIN_5.equals(this)) {
            descriptionCode = "Error code token redirect Login5";
        }
        if (ModelCodes.ERROR_DECRYPTING_DATA_RESPONSE_LOGIN_5.equals(this)) {
            descriptionCode = "Error decrypting data response Login5";
        }
        if (ModelCodes.ERROR_TO_WRITE_FILE.equals(this)) {
            descriptionCode = "Error writing file on disk";
        }
        if (ModelCodes.ERROR_TO_READ_FILE.equals(this)) {
            descriptionCode = "Error read file from disk";
        }
        if (ModelCodes.ERROR_RESPONSE_SERVICE.equals(this)) {
            descriptionCode = "Error on response service";
        }
        return descriptionCode;
    }

    public String detalle() {
        return detalle;
    }

    public ModelCodes detalle(String detalle) {
        this.detalle = detalle;
        return this;
    }

}
