/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.model.commons;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author cescobarh
 */
public class CustomException extends Exception {

    private ModelCodes modelCodes;
    private final Logger logger = LogManager.getLogger();

    public CustomException(Throwable cause) {
        super(cause);
    }

    public CustomException(ModelCodes modelCodes) {
        super("d3d23d[" + modelCodes.code() + "," + modelCodes.description() + "," + modelCodes.detalle() + "]");
        this.modelCodes = modelCodes;
    }

    public CustomException(ModelCodes modelCodes, Throwable cause) {
        super("d3d23d[" + modelCodes.code() + "," + modelCodes.description() + "," + modelCodes.detalle() + "]");
        logger.log(Level.TRACE, cause);
        this.modelCodes = modelCodes;
    }

    public ModelCodes getCode() {
        return modelCodes;
    }

}
