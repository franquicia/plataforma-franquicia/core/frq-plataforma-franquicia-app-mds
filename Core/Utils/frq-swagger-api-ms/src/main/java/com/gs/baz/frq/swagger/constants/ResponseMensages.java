/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.swagger.constants;

import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ResponseMessage;

/**
 *
 * @author cescobarh
 */
public class ResponseMensages {

    public static ResponseMessage RESPONSE_MESSAGE_200 = new ResponseMessageBuilder().code(200).message("Operación exitosa.").build();
    public static ResponseMessage RESPONSE_MESSAGE_201 = new ResponseMessageBuilder().code(201).message("Operación exitosa.").responseModel(new ModelRef("Respuesta201")).build();
    public static ResponseMessage RESPONSE_MESSAGE_400 = new ResponseMessageBuilder().code(400).message("Solicitud formada erroneamente.").responseModel(new ModelRef("Respuesta400")).build();
    public static ResponseMessage RESPONSE_MESSAGE_401 = new ResponseMessageBuilder().code(401).message("Recurso no autorizado.").responseModel(new ModelRef("Respuesta401")).build();
    public static ResponseMessage RESPONSE_MESSAGE_403 = new ResponseMessageBuilder().code(403).message("Recurso prohibido.").responseModel(new ModelRef("Respuesta403")).build();
    public static ResponseMessage RESPONSE_MESSAGE_404 = new ResponseMessageBuilder().code(404).message("Información no encontrada.").responseModel(new ModelRef("Respuesta404")).build();
    public static ResponseMessage RESPONSE_MESSAGE_405 = new ResponseMessageBuilder().code(405).message("Método http no permitido.").responseModel(new ModelRef("Respuesta405")).build();
    public static ResponseMessage RESPONSE_MESSAGE_415 = new ResponseMessageBuilder().code(415).message("Tipo de solicitud no soportado.").responseModel(new ModelRef("Respuesta415")).build();
    public static ResponseMessage RESPONSE_MESSAGE_429 = new ResponseMessageBuilder().code(429).message("Exceso de solicitudes.").responseModel(new ModelRef("Respuesta429")).build();
    public static ResponseMessage RESPONSE_MESSAGE_500 = new ResponseMessageBuilder().code(500).message("Error interno en el servidor.").responseModel(new ModelRef("Respuesta500")).build();
    public static ResponseMessage RESPONSE_MESSAGE_503 = new ResponseMessageBuilder().code(503).message("Servicio no disponible.").responseModel(new ModelRef("Respuesta503")).build();

    public static ResponseMessage getByStatus(int stauts) {
        if (stauts == 200) {
            return RESPONSE_MESSAGE_200;
        } else if (stauts == 201) {
            return RESPONSE_MESSAGE_201;
        } else if (stauts == 400) {
            return RESPONSE_MESSAGE_400;
        } else if (stauts == 401) {
            return RESPONSE_MESSAGE_401;
        } else if (stauts == 403) {
            return RESPONSE_MESSAGE_403;
        } else if (stauts == 404) {
            return RESPONSE_MESSAGE_404;
        } else if (stauts == 405) {
            return RESPONSE_MESSAGE_405;
        } else if (stauts == 415) {
            return RESPONSE_MESSAGE_415;
        } else if (stauts == 429) {
            return RESPONSE_MESSAGE_429;
        } else if (stauts == 500) {
            return RESPONSE_MESSAGE_500;
        } else if (stauts == 503) {
            return RESPONSE_MESSAGE_503;
        } else {
            return null;
        }
    }

}
