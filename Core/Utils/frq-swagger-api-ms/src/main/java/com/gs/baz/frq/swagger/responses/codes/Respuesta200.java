/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.swagger.responses.codes;

import com.gs.baz.frq.swagger.responses.RespuestaAPI;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
public class Respuesta200 implements RespuestaAPI {

    @ApiModelProperty(notes = "Mensaje de salida", example = "Operación exitosa.", position = 1)
    private String mensaje;
    @ApiModelProperty(notes = "Folio de seguimiento", example = "285839478929478282", position = 2)
    private String folio;
    @ApiModelProperty(notes = "Resultado de la petición solicitada", position = 3)
    private Object resultado;

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public Object getResultado() {
        return resultado;
    }

    public void setResultado(Object resultado) {
        this.resultado = resultado;
    }
}
