/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.swagger.bdd;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class ScenarioOutlineImp implements Scenario {

    private String name;
    private List<Step> steps;
    private List<Examples> examples;

    public ScenarioOutlineImp() {
    }

    public ScenarioOutlineImp(Scenario scenario) {
        this.name = scenario.getName();
        this.steps = scenario.getSteps();
        this.examples = scenario.getExamples();
    }

    public ScenarioOutlineImp(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public List<Step> getSteps() {
        return steps;
    }

    @Override
    public Step When(String description) {
        Step step = new Step(StepType.When, description);
        if (this.steps == null) {
            this.steps = new ArrayList<>();
        }
        this.steps.add(step);
        return step;
    }

    @Override
    public Step Given(String description) {
        Step step = new Step(StepType.Given, description);
        if (this.steps == null) {
            this.steps = new ArrayList<>();
        }
        this.steps.add(step);
        return step;
    }

    @Override
    public Step Then(String description) {
        Step step = new Step(StepType.Then, description);
        if (this.steps == null) {
            this.steps = new ArrayList<>();
        }
        this.steps.add(step);
        return step;
    }

    @Override
    public List<Examples> getExamples() {
        return examples;
    }

    @Override
    public Examples Examples(List<String> headers, List<List<String>> rowData, int identation) {
        Examples example = new Examples(headers, rowData, identation);
        if (this.examples == null) {
            this.examples = new ArrayList<>();
        }
        this.examples.add(example);
        return example;
    }

}
