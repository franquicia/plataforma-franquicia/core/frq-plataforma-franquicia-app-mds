/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.swagger.bdd;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class Step {

    private StepType stepType;
    private String descripction;
    private List<Connector> connectors;
    private Parameters parameters;

    public Step() {
    }

    public Step(StepType stepType, String descripction) {
        this.stepType = stepType;
        this.descripction = descripction;
    }

    public String name() {
        return stepType.name();
    }

    public String getDescripction() {
        return descripction;
    }

    public void setDescripction(String descripction) {
        this.descripction = descripction;
    }

    public List<Connector> getConnectors() {
        return connectors;
    }

    public Step And(String descripction) {
        Connector connector = new Connector(ConnectorType.And, descripction);
        if (this.connectors == null) {
            this.connectors = new ArrayList<>();
        }
        this.connectors.add(connector);
        return this;
    }

    public Connector But(String descripction) {
        Connector connector = new Connector(ConnectorType.But, descripction);
        if (this.connectors == null) {
            this.connectors = new ArrayList<>();
        }
        this.connectors.add(connector);
        return connector;
    }

    public Parameters Parameters(List<String> headers, List<List<String>> rowData, int identation) {
        this.parameters = new Parameters(headers, rowData, identation);
        return parameters;
    }

    public Parameters Parameters() {
        return this.parameters;
    }

    @Override
    public String toString() {
        return "            " + stepType.name() + " " + descripction + "\n" + connectors;
    }

}
