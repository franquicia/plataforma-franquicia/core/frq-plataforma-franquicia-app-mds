/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.swagger.responses.codes;

import com.gs.baz.frq.swagger.responses.ErrorDetalle;
import com.gs.baz.frq.swagger.responses.RespuestaAPI;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author cescobarh
 */
public class Respuesta400 implements RespuestaAPI {

    @ApiModelProperty(notes = "Código de salida", example = "400.sistemas-franquicia-api-name.104001", position = 1)
    private String codigo;
    @ApiModelProperty(notes = "Mensaje de salida", example = "Solicitud formada erroneamente.", position = 2)
    private String mensaje;
    @ApiModelProperty(notes = "Folio de seguimiento", example = "17020201110030616769", position = 3)
    private String folio;
    @ApiModelProperty(notes = "Información del error generado", example = "https://baz-developer.bancoazteca.com.mx/info#400.sistemas-franquicia-api-name.104001", position = 4)
    private String info;
    @ApiModelProperty(notes = "Información detallada del error generado", position = 5)
    private List<ErrorDetalle> detalles;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public List<ErrorDetalle> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<ErrorDetalle> detalles) {
        this.detalles = detalles;
    }

    @Override
    public String toString() {
        return "Respuesta400{" + "codigo=" + codigo + ", mensaje=" + mensaje + ", folio=" + folio + ", info=" + info + ", detalles=" + detalles + '}';
    }

}
