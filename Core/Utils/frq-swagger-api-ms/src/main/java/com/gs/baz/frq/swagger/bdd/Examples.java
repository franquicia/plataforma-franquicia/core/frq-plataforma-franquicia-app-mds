/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.swagger.bdd;

import java.util.List;

/**
 *
 * @author cescobarh
 */
public class Examples extends Parameters {

    public Examples(List<String> headers, List<List<String>> rowData, int identation) {
        super(headers, rowData, identation);
    }

    @Override
    public String toString() {
        return this.tableGenerator.generateTable(headers, rowData, identation);
    }

}
