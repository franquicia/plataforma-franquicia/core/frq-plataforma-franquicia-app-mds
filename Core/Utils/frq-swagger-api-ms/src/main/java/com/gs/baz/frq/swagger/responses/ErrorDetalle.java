/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.swagger.responses;

import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
public class ErrorDetalle implements RespuestaAPI {

    @ApiModelProperty(notes = "Mensaje de salida", example = "Error 'xxxx' favor de validar.")
    private String mensaje;

    public ErrorDetalle(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return "ErrorDetalle{" + "mensaje=" + mensaje + '}';
    }

}
