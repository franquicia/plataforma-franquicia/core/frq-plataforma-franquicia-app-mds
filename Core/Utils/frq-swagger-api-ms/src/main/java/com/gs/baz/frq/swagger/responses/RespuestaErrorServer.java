/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.swagger.responses;

import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author cescobarh
 */
public class RespuestaErrorServer implements RespuestaAPI {

    @ApiModelProperty(notes = "Código de salida", example = "500.sistemas-franquicia-api-name.105001", position = 1)
    private String codigo;
    @ApiModelProperty(notes = "Mensaje de salida", example = "Error interno en el servidor.", position = 2)
    private String mensaje;
    @ApiModelProperty(notes = "Folio de seguimiento", example = "17020201110030616769", position = 3)
    private String folio;
    @ApiModelProperty(notes = "Información del error generado", example = "https://baz-developer.bancoazteca.com.mx/info#500.sistemas-franquicia-api-name.105001", position = 4)
    private String info;

    public RespuestaErrorServer(String codigo, String mensaje, String folio) {
        this.codigo = codigo;
        this.mensaje = mensaje;
        this.folio = folio;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getInfo() {
        this.info = "https://baz-developer.bancoazteca.com.mx/info#" + this.codigo;
        return info;
    }

    @Override
    public String toString() {
        return "RespuestaError500{" + "codigo=" + codigo + ", mensaje=" + mensaje + ", folio=" + folio + ", info=" + info + '}';
    }

}
