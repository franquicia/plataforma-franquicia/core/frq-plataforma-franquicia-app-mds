/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.swagger.bdd;

import java.util.List;

/**
 *
 * @author cescobarh
 */
public class Parameters {

    protected final List<String> headers;
    protected final List< List<String>> rowData;
    protected final TableGenerator tableGenerator = new TableGenerator();
    protected final int identation;

    public Parameters(List<String> headers, List<List<String>> rowData, int identation) {
        this.headers = headers;
        this.rowData = rowData;
        this.identation = identation;
    }

    public List<String> headers() {
        return headers;
    }

    public List<List<String>> rowData() {
        return rowData;
    }

    @Override
    public String toString() {
        return tableGenerator.generateTable(headers, rowData, identation);
    }

}
