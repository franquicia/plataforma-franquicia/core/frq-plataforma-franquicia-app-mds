/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.swagger.api.mocker;

/**
 *
 * @author cescobarh
 */
public class ResponsesMethodsObject {

    private int httpStatus;
    private String mockFile;

    public ResponsesMethodsObject() {
    }

    public ResponsesMethodsObject(int httpStatus, String mockFile) {
        this.httpStatus = httpStatus;
        this.mockFile = mockFile;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(int httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getMockFile() {
        return mockFile;
    }

    public void setMockFile(String mockFile) {
        this.mockFile = mockFile;
    }

}
