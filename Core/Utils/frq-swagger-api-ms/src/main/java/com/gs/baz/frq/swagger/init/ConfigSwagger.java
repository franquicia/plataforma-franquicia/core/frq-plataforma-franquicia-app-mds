/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.swagger.init;

import com.fasterxml.classmate.TypeResolver;
import com.google.common.base.Predicates;
import com.gs.baz.frq.swagger.bi.SwaggerBI;
import com.gs.baz.frq.swagger.constants.ResponseMensages;
import com.gs.baz.frq.swagger.filters.SwaggerFilterCors;
import com.gs.baz.frq.swagger.responses.Respuesta;
import com.gs.baz.frq.swagger.responses.codes.Respuesta201;
import com.gs.baz.frq.swagger.responses.codes.Respuesta400;
import com.gs.baz.frq.swagger.responses.codes.Respuesta403;
import com.gs.baz.frq.swagger.responses.codes.Respuesta404;
import com.gs.baz.frq.swagger.responses.codes.Respuesta500;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import javax.servlet.ServletContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import static springfox.documentation.builders.PathSelectors.regex;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.GrantType;
import springfox.documentation.service.OAuth;
import springfox.documentation.service.ResourceOwnerPasswordCredentialsGrant;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.paths.RelativePathProvider;
import springfox.documentation.spring.web.plugins.ApiSelectorBuilder;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.DocExpansion;
import springfox.documentation.swagger.web.ModelRendering;
import springfox.documentation.swagger.web.OperationsSorter;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.TagsSorter;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@ComponentScan("com.gs.baz.frq.swagger")
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class ConfigSwagger {

    private final Logger logger = LogManager.getLogger();
    private final String apigeeHost;
    private final String host;
    private static final String CLIENT_ID = "frontendapp";
    private static final String CLIENT_SECRET = "12345";
    private static final String AUTH_SERVER = "http://127.0.0.1:8080/plataforma-franquicia-core/api/oauth/token";
    private final TypeResolver typeResolver;
    private final Contact contact = new Contact("Sistemas Franquicia", "http://franquiciags.com.mx/", "sistemas-franquicia@elektra.com.mx");
    private ApiInfo apiInfo;
    private final List<VendorExtension> vext = new ArrayList<>();
    private final String termsOfServiceUrl = "https://franquiciags.com.mx/termsOfService";
    private String description;

    @Autowired
    public ConfigSwagger(TypeResolver typeResolver) {
        logger.info("Loading " + getClass().getName() + "...!");
        this.typeResolver = typeResolver;
        this.apigeeHost = "dev-api.bancoazteca.com.mx:8080";
        this.host = getIPLocal() + "8080";
    }

    private String getIPLocal() {
        String localIP = null;
        try {
            Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
            for (NetworkInterface netint : Collections.list(nets)) {
                if (!netint.isLoopback()) {
                    Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
                    for (InetAddress inetAddress : Collections.list(inetAddresses)) {
                        if (!inetAddress.getHostAddress().contains(":")) {
                            localIP = inetAddress.getHostAddress();
                        }
                    }
                }
            }
        } catch (SocketException ex) {
            logger.info("No se logro leer la ip del host " + ex.getMessage());
        }
        if (localIP == null) {
            localIP = "127.0.0.1";
        }
        return localIP;
    }

    private ApiSelectorBuilder apiSelectorBuilder(boolean globalResponses) {
        Set<String> produces = new TreeSet<>(new ArrayList<>());
        produces.add(MediaType.APPLICATION_JSON_VALUE);
        Set<String> consumes = new TreeSet<>(new ArrayList<>());
        consumes.add(MediaType.APPLICATION_JSON_VALUE);

        Docket docket = new Docket(DocumentationType.SWAGGER_2);

        docket.pathMapping("/");
        docket.host(this.host);
        docket.forCodeGeneration(false);
        docket.genericModelSubstitutes(ResponseEntity.class);
        docket.useDefaultResponseMessages(false);
        docket.produces(produces);
        docket.consumes(consumes);
        docket.securityContexts(Arrays.asList(securityContext()));
        docket.securitySchemes(Arrays.asList(securitySchema()));
        if (globalResponses) {
            setGlobalResponses(docket);
        }
        ApiSelectorBuilder apiSelectorBuilder = docket.select();
        apiSelectorBuilder.paths(Predicates.not(regex(".*/service/security/oauth/.*")));
        return apiSelectorBuilder;
    }

    private void setGlobalResponsesSecurityApi(Docket docket) {
        List<ResponseMessage> responseGetMessages = new ArrayList<>();

        responseGetMessages.add(ResponseMensages.RESPONSE_MESSAGE_200);
        responseGetMessages.add(ResponseMensages.RESPONSE_MESSAGE_404);
        responseGetMessages.add(ResponseMensages.RESPONSE_MESSAGE_500);

        docket.globalResponseMessage(RequestMethod.GET, responseGetMessages);

        docket.additionalModels(typeResolver.resolve(Respuesta.class));
        docket.additionalModels(typeResolver.resolve(Respuesta400.class));
        docket.additionalModels(typeResolver.resolve(Respuesta404.class));
        docket.additionalModels(typeResolver.resolve(Respuesta500.class));
    }

    private void setGlobalResponses(Docket docket) {
        List<ResponseMessage> responsePostMessages = new ArrayList<>();
        List<ResponseMessage> responsePutMessages = new ArrayList<>();
        List<ResponseMessage> responseGetMessages = new ArrayList<>();
        List<ResponseMessage> responseDeleteMessages = new ArrayList<>();

        responseGetMessages.add(ResponseMensages.RESPONSE_MESSAGE_200);
        responseGetMessages.add(ResponseMensages.RESPONSE_MESSAGE_400);
        responseGetMessages.add(ResponseMensages.RESPONSE_MESSAGE_404);
        responseGetMessages.add(ResponseMensages.RESPONSE_MESSAGE_500);

        responsePostMessages.add(ResponseMensages.RESPONSE_MESSAGE_201);
        responsePostMessages.add(ResponseMensages.RESPONSE_MESSAGE_400);
        responsePostMessages.add(ResponseMensages.RESPONSE_MESSAGE_500);

        responsePutMessages.add(ResponseMensages.RESPONSE_MESSAGE_200);
        responsePutMessages.add(ResponseMensages.RESPONSE_MESSAGE_400);
        responsePutMessages.add(ResponseMensages.RESPONSE_MESSAGE_500);

        responseDeleteMessages.add(ResponseMensages.RESPONSE_MESSAGE_200);
        responseDeleteMessages.add(ResponseMensages.RESPONSE_MESSAGE_400);
        responseDeleteMessages.add(ResponseMensages.RESPONSE_MESSAGE_500);

        docket.globalResponseMessage(RequestMethod.POST, responsePostMessages);
        docket.globalResponseMessage(RequestMethod.PUT, responsePutMessages);
        docket.globalResponseMessage(RequestMethod.GET, responseGetMessages);
        docket.globalResponseMessage(RequestMethod.DELETE, responseDeleteMessages);

        docket.additionalModels(typeResolver.resolve(Respuesta.class));
        docket.additionalModels(typeResolver.resolve(Respuesta201.class));
        docket.additionalModels(typeResolver.resolve(Respuesta400.class));
        docket.additionalModels(typeResolver.resolve(Respuesta403.class));
        docket.additionalModels(typeResolver.resolve(Respuesta404.class));
        docket.additionalModels(typeResolver.resolve(Respuesta500.class));
    }

    @Bean
    public Docket frqGestionIncidenciasLimpiezaV1Api(ServletContext servletContext) {
        String basePath = "/sistemas-franquicia/gestion-incidentes/limpieza/v1";
        String apiName = "sistemas-franquicia-gestion-incidentes-limpieza-v1";
        description = ""
                + "# Gestión de Incidentes Limpieza\n"
                + "API de gestión de tickets para el capital humano e insumos y herramientas de limpieza en sucursal.";
        apiInfo = new ApiInfo("API Gestion de Incidentes Limpieza", description, "1.0", null, null, null, null, vext);
        ApiSelectorBuilder apiSelectorBuilder = this.apiSelectorBuilder(true);
        Docket docket = apiSelectorBuilder.paths(regex(".*/gestion-incidentes/limpieza/.*")).paths(Predicates.not(regex(".*/gestion-incidentes/catalogos/.*"))).build();
        docket.groupName(apiName);
        docket.apiInfo(apiInfo);
        docket.host(this.apigeeHost);
        docket.pathProvider(new RelativePathProvider(servletContext) {
            @Override
            public String getApplicationBasePath() {
                return basePath;
            }
        });
        return docket;
    }

    @Bean
    public Docket frqGestionIncidenciasCatalogosV1Api(ServletContext servletContext) {
        String basePath = "/sistemas-franquicia/gestion-incidentes/catalogos/v1";
        String apiName = "sistemas-franquicia-gestion-incidentes-catalogos-v1";
        description = ""
                + "# Gestión de Incidentes Catálogos\n"
                + "API para la consulta de catálogos.";
        apiInfo = new ApiInfo("API Gestion de Incidentes Catálogos", description, "1.0", null, null, null, null, vext);
        ApiSelectorBuilder apiSelectorBuilder = this.apiSelectorBuilder(true);
        Docket docket = apiSelectorBuilder.paths(regex(".*/gestion-incidentes/catalogos/.*")).build();
        docket.groupName(apiName);
        docket.apiInfo(apiInfo);
        docket.host(this.apigeeHost);
        docket.pathProvider(new RelativePathProvider(servletContext) {
            @Override
            public String getApplicationBasePath() {
                return basePath;
            }
        });
        return docket;
    }

    @Bean
    public Docket frqSeguridadV1Api(ServletContext servletContext) {
        String basePath = "/sistemas-franquicia/seguridad/v1";
        String apiName = "sistemas-franquicia-seguridad-v1";
        description = ""
                + "# Seguridad\n"
                + "API de Seguridad para generación de llaves para encriptar y desencriptar la información.";
        apiInfo = new ApiInfo("API Seguridad", description, "1.0", null, null, null, null, vext);
        ApiSelectorBuilder apiSelectorBuilder = this.apiSelectorBuilder(false);
        Docket docket = apiSelectorBuilder.paths(regex(".*/seguridad/.*")).build();
        docket.groupName(apiName);
        this.setGlobalResponsesSecurityApi(docket);
        docket.apiInfo(apiInfo);
        docket.host(this.apigeeHost);
        docket.pathProvider(new RelativePathProvider(servletContext) {
            @Override
            public String getApplicationBasePath() {
                return basePath;
            }
        });
        return docket;
    }

    @Bean
    public Docket frqUtileriasV1Api(ServletContext servletContext) {
        String basePath = "/sistemas-franquicia/utilerias/v1";
        String apiName = "sistemas-franquicia-utilerias-v1";
        description = ""
                + "# Utilerías\n"
                + "API de Utilerías que brinda recursos de uso común.";
        apiInfo = new ApiInfo("API Utilerias", description, "1.0", null, null, null, null, vext);
        ApiSelectorBuilder apiSelectorBuilder = this.apiSelectorBuilder(true);
        Docket docket = apiSelectorBuilder.paths(regex(".*/utilerias/.*")).build();
        docket.groupName(apiName);
        this.setGlobalResponses(docket);
        docket.apiInfo(apiInfo);
        docket.host(this.apigeeHost);
        docket.pathProvider(new RelativePathProvider(servletContext) {
            @Override
            public String getApplicationBasePath() {
                return basePath;
            }
        });
        return docket;
    }

    @Bean
    public Docket frqTokenV1Api(ServletContext servletContext) {
        String basePath = "/sistemas-franquicia/oauth/token/v1";
        String apiName = "sistemas-franquicia-oauth-v1";
        description = ""
                + "# OAuth2\n"
                + "API de OAuth2.";
        apiInfo = new ApiInfo("API OAuth2", description, "1.0", null, null, null, null, vext);
        ApiSelectorBuilder apiSelectorBuilder = this.apiSelectorBuilder(true);
        Docket docket = apiSelectorBuilder.paths(regex(".*/api/oauth/token/.*")).build();
        docket.groupName(apiName);
        this.setGlobalResponsesSecurityApi(docket);
        docket.apiInfo(apiInfo);
        docket.host(this.apigeeHost);
        docket.pathProvider(new RelativePathProvider(servletContext) {
            @Override
            public String getApplicationBasePath() {
                return basePath;
            }
        });
        return docket;
    }

    @Bean
    public Docket defaultDocket() {
        description = "Plataforma Franquicia cuenta con una gran variedad de Servicios y API's que pueden ser de funcionalidad interna o ayuda para terceros. Para más información técnica de esta API visita [nuestra documentación](http://127.0.0.1:8080/plataforma-franquicia-core/v2/api-docs/custom/group/html).";
        ApiSelectorBuilder apiSelectorBuilder = this.apiSelectorBuilder(true);
        apiSelectorBuilder.paths(regex(".*/"));
        Docket docket = apiSelectorBuilder.build();
        apiInfo = new ApiInfo("Plataforma Franquicia - All API's", description, "1.0", termsOfServiceUrl, contact, null, null, vext);
        docket.apiInfo(apiInfo);
        return docket;
    }

    @Bean
    public Docket allServices() {
        String apiName = "all-services";
        description = "Plataforma Franquicia cuenta con una gran variedad de Servicios para el funcionamiento interno de la aplicación. Para más información técnica de esta API visita [nuestra documentación](http://127.0.0.1:8080/plataforma-franquicia-core/v2/api-docs/custom/group/" + apiName + "/html).";
        ApiSelectorBuilder apiSelectorBuilder = this.apiSelectorBuilder(true);
        apiSelectorBuilder.paths(Predicates.and(regex(".*/service/.*")));
        Docket docket = apiSelectorBuilder.build();
        apiInfo = new ApiInfo("Plataforma Franquicia - All Services", description, "1.0", termsOfServiceUrl, contact, null, null, vext);
        docket.groupName(apiName);
        docket.apiInfo(apiInfo);
        return docket;
    }

    @Bean
    public Docket allApis() {
        String apiName = "all-apis";
        description = "Plataforma Franquicia cuenta con una gran variedad de Apis. Para más información técnica de esta API visita [nuestra documentación](http://127.0.0.1:8080/plataforma-franquicia-core/v2/api-docs/custom/group/" + apiName + "/html).";
        ApiSelectorBuilder apiSelectorBuilder = this.apiSelectorBuilder(true);
        apiSelectorBuilder.paths(Predicates.and(regex(".*/api/.*")));
        Docket docket = apiSelectorBuilder.build();
        apiInfo = new ApiInfo("Plataforma Franquicia - All API's", description, "1.0", termsOfServiceUrl, contact, null, null, vext);
        docket.groupName(apiName);
        docket.apiInfo(apiInfo);
        return docket;
    }

    private OAuth securitySchema() {
        List<AuthorizationScope> authorizationScopeList = new ArrayList<>();
        authorizationScopeList.add(new AuthorizationScope("read", "read all"));
        authorizationScopeList.add(new AuthorizationScope("write", "access all"));
        List<GrantType> grantTypes = new ArrayList<>();
        GrantType passwordCredentialsGrant = new ResourceOwnerPasswordCredentialsGrant(AUTH_SERVER);
        grantTypes.add(passwordCredentialsGrant);
        return new OAuth("OAuth2", authorizationScopeList, grantTypes);
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder().securityReferences(defaultAuth()).build();
    }

    private List<SecurityReference> defaultAuth() {
        final AuthorizationScope[] authorizationScopes = new AuthorizationScope[2];
        authorizationScopes[0] = new AuthorizationScope("read", "read all");
        authorizationScopes[1] = new AuthorizationScope("write", "write all");
        return Collections.singletonList(new SecurityReference("OAuth2", authorizationScopes));
    }

    @Bean
    public SecurityConfiguration security() {
        return new SecurityConfiguration(CLIENT_ID, CLIENT_SECRET, "", "", "Bearer access token", ApiKeyVehicle.HEADER, HttpHeaders.AUTHORIZATION, "");
    }

    @Bean
    UiConfiguration uiConfig() {
        return UiConfigurationBuilder.builder()
                .deepLinking(true)
                .displayOperationId(true)
                .defaultModelsExpandDepth(1)
                .defaultModelExpandDepth(1)
                .defaultModelRendering(ModelRendering.EXAMPLE)
                .displayRequestDuration(true)
                .docExpansion(DocExpansion.NONE)
                .filter(true)
                .maxDisplayedTags(null)
                .operationsSorter(OperationsSorter.ALPHA)
                .showExtensions(true)
                .tagsSorter(TagsSorter.ALPHA)
                .supportedSubmitMethods(UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS)
                .build();
    }

    @Bean
    public FilterRegistrationBean<SwaggerFilterCors> filterRegistrationBeanSwaggerFilterCors() {
        FilterRegistrationBean<SwaggerFilterCors> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new SwaggerFilterCors());
        registrationBean.addUrlPatterns("/v2/api-docs/*", "");
        registrationBean.setOrder(0);
        return registrationBean;
    }

    @Bean
    public SwaggerBI swaggerBI() {
        return new SwaggerBI();
    }

}
