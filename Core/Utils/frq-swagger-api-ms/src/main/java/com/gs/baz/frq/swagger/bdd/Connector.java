/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.swagger.bdd;

import java.util.List;

/**
 *
 * @author cescobarh
 */
public class Connector {

    private final ConnectorType connectorType;
    private String descripction;
    private Parameters parameters;

    public Connector(ConnectorType connectorType, String descripction) {
        this.connectorType = connectorType;
        this.descripction = descripction;
    }

    public String name() {
        return connectorType.name();
    }

    public String getDescripction() {
        return descripction;
    }

    public void setDescripction(String descripction) {
        this.descripction = descripction;
    }

    public Parameters Parameters(List<String> headers, List<List<String>> rowData, int identation) {
        this.parameters = new Parameters(headers, rowData, identation);
        return parameters;
    }

    public Parameters Parameters() {
        return this.parameters;
    }

    @Override
    public String toString() {
        return "                " + connectorType.name() + " " + descripction;
    }

}
