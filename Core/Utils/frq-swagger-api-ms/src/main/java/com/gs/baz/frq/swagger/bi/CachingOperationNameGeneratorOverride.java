/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.swagger.bi;

import org.springframework.stereotype.Component;
import springfox.documentation.OperationNameGenerator;

import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Primary;

/**
 *
 * @author cescobarh
 */
@Component
@Primary
public class CachingOperationNameGeneratorOverride implements OperationNameGenerator {

    private final Logger logger = LogManager.getLogger();
    private final Map<String, Integer> generated = new HashMap<>();

    @Override
    public String startingWith(String prefix) {
        if (generated.containsKey(prefix)) {
            generated.put(prefix, generated.get(prefix) + 1);
            String nextUniqueOperationName = String.format("%s_%s", prefix, generated.get(prefix));
            logger.debug("Generating unique operation named: {}", nextUniqueOperationName);
            return nextUniqueOperationName;
        } else {
            generated.put(prefix, 0);
            return prefix;
        }
    }
}
