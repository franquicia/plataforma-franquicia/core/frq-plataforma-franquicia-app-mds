/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.swagger.api.mocker;

import java.util.Map;

/**
 *
 * @author cescobarh
 */
public class ApiMockerObject {

    private String mockDirectory;
    private boolean quiet;
    private int port;
    private int latency;
    private boolean logRequestHeaders;
    private Map<String, ResourceServiceObject> webServices;

    public ApiMockerObject() {
    }

    public ApiMockerObject(String mockDirectory, boolean quiet, int port, int latency, boolean logRequestHeaders) {
        this.mockDirectory = mockDirectory;
        this.quiet = quiet;
        this.port = port;
        this.latency = latency;
        this.logRequestHeaders = logRequestHeaders;
    }

    public ApiMockerObject(String mockDirectory, boolean quiet, int port, int latency, boolean logRequestHeaders, Map<String, ResourceServiceObject> webServices) {
        this.mockDirectory = mockDirectory;
        this.quiet = quiet;
        this.port = port;
        this.latency = latency;
        this.logRequestHeaders = logRequestHeaders;
        this.webServices = webServices;
    }

    public String getMockDirectory() {
        return mockDirectory;
    }

    public void setMockDirectory(String mockDirectory) {
        this.mockDirectory = mockDirectory;
    }

    public boolean isQuiet() {
        return quiet;
    }

    public void setQuiet(boolean quiet) {
        this.quiet = quiet;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getLatency() {
        return latency;
    }

    public void setLatency(int latency) {
        this.latency = latency;
    }

    public boolean isLogRequestHeaders() {
        return logRequestHeaders;
    }

    public void setLogRequestHeaders(boolean logRequestHeaders) {
        this.logRequestHeaders = logRequestHeaders;
    }

    public Map<String, ResourceServiceObject> getWebServices() {
        return webServices;
    }

    public void setWebServices(Map<String, ResourceServiceObject> webServices) {
        this.webServices = webServices;
    }

}
