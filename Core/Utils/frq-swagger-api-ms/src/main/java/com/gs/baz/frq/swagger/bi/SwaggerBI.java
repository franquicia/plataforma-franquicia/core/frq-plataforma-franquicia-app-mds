/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.swagger.bi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.wnameless.json.flattener.JsonFlattener;
import com.gs.baz.frq.swagger.api.mocker.ApiMockerObject;
import com.gs.baz.frq.swagger.api.mocker.ResourceServiceObject;
import com.gs.baz.frq.swagger.api.mocker.ResponsesMethodsObject;
import com.gs.baz.frq.swagger.api.mocker.SwitchesResponses;
import com.gs.baz.frq.swagger.bdd.Feature;
import com.gs.baz.frq.swagger.bdd.ScenarioImp;
import com.gs.baz.frq.swagger.bdd.ScenarioOutlineImp;
import com.gs.baz.frq.swagger.bdd.Step;
import io.github.swagger2markup.Swagger2MarkupConverter;
import io.swagger.models.HttpMethod;
import io.swagger.models.Model;
import io.swagger.models.ModelImpl;
import io.swagger.models.Operation;
import io.swagger.models.RefModel;
import io.swagger.models.Response;
import io.swagger.models.Scheme;
import io.swagger.models.Swagger;
import io.swagger.models.Tag;
import io.swagger.models.parameters.BodyParameter;
import io.swagger.models.parameters.Parameter;
import io.swagger.models.parameters.PathParameter;
import io.swagger.models.properties.AbstractProperty;
import io.swagger.models.properties.ArrayProperty;
import io.swagger.models.properties.ObjectProperty;
import io.swagger.models.properties.Property;
import io.swagger.models.properties.RefProperty;
import io.swagger.models.properties.StringProperty;
import io.swagger.parser.SwaggerParser;
import io.swagger.util.Json;
import io.swagger.util.Yaml;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Attributes;
import org.asciidoctor.OptionsBuilder;
import org.asciidoctor.SafeMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

/**
 *
 * @author cescobarh
 */
@Component
public class SwaggerBI {

    private final Logger logger = LogManager.getLogger();

    @Autowired
    HttpServletRequest httpServletRequest;

    private String originalRef;

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final Path asciidocsIn = Paths.get("/franquicia/plataforma_franquicia/resources/asciidoc/in");
    private final String ourDirectory = "/franquicia/plataforma_franquicia/resources/asciidoc/out/";

    private final static String ASCII = ""
            + "include::{generated}/overview.adoc[]\n"
            + "include::{generated}/paths.adoc[]\n"
            + "include::{generated}/security.adoc[]\n"
            + "include::{generated}/definitions.adoc[]";

    private ObjectNode getSwaggerModified(ObjectNode swagger, boolean apigeeFormat) throws IOException {
        JsonNode paths = swagger.get("paths");
        JsonNode definitions = swagger.get("definitions");
        if (paths != null) {
            paths.fields().forEachRemaining(entrySet -> {
                JsonNode itemPath = entrySet.getValue();
                if (itemPath != null) {
                    itemPath.fields().forEachRemaining(itemMethod -> {
                        JsonNode respuesta;
                        String keyMethod = itemMethod.getKey();
                        JsonNode itemVerb = itemMethod.getValue();
                        ArrayNode itemParams = (ArrayNode) itemVerb.get("parameters");
                        if (itemParams != null && itemParams.size() > 0) {
                            List<ObjectNode> bodyParam = new ArrayList<>();
                            List<ObjectNode> headerParam = new ArrayList<>();
                            List<ObjectNode> pathParam = new ArrayList<>();
                            List<ObjectNode> queryParam = new ArrayList<>();
                            itemParams.forEach(itemParam -> {
                                String typeParam = itemParam.get("in").asText();
                                if (typeParam.equals("body")) {
                                    bodyParam.add((ObjectNode) itemParam);
                                }
                                if (typeParam.equals("header")) {
                                    headerParam.add((ObjectNode) itemParam);
                                }
                                if (typeParam.equals("path")) {
                                    pathParam.add((ObjectNode) itemParam);
                                }
                                if (typeParam.equals("query")) {
                                    queryParam.add((ObjectNode) itemParam);
                                }
                            });
                            itemParams.removeAll();
                            if (!headerParam.isEmpty()) {
                                itemParams.addAll(headerParam);
                            }
                            if (!pathParam.isEmpty()) {
                                itemParams.addAll(pathParam);
                            }
                            if (!queryParam.isEmpty()) {
                                itemParams.addAll(queryParam);
                            }
                            if (!bodyParam.isEmpty()) {
                                itemParams.addAll(bodyParam);
                            }
                        }
                        JsonNode responses = itemVerb.get("responses");
                        if (responses != null) {
                            JsonNode code = null;
                            JsonNode code200 = responses.get("200");
                            JsonNode code201 = responses.get("201");
                            if (keyMethod.equals("post")) {
                                if (code200 != null) {
                                    code201 = null;
                                    ((ObjectNode) responses).remove("201");
                                }
                            }
                            if (code201 != null) {
                                code = code201;
                            }
                            if (code200 != null) {
                                code = code200;
                            }
                            if (code != null) {
                                JsonNode schema = code.get("schema");
                                if (schema != null) {
                                    boolean applyResponseWrapper = true;
                                    String isList = "";
                                    JsonNode type = schema.get("type");
                                    if (type != null) {
                                        if (type.asText().equals("array")) {
                                            isList = "Lista";
                                            JsonNode ref = schema.get("items").get("$ref");
                                            if (ref != null) {
                                                originalRef = schema.get("items").get("$ref").asText();
                                            } else {
                                                applyResponseWrapper = false;
                                            }
                                        } else {
                                            applyResponseWrapper = false;
                                        }

                                    }
                                    String definition = originalRef.replace("#/definitions/", "");
                                    if (definition.equals("SinResultado")) {
                                        applyResponseWrapper = false;
                                    }
                                    if (applyResponseWrapper) {
                                        String nameResponse = "Salida" + isList + definition;
                                        JsonNode originalResponse = code.get("schema").deepCopy();
                                        if (code201 == null) {
                                            respuesta = definitions.get("Respuesta").deepCopy();
                                        } else {

                                            respuesta = definitions.get("Respuesta201").deepCopy();
                                        }
                                        JsonNode properties = respuesta.get("properties");
                                        if (originalResponse.toString().contains("RespuestaLessResult200") || originalResponse.toString().contains("RespuestaLessResult201")) {

                                            ((ObjectNode) properties).remove("resultado");

                                        } else {
                                            ((ObjectNode) properties).set("resultado", originalResponse);
                                        }
                                        ((ObjectNode) respuesta).put("title", nameResponse);
                                        ((ObjectNode) definitions).set(nameResponse, respuesta);
                                        ((ObjectNode) schema).removeAll();
                                        ((ObjectNode) schema).put("$ref", "#/definitions/" + nameResponse);
                                    }
                                }
                            }
                        }
                    });
                }
            });
            ((ObjectNode) definitions).remove("Respuesta");
            ((ObjectNode) definitions).remove("Respuesta201");
        }
        if (apigeeFormat) {
            swagger = this.resolveWithApigeeFormat(swagger);
        } else {
            swagger = this.withCustomFormat(swagger);
        }
        return swagger;
    }

    public ArrayNode customFindApiName(HttpServletRequest httpServletRequest, String basePath) throws IOException {
        List<String> tags = new ArrayList<>();
        List<String> tempTags = new ArrayList<>();
        ObjectNode swagger = this.getBaseApiDocs(httpServletRequest);
        JsonNode paths = swagger.get("paths");
        ObjectNode findedPaths = objectMapper.createObjectNode();
        if (paths != null) {
            paths.fields().forEachRemaining(item -> {
                String itemPath = item.getKey();
                if (itemPath != null) {
                    if (itemPath.contains(basePath)) {
                        findedPaths.set(item.getKey(), item.getValue());
                    }
                }
            });
        }
        findedPaths.forEach(itemPath -> {
            itemPath.fields().forEachRemaining(item -> {
                JsonNode value = itemPath.get(item.getKey());
                if (value != null) {
                    ArrayNode itemTags = (ArrayNode) value.get("tags");
                    for (JsonNode tag : itemTags) {
                        tempTags.add(tag.asText());
                    }
                }
            });
        });
        tempTags.stream().filter((item) -> (!tags.contains(item))).forEach((item) -> {
            tags.add(item);
        });
        return objectMapper.convertValue(tags, ArrayNode.class);
    }

    public ObjectNode getBaseApiDocsModified(boolean apigeeFormat) throws IOException {
        ObjectNode swagger = this.getBaseApiDocs();
        swagger = this.getSwaggerModified(swagger, apigeeFormat);
        return swagger;
    }

    private ObjectNode getBaseApiDocs(HttpServletRequest httpServletRequest) throws IOException {
        String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort() + httpServletRequest.getContextPath();
        URL u = new URL(basePath + "/v2/api-docs");
        String out = new Scanner(u.openStream(), "UTF-8").useDelimiter("\\A").next();
        return objectMapper.readValue(out, ObjectNode.class);
    }

    private ObjectNode getBaseApiDocs() throws IOException {
        String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort() + httpServletRequest.getContextPath();
        URL u = new URL(basePath + "/v2/api-docs");
        String out = new Scanner(u.openStream(), "UTF-8").useDelimiter("\\A").next();
        return objectMapper.readValue(out, ObjectNode.class);
    }

    public ObjectNode getApiDocsByGroup(String group, boolean apigeeFormat) throws IOException {
        String basePath = httpServletRequest.getScheme() + "://" + httpServletRequest.getLocalAddr() + ":" + httpServletRequest.getLocalPort() + httpServletRequest.getContextPath();
        URL u = new URL(basePath + "/v2/api-docs?group=" + group);
        String out = new Scanner(u.openStream(), "UTF-8").useDelimiter("\\A").next();
        ObjectNode swagger = objectMapper.readValue(out, ObjectNode.class);
        swagger = this.getSwaggerModified(swagger, apigeeFormat);
        return swagger;
    }

    public byte[] getApiDocsByGroupHTML(String groupName, boolean forceUpdate, boolean apigeeFormat) throws IOException {
        ObjectNode swaggerGroup = this.getApiDocsByGroup(groupName, apigeeFormat);
        String swagger = objectMapper.writeValueAsString(swaggerGroup);
        return this.getApiDocs(groupName, forceUpdate, "html", swagger);
    }

    public byte[] getApiDocsHTML(boolean forceUpdate, boolean apigeeFormat) throws IOException {
        ObjectNode swaggerGroup = this.getBaseApiDocsModified(apigeeFormat);
        String swagger = objectMapper.writeValueAsString(swaggerGroup);
        return this.getApiDocs("index", forceUpdate, "html", swagger);
    }

    public byte[] getApiDocsByGroupPDF(String groupName, boolean forceUpdate, boolean apigeeFormat) throws IOException {
        ObjectNode swaggerGroup = this.getApiDocsByGroup(groupName, apigeeFormat);
        String swagger = objectMapper.writeValueAsString(swaggerGroup);
        return this.getApiDocs(groupName, forceUpdate, "pdf", swagger);
    }

    public byte[] getApiDocsPDF(boolean forceUpdate, boolean apigeeFormat) throws IOException {
        ObjectNode swaggerGroup = this.getBaseApiDocsModified(apigeeFormat);
        String swagger = objectMapper.writeValueAsString(swaggerGroup);
        return this.getApiDocs("index", forceUpdate, "pdf", swagger);
    }

    private byte[] getApiDocs(String groupName, boolean forceUpdate, String backend, String swagger) throws IOException {
        byte[] bytesFile;
        Path out;
        if (backend.equals("html")) {
            out = Paths.get(ourDirectory + groupName + ".html");
        } else {
            out = Paths.get(ourDirectory + groupName + ".pdf");
        }
        if (out.toFile().exists() && !forceUpdate) {
            bytesFile = Files.readAllBytes(out);
        } else {
            Asciidoctor asciidoctor = Asciidoctor.Factory.create();
            Swagger2MarkupConverter.from(swagger).build().toFolder(asciidocsIn);
            Attributes attributes = new Attributes();
            attributes.setDocType("book");
            attributes.setTableOfContents(true);
            attributes.setSectNumLevels(3);
            attributes.setAttribute("toc", "left");
            attributes.setAttribute("generated", asciidocsIn.toString());
            OptionsBuilder ob = OptionsBuilder.options();
            ob.safe(SafeMode.UNSAFE);
            ob.backend(backend);
            ob.attributes(attributes);
            ob.toFile(out.toFile());
            ob.mkDirs(true);
            asciidoctor.convert(ASCII, ob);
            bytesFile = Files.readAllBytes(out);
        }
        return bytesFile;
    }

    private ObjectNode withCustomFormat(JsonNode jsonSwagger) throws JsonProcessingException {
        Swagger swagger = new SwaggerParser().read(jsonSwagger);
        swagger.addScheme(Scheme.HTTP);
        boolean response400 = false;
        String basePath = swagger.getBasePath();
        String realBasePath = "/plataforma-franquicia-core/api";
        basePath = basePath.replace("/sistemas-franquicia", realBasePath);
        swagger.setBasePath(basePath);
        swagger.setHost("127.0.0.1:8080");
        Map<String, io.swagger.models.Path> newPaths = new HashMap<>();
        Map<String, io.swagger.models.Path> paths = swagger.getPaths();
        if (paths != null) {
            Tag apiNameTag = swagger.getTags().get(0);
            String apiName = apiNameTag.getName();
            for (Map.Entry<String, io.swagger.models.Path> entrySet : paths.entrySet()) {
                String key = entrySet.getKey();
                io.swagger.models.Path value = entrySet.getValue();
                String temp = basePath.replace("/plataforma-franquicia-core", "");
                key = key.replace(temp, "");
                newPaths.put(key, value);
            }
            swagger.setPaths(newPaths);
            Map<String, Model> definitions = swagger.getDefinitions();
            Map<String, Model> renamedDefinitions = new HashMap<>();
            definitions.forEach((key, value) -> {
                Map<String, Property> properties = value.getProperties();
                Map<String, Property> reramedProperties = new HashMap<>();
                if (properties != null) {
                    properties.forEach((keyPropertie, valuePropertie) -> {
                        Object example = valuePropertie.getExample();
                        if (example != null && example instanceof String) {
                            String exampleString = (String) example;
                            exampleString = exampleString.replace("https://baz-developer.bancoazteca.com.mx/info", "http://127.0.0.1:8080/plataforma-franquicia-core/info-errors");
                            exampleString = exampleString.replace("api-name", apiName);
                            valuePropertie.setExample(exampleString);
                        }
                        reramedProperties.put(keyPropertie, valuePropertie);
                    });
                }
                value.setProperties(reramedProperties);
                renamedDefinitions.put(key, value);
            });
            for (Map.Entry<String, io.swagger.models.Path> entrySet : paths.entrySet()) {
                io.swagger.models.Path itemPath = entrySet.getValue();
                List<Operation> operations = itemPath.getOperations();
                if (operations != null) {
                    for (Operation itemOperation : operations) {
                        Map<String, Response> responses = itemOperation.getResponses();
                        List<String> tags = itemOperation.getTags();
                        if (responses != null) {
                            responses.forEach((keyResponse, itemResponse) -> {
                                try {
                                    Map examples = new HashMap();
                                    Model responseSchema = itemResponse.getResponseSchema();
                                    if (responseSchema != null && responseSchema instanceof RefModel) {
                                        RefModel refModel = (RefModel) itemResponse.getResponseSchema();
                                        ObjectNode example = this.getDefinition(swagger, refModel.getSimpleRef(), false);
                                        String exampleString = objectMapper.writeValueAsString(example);
                                        exampleString = exampleString.replace(apiName, tags.get(0));
                                        example = objectMapper.readValue(exampleString, ObjectNode.class);
                                        examples.put("application/json", example);
                                        itemResponse.setExamples(examples);
                                    }
                                } catch (Exception ex) {
                                    logger.trace(ex.getMessage(), ex);
                                }
                            });
                            List<Parameter> parameters = itemOperation.getParameters();
                            if (parameters.isEmpty()) {
                                responses.remove("400");
                            } else {
                                response400 = true;
                            }
                        }
                    }
                }
            }
            if (!response400) {
                swagger.getDefinitions().remove("Respuesta400");
            }
        }
        swagger.getDefinitions().remove("CifraRequest");
        swagger.getDefinitions().remove("CifraResponse");
        swagger.getDefinitions().remove("DescifraResponse");
        swagger.getDefinitions().remove("DescifraSolicitud");
        String swaggerString = Json.pretty(swagger);
        return objectMapper.readValue(swaggerString, ObjectNode.class);
    }

    private ObjectNode resolveWithApigeeFormat(JsonNode jsonSwagger) throws JsonProcessingException {
        Swagger swagger = new SwaggerParser().read(jsonSwagger);
        swagger.addScheme(Scheme.HTTPS);
        boolean response400 = false;
        String basePath = swagger.getBasePath();
        Map<String, io.swagger.models.Path> newPaths = new HashMap<>();
        Map<String, io.swagger.models.Path> paths = swagger.getPaths();
        if (paths != null) {
            Tag apiNameTag = swagger.getTags().get(0);
            String apiName = apiNameTag.getName();
            paths.forEach((key, value) -> {
                String temp = basePath;
                temp = temp.replace("/sistemas-franquicia", "");
                key = key.replace(temp, "");
                key = key.replace("/api", "");
                newPaths.put(key, value);
            });
            swagger.setPaths(newPaths);
            Map<String, Model> definitions = swagger.getDefinitions();
            Map<String, Model> renamedDefinitions = new HashMap<>();
            ModelImpl summary = this.addCustomSummaryModel(basePath, apiNameTag, newPaths);
            renamedDefinitions.put("summary", summary);
            definitions.forEach((key, value) -> {
                Map<String, Property> properties = value.getProperties();
                Map<String, Property> reramedProperties = new HashMap<>();
                if (properties != null) {
                    properties.forEach((keyPropertie, valuePropertie) -> {
                        Object example = valuePropertie.getExample();
                        if (example != null && example instanceof String) {
                            String exampleString = (String) example;
                            exampleString = exampleString.replace("api-name", apiName);
                            valuePropertie.setExample(exampleString);
                        }
                        reramedProperties.put(keyPropertie, valuePropertie);
                    });
                }
                value.setProperties(reramedProperties);
                renamedDefinitions.put(key, value);
            });
            swagger.setDefinitions(renamedDefinitions);
            for (Map.Entry<String, io.swagger.models.Path> entrySet : newPaths.entrySet()) {
                io.swagger.models.Path itemPath = entrySet.getValue();
                List<Operation> operations = itemPath.getOperations();
                if (operations != null) {
                    for (Operation itemOperation : operations) {
                        Map<String, Response> responses = itemOperation.getResponses();
                        List<String> tags = itemOperation.getTags();
                        if (responses != null) {
                            responses.forEach((keyResponse, itemResponse) -> {
                                try {
                                    Map examples = new HashMap();
                                    Model responseSchema = itemResponse.getResponseSchema();
                                    if (responseSchema != null && responseSchema instanceof RefModel) {
                                        RefModel refModel = (RefModel) itemResponse.getResponseSchema();
                                        ObjectNode example = this.getDefinition(swagger, refModel.getSimpleRef(), false);
                                        String exampleString = objectMapper.writeValueAsString(example);
                                        exampleString = exampleString.replace(apiName, tags.get(0));
                                        example = objectMapper.readValue(exampleString, ObjectNode.class);
                                        examples.put("application/json", example);
                                        itemResponse.setExamples(examples);
                                    }
                                } catch (Exception ex) {
                                    logger.trace(ex.getMessage(), ex);
                                }
                            });
                            List<Parameter> parameters = null;//itemOperation.getParameters();
                            if (itemOperation.getParameters().isEmpty()) {
                                responses.remove("400");
                            } else {
                                response400 = true;
                            }

                            if (parameters != null) {
                                parameters.forEach(itemParameter -> {
                                    if (itemParameter instanceof BodyParameter) {
                                        BodyParameter inParam = (BodyParameter) itemParameter;
                                        try {
                                            Map examples = new HashMap();
                                            Model responseSchema = inParam.getSchema();
                                            if (responseSchema != null && responseSchema instanceof RefModel) {
                                                RefModel refModel = (RefModel) inParam.getSchema();
                                                ObjectNode example = this.getDefinition(swagger, refModel.getSimpleRef(), true);
                                                String exampleString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(example);
                                                examples.put("application/json", exampleString);
                                                inParam.setExamples(examples);
                                            }
                                        } catch (Exception ex) {
                                            logger.trace(ex.getMessage(), ex);
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            }
            if (!response400) {
                swagger.getDefinitions().remove("Respuesta400");
            }
            swagger.getDefinitions().remove("DesencriptaRespuesta");
            swagger.getDefinitions().remove("DesencriptaSolicitud");
            swagger.getDefinitions().remove("EncriptaRespuesta");
            swagger.getDefinitions().remove("EncriptaSolicitud");
        }
        String swaggerString = Json.pretty(swagger);
        ObjectNode swaggerJson = objectMapper.readValue(swaggerString, ObjectNode.class);
        if (paths != null) {
            ObjectNode summary = swaggerJson.get("definitions").get("summary").deepCopy();
            ObjectNode apiName = summary.get("properties").get("api-name").deepCopy();
            ((ObjectNode) summary.get("properties")).remove("api-name");
            ObjectNode allProperties = objectMapper.createObjectNode();
            ObjectNode apiNameObject = objectMapper.createObjectNode();
            apiNameObject.set("api-name", apiName);
            ObjectNode properties = summary.get("properties").deepCopy();
            allProperties.setAll(apiNameObject);
            allProperties.setAll(properties);
            summary.remove("properties");
            summary.set("properties", allProperties);
            ((ObjectNode) swaggerJson.get("definitions")).remove("summary");
            ((ObjectNode) swaggerJson.get("definitions")).set("summary", summary);
        }
        return swaggerJson;
    }

    private ObjectNode getDefinition(Swagger swagger, String reference, boolean inParamater) {
        ObjectNode object = objectMapper.createObjectNode();
        Map definitions = swagger.getDefinitions();

        ModelImpl model = (ModelImpl) definitions.get(reference);
        if (model != null) {
            Map properties = model.getProperties();
            properties.forEach((Object key, Object value) -> {
                AbstractProperty propertie = (AbstractProperty) value;
                try {
                    Object jsonObjectValue = propertie.getExample();
                    if (value instanceof RefProperty) {
                        RefProperty refProperty = (RefProperty) value;
                        jsonObjectValue = this.getReferenceToJson(swagger, refProperty.getSimpleRef(), inParamater);
                    }
                    if (value instanceof ArrayProperty) {
                        ArrayProperty arrayProperty = (ArrayProperty) value;
                        if (arrayProperty.getItems() instanceof RefProperty) {
                            RefProperty refProperty = (RefProperty) arrayProperty.getItems();
                            ArrayNode array = objectMapper.createArrayNode();
                            array.add(this.getReferenceToJson(swagger, refProperty.getSimpleRef(), inParamater));
                            jsonObjectValue = array;
                        } else {
                            ArrayNode array = objectMapper.createArrayNode();
                            Property values = arrayProperty.getItems();
                            if (values.getExample() == null) {
                                array.add(values.getType());
                            } else {
                                array.add((String) values.getExample());
                            }
                            jsonObjectValue = array;
                        }
                    }
                    object.putPOJO((String) key, jsonObjectValue);
                } catch (Exception ex) {
                    logger.trace(ex.getMessage(), ex);
                }
            });
        }
        return object;
    }

    private ObjectNode getReferenceToJson(Swagger swagger, String reference, boolean inParamater) {
        ObjectNode object = objectMapper.createObjectNode();
        Map definitions = swagger.getDefinitions();
        ModelImpl model = (ModelImpl) definitions.get(reference);
        if (model != null) {
            Map properties = model.getProperties();
            properties.forEach((Object key, Object value) -> {
                AbstractProperty propertie = (AbstractProperty) value;
                try {
                    Object jsonObjectValue = propertie.getExample();
                    if (value instanceof RefProperty) {
                        RefProperty refProperty = (RefProperty) value;
                        if (!reference.equals(refProperty.getSimpleRef())) {
                            jsonObjectValue = this.getReferenceToJson(swagger, refProperty.getSimpleRef(), inParamater);
                        } else {
                            ObjectNode sameObjectParent = objectMapper.createObjectNode();
                            sameObjectParent.put("$ref", refProperty.get$ref());
                            jsonObjectValue = refProperty.get$ref();
                        }
                    }
                    if (value instanceof ArrayProperty) {
                        ArrayProperty arrayProperty = (ArrayProperty) value;
                        if (arrayProperty.getItems() instanceof RefProperty) {
                            RefProperty refProperty = (RefProperty) arrayProperty.getItems();
                            ArrayNode array = objectMapper.createArrayNode();
                            if (!reference.equals(refProperty.getSimpleRef())) {
                                if (!inParamater) {
                                    array.add(this.getReferenceToJson(swagger, refProperty.getSimpleRef(), !inParamater));
                                } else {
                                    ObjectNode sameObjectParent = objectMapper.createObjectNode();
                                    sameObjectParent.put("$ref", refProperty.get$ref());
                                    array.add(sameObjectParent);
                                }
                            } else {
                                ObjectNode sameObjectParent = objectMapper.createObjectNode();
                                sameObjectParent.put("$ref", refProperty.get$ref());
                                array.add(sameObjectParent);
                            }
                            jsonObjectValue = array;
                        } else {
                            ArrayNode array = objectMapper.createArrayNode();
                            Property values = arrayProperty.getItems();
                            if (values.getExample() == null) {
                                array.add(values.getType());
                            } else {
                                array.add((String) values.getExample());
                            }
                            jsonObjectValue = array;
                        }
                    }
                    object.putPOJO((String) key, jsonObjectValue);
                } catch (Exception ex) {
                    logger.trace(ex.getMessage(), ex);
                }
            });
        }
        return object;
    }

    private ModelImpl addCustomSummaryModel(String basePath, Tag apiInfoTag, Map<String, io.swagger.models.Path> paths) {
        Map<String, Property> properties = new HashMap<>();
        Property propertyApiName = new StringProperty();
        ModelImpl summaryModel = new ModelImpl();
        summaryModel.setType("object");
        propertyApiName.setDescription(apiInfoTag.getDescription());
        String nameApi = "";
        if (basePath.startsWith("/")) {
            basePath = basePath.substring(1, basePath.length());
            nameApi = basePath.replace("/", "-");
        }
        propertyApiName.setExample(nameApi);
        paths.forEach((key, value) -> {
            Map<HttpMethod, Operation> operations = value.getOperationMap();
            operations.forEach((httpMethod, itemOperation) -> {
                String prefixName = key;
                if (prefixName.startsWith("/")) {
                    prefixName = prefixName.substring(1, prefixName.length());
                }
                prefixName = prefixName.replace("/", "-");
                prefixName = prefixName.replace("{", "");
                prefixName = prefixName.replace("}", "");
                prefixName = prefixName + "-" + httpMethod.name().toLowerCase();

                Property propertyDisplayName = new StringProperty();
                propertyDisplayName.setDescription(itemOperation.getDescription());
                propertyDisplayName.setExample(itemOperation.getSummary());

                Map<String, Property> propertiesOperation = new HashMap<>();
                propertiesOperation.put("displayName", propertyDisplayName);
                Property propertyOperation = new ObjectProperty(propertiesOperation);
                properties.put(prefixName, propertyOperation);

            });
        });
        properties.put("api-name", propertyApiName);
        summaryModel.setProperties(properties);
        return summaryModel;
    }

    public byte[] getTemplateTest(String nameGroup, boolean apigeeFormat) throws IOException {
        ObjectNode jsonSwagger = this.getApiDocsByGroup(nameGroup, apigeeFormat);
        Swagger swagger = new SwaggerParser().read(jsonSwagger);
        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
        try (ZipOutputStream zipOut = new ZipOutputStream(arrayOutputStream)) {
            this.buildOas(nameGroup, zipOut, swagger);
            this.getApiMocker(zipOut, swagger);
            this.getGherkin(zipOut, swagger);
        }
        return arrayOutputStream.toByteArray();
    }

    private void buildOas(String nameGroup, ZipOutputStream zipOut, Swagger swagger) throws IOException {
        String yamlOutput = Yaml.pretty().writeValueAsString(swagger);
        String oas = "oas/" + nameGroup + ".yaml";
        ZipEntry zipEntryOas = new ZipEntry(oas);
        zipOut.putNextEntry(zipEntryOas);
        zipOut.write(yamlOutput.getBytes());
        zipOut.closeEntry();
    }

    private void getGherkin(ZipOutputStream zipOut, Swagger swagger) throws IOException {
        String basePath = swagger.getBasePath();
        Map<String, io.swagger.models.Path> paths = swagger.getPaths();
        if (paths != null) {
            String basePathTestFeatures = "tests/features";

            for (Map.Entry<String, io.swagger.models.Path> entrySet : paths.entrySet()) {//Paths
                String key = entrySet.getKey();
                io.swagger.models.Path value = entrySet.getValue();
                Map<HttpMethod, Operation> operations = value.getOperationMap();
                String prefixName = this.prefixName(key);
                for (Map.Entry<HttpMethod, Operation> entrySetOperations : operations.entrySet()) {//Methods->Features
                    HttpMethod httpMethod = entrySetOperations.getKey();
                    Operation itemOperation = entrySetOperations.getValue();
                    String methodGroup = httpMethod.name().toLowerCase();
                    Map<String, Response> responses = itemOperation.getResponses();
                    List<Parameter> swaggerParameters = itemOperation.getParameters();
                    String featureName = key + " " + methodGroup;
                    Feature feature = new Feature(featureName, itemOperation.getDescription());

                    ScenarioImp scenarioConsumerKey = feature.Scenario("Solicita consumer key y secret key de la app");
                    Step stepConsumerKeyGiven = scenarioConsumerKey.Given("I have basic authentication credentials `apigeeUsername` and `apigeePassword`");
                    stepConsumerKeyGiven.And("I have valid client TLS configuration");
                    scenarioConsumerKey.When("I GET `apigeeHost`/v1/organizations/`apigeeOrg`/developers/`apigeeDeveloper`/apps/`apigeeApp`");
                    Step stepConsumerKeyThen = scenarioConsumerKey.Then("response code should be 200");
                    stepConsumerKeyThen.And("response body should be valid json");
                    stepConsumerKeyThen.And("I store the value of body path $.credentials[0].consumerKey as globalConsumerKey in global scope");
                    stepConsumerKeyThen.And("I store the value of body path $.credentials[0].consumerSecret as globalConsumerSecret in global scope");

                    ScenarioImp scenarioTokenOauth = feature.Scenario("Solicita un access token con el servidor de autorizacion");
                    Step stepTokenOauthGiven = scenarioTokenOauth.Given("I set form parameters to");
                    List<String> headersDataParams = new ArrayList<>();
                    List< List<String>> rowsDataValuesParams = new ArrayList<>();
                    List<String> cellValueParam = new ArrayList<>();
                    headersDataParams.add("parameter");
                    cellValueParam.add("grant_type");
                    headersDataParams.add("value");
                    cellValueParam.add("client_credentials");
                    rowsDataValuesParams.add(cellValueParam);
                    stepTokenOauthGiven.Parameters(headersDataParams, rowsDataValuesParams, 19);
                    stepTokenOauthGiven.And("I have basic authentication credentials `globalConsumerKey` and `globalConsumerSecret`");
                    stepTokenOauthGiven.And("I have valid client TLS configuration");
                    scenarioTokenOauth.When("I POST to `apigeeDomain`/`apigeeOauthEndpoint`");
                    Step stepTokenOauthThen = scenarioTokenOauth.Then("response code should be 200");
                    stepTokenOauthThen.And("response body should be valid json");
                    stepTokenOauthThen.And("I store the value of body path $.access_token as access token");

                    for (Map.Entry<String, Response> entrySetResponse : responses.entrySet()) {//Codes->Scenarios
                        String code = entrySetResponse.getKey();
                        Response itemResponse = entrySetResponse.getValue();
                        String nameScenario = key + " " + code + " " + itemResponse.getDescription();
                        Map<String, Object> examples = itemResponse.getExamples();
                        LinkedHashMap<String, LinkedHashMap> exampleResponseJson = (LinkedHashMap<String, LinkedHashMap>) examples.get("application/json");
                        String exampleResponseJsonString = objectMapper.writeValueAsString(exampleResponseJson);
                        if (swaggerParameters != null) {
                            List<String> headersData = new ArrayList<>();
                            List< List<String>> rowsDataValues = new ArrayList<>();
                            List<String> cellValue = new ArrayList<>();
                            ScenarioOutlineImp scenarioOutline = feature.ScenarioOutline(nameScenario);
                            Step stepGiven = scenarioOutline.Given("I set Content-Type header to application/json");
                            stepGiven.And("I set bearer token");
                            stepGiven.And("I have valid client TLS configuration");
                            for (Parameter itemParameter : swaggerParameters) {
                                if (itemParameter instanceof BodyParameter) {
                                    BodyParameter inParam = (BodyParameter) itemParameter;
                                    RefModel refModel = (RefModel) inParam.getSchema();
                                    ObjectNode example = this.getDefinition(swagger, refModel.getSimpleRef(), true);
                                    ObjectNode exampleTemplateValues = objectMapper.createObjectNode();
                                    Iterator<String> iteratorNames = example.fieldNames();
                                    iteratorNames.forEachRemaining(itemKey -> {
                                        headersData.add(itemKey);
                                        exampleTemplateValues.put(itemKey, "<" + itemKey + ">");
                                    });
                                    example.forEach(itemNode -> {
                                        cellValue.add(itemNode.asText());
                                    });
                                    String body = objectMapper.writeValueAsString(exampleTemplateValues);
                                    stepGiven.And("I set body to " + body);
                                }
                                if (itemParameter instanceof PathParameter) {
                                    PathParameter inParam = (PathParameter) itemParameter;
                                    String keyPathParam = inParam.getName();
                                    Object valuePathParam = inParam.getExample();
                                    headersData.add(keyPathParam);
                                    cellValue.add(valuePathParam.toString());
                                }
                            }
                            rowsDataValues.add(cellValue);
                            scenarioOutline.When("I " + httpMethod + " to `apigeeDomain`" + basePath + prefixNameWithPathParams(key));
                            Step stepThen = scenarioOutline.Then("response code should be " + code);
                            stepThen.And("response body should be valid json");
                            if (exampleResponseJsonString != null) {
                                Map<String, Object> exampleResponseValues = this.flattExample(exampleResponseJsonString);
                                exampleResponseValues.forEach((itemKey, itemValue) -> {
                                    String regexTarget = "\\[.*\\]";
                                    itemKey = itemKey.replaceAll(regexTarget, "[*]");
                                    stepThen.And("response body path $." + itemKey + " should be " + itemValue);
                                });
                            }
                            scenarioOutline.Examples(headersData, rowsDataValues, 19);
                        } else {
                            ScenarioImp scenario = feature.Scenario(nameScenario);
                            Step stepGiven = scenario.Given("I set Content-Type header to application/json");
                            stepGiven.And("I set bearer token");
                            stepGiven.And("I have valid client TLS configuration");
                            scenario.When("I " + httpMethod + "to `apigeeDomain`" + basePath + prefixNameWithPathParams(key));
                            Step stepThen = scenario.Then("response code should be " + code);
                            stepThen.And("response body should be valid json");
                            if (exampleResponseJson != null) {
                                Map<String, Object> exampleResponseValues = this.flattExample(exampleResponseJsonString);
                                exampleResponseValues.forEach((itemKey, itemValue) -> {
                                    String regexTarget = "\\[.*\\]";
                                    itemKey = itemKey.replaceAll(regexTarget, "[*]");
                                    stepThen.And("response body path $." + itemKey + " should be " + itemValue);
                                });
                            }
                        }
                    }
                    String features = basePathTestFeatures + "/" + prefixName + "-" + methodGroup + ".feature";
                    ZipEntry zipEntryFeatures = new ZipEntry(features);
                    zipOut.putNextEntry(zipEntryFeatures);
                    zipOut.write(feature.toStringFormat().getBytes());
                    zipOut.closeEntry();
                }
            }
        }
    }

    private Map<String, Object> flattExample(String exampleResponseJson) throws JsonProcessingException {
        return JsonFlattener.flattenAsMap(exampleResponseJson);
    }

    private String prefixName(String resourceName) {
        String prefixName = resourceName;
        if (prefixName.startsWith("/")) {
            prefixName = prefixName.substring(1, prefixName.length());
        }
        prefixName = prefixName.replace("/", "-");
        prefixName = prefixName.replace("{", "");
        prefixName = prefixName.replace("}", "");
        return prefixName;
    }

    private String prefixNameWithPathParams(String resourceName) {
        String prefixName = resourceName;
        prefixName = prefixName.replace("{", "<");
        prefixName = prefixName.replace("}", ">");
        return prefixName;
    }

    private void getApiMocker(ZipOutputStream zipOut, Swagger swagger) throws IOException {
        Map<String, io.swagger.models.Path> paths = swagger.getPaths();
        if (paths != null) {
            String basePathNode = "node";
            String basePathMocks = "node/mock";
            ApiMockerObject apiMockerObject = new ApiMockerObject("../" + basePathMocks, false, 9000, 50, false);
            Map<String, ResourceServiceObject> webServicesMap = new HashMap<>();
            for (Map.Entry<String, io.swagger.models.Path> entrySet : paths.entrySet()) {
                String key = entrySet.getKey();
                io.swagger.models.Path value = entrySet.getValue();
                Map<HttpMethod, Operation> operations = value.getOperationMap();
                String prefixName = this.prefixName(key);
                String serviceName = key;
                if (serviceName.startsWith("/")) {
                    serviceName = serviceName.substring(1, serviceName.length());
                }
                serviceName = serviceName.replace("{", ":");
                serviceName = serviceName.replace("}", "");
                List<String> verbs = new ArrayList<>();
                List<String> switchs = new ArrayList<>();
                Map<HttpMethod, Operation> swaggerOperationMap = value.getOperationMap();
                if (swaggerOperationMap != null) {
                    swaggerOperationMap.forEach((httpMethodItem, operationItem) -> {
                        verbs.add(httpMethodItem.name().toLowerCase());
                    });
                }
                Map<String, ResponsesMethodsObject> responsesMethodsObjectMap = new HashMap<>();
                Map<String, SwitchesResponses> switchesResponsesMap = new HashMap<>();
                for (Map.Entry<HttpMethod, Operation> entrySetOperations : operations.entrySet()) {
                    HttpMethod httpMethod = entrySetOperations.getKey();
                    Operation itemOperation = entrySetOperations.getValue();
                    String methodGroup = httpMethod.name().toLowerCase();
                    Map<String, Response> responses = itemOperation.getResponses();
                    List<Parameter> swaggerParameters = itemOperation.getParameters();
                    String switchResponseSuccessKey = "";
                    if (swaggerParameters != null) {
                        for (Parameter itemParameter : swaggerParameters) {
                            if (itemParameter instanceof BodyParameter) {
                                BodyParameter inParam = (BodyParameter) itemParameter;
                                RefModel refModel = (RefModel) inParam.getSchema();
                                ObjectNode example = this.getDefinition(swagger, refModel.getSimpleRef(), true);
                                String inParamString = objectMapper.writer().withDefaultPrettyPrinter().writeValueAsString(example);
                                Map<String, Object> flattenJson = JsonFlattener.flattenAsMap(inParamString);
                                for (Map.Entry<String, Object> path : flattenJson.entrySet()) {
                                    String keyBodyParam = "$." + path.getKey();
                                    Object valueBodyParam = path.getValue();
                                    String encodeURL = URLEncoder.encode(valueBodyParam.toString(), "UTF-8");
                                    if (!switchs.contains(keyBodyParam)) {
                                        switchs.add(keyBodyParam);
                                    }
                                    switchResponseSuccessKey += keyBodyParam + encodeURL;
                                }
                            }
                            if (itemParameter instanceof PathParameter) {
                                PathParameter inParam = (PathParameter) itemParameter;
                                String keyPathParam = inParam.getName();
                                Object valuePathParam = inParam.getExample();
                                if (!switchs.contains(keyPathParam)) {
                                    switchs.add(keyPathParam);
                                    switchResponseSuccessKey += keyPathParam + valuePathParam;
                                } else {
                                    if (inParam.getType().equals("integer")) {
                                        if (valuePathParam instanceof Long) {
                                            switchResponseSuccessKey += keyPathParam + ((Long) valuePathParam + 1);
                                        } else if (inParam.getExample() instanceof Integer) {
                                            switchResponseSuccessKey += keyPathParam + ((Integer) valuePathParam + 1);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    responsesMethodsObjectMap.put(methodGroup, new ResponsesMethodsObject());
                    for (Map.Entry<String, Response> entrySetResponse : responses.entrySet()) {
                        String code = entrySetResponse.getKey();
                        Response itemResponse = entrySetResponse.getValue();
                        String prefixNameTemp = "codigo-respuesta-" + code;
                        Map<String, Object> examples = itemResponse.getExamples();
                        for (Map.Entry<String, Object> entrySetExample : examples.entrySet()) {
                            String exampleType = entrySetExample.getKey();
                            String example;
                            String finalName;
                            if (exampleType.equals(MediaType.APPLICATION_JSON_VALUE)) {
                                finalName = prefixNameTemp + ".json";
                                example = objectMapper.writer().withDefaultPrettyPrinter().writeValueAsString(entrySetExample.getValue());
                            } else {
                                finalName = prefixNameTemp + ".txt";
                                example = entrySetExample.getValue().toString();
                            }
                            String fullPathMockFile = basePathMocks + "/" + prefixName + "/" + methodGroup + "/" + finalName;
                            String pathMockFile = prefixName + "/" + methodGroup + "/" + finalName;
                            ZipEntry zipEntry = new ZipEntry(fullPathMockFile);
                            zipOut.putNextEntry(zipEntry);
                            zipOut.write(example.getBytes());
                            zipOut.closeEntry();
                            String defaultPathMockFile;
                            Integer defaultCode;
                            if (code.equals("200") || code.equals("201")) {
                                switchesResponsesMap.put(switchResponseSuccessKey, new SwitchesResponses(new Integer(code), pathMockFile));
                            }
                            if (code.equals("400")) {
                                defaultPathMockFile = pathMockFile;
                                defaultCode = new Integer(code);
                                responsesMethodsObjectMap.get(methodGroup).setMockFile(defaultPathMockFile);
                                responsesMethodsObjectMap.get(methodGroup).setHttpStatus(defaultCode);
                            } else {
                                defaultPathMockFile = pathMockFile;
                                defaultCode = new Integer(code);
                            }
                        }
                    }
                }
                ResourceServiceObject resourceServiceObject = new ResourceServiceObject(10, verbs, switchs);
                resourceServiceObject.setResponses(responsesMethodsObjectMap);
                resourceServiceObject.setSwitchResponses(switchesResponsesMap);
                webServicesMap.put(serviceName, resourceServiceObject);
            }
            apiMockerObject.setWebServices(webServicesMap);
            String configGenerated = basePathNode + "/config-generated.json";
            ZipEntry zipEntryConfigGenerated = new ZipEntry(configGenerated);
            zipOut.putNextEntry(zipEntryConfigGenerated);
            zipOut.write(objectMapper.writer().withDefaultPrettyPrinter().writeValueAsString(apiMockerObject).getBytes());
            zipOut.closeEntry();
        }
    }

}
