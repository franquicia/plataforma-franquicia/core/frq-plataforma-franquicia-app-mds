/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.swagger.filters;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

@Component
public class SwaggerFilterCors extends GenericFilterBean {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        if (!httpServletResponse.containsHeader("Access-Control-Allow-Origin")) {
            httpServletResponse.addHeader("Access-Control-Allow-Origin", "*");
        }
        if (!httpServletResponse.containsHeader("Access-Control-Allow-Headers")) {
            httpServletResponse.addHeader("Access-Control-Allow-Headers", "*");
        }
        if (!httpServletResponse.containsHeader("Access-Control-Allow-Methods")) {
            httpServletResponse.addHeader("Access-Control-Allow-Methods", "*");
        }
        if (!httpServletResponse.containsHeader("Access-Control-Expose-Headers")) {
            httpServletResponse.addHeader("Access-Control-Expose-Headers", "*");
        }
        if (httpServletRequest.getMethod().equals("OPTIONS")) {
            httpServletResponse.setStatus(HttpServletResponse.SC_ACCEPTED);
            return;
        }
        chain.doFilter(httpServletRequest, httpServletResponse);
    }
}
