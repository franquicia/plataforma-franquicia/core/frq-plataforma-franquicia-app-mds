/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.swagger.rest;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.frq.swagger.bi.SwaggerBI;
import io.swagger.models.Swagger;
import io.swagger.parser.SwaggerParser;
import io.swagger.util.Yaml;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cescobarh
 */
@RestController
@RequestMapping("/v2/api-docs")
public class ExtensionRest {

    @Autowired
    SwaggerBI swaggerBI;

    @Autowired
    HttpServletRequest httpServletRequest;

    @RequestMapping(value = "custom/find", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity customFindApiName(@RequestParam("basePath") String basePath) throws IOException {
        ArrayNode swagger = swaggerBI.customFindApiName(httpServletRequest, basePath);
        return ResponseEntity.status(HttpStatus.OK).body(swagger);
    }

    @RequestMapping(value = "custom", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity customApiDocs() {
        try {
            ObjectNode swagger = swaggerBI.getBaseApiDocsModified(false);
            return ResponseEntity.status(HttpStatus.OK).body(swagger);
        } catch (IOException ex) {
            Logger.getLogger(ExtensionRest.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "custom/yaml", method = RequestMethod.GET, produces = "application/yaml")
    @ResponseBody
    public ResponseEntity yaml() {
        try {
            Swagger swagger = new SwaggerParser().read(swaggerBI.getBaseApiDocsModified(false));
            String yamlOutput = Yaml.pretty().writeValueAsString(swagger);
            HttpHeaders headers = new HttpHeaders();
            headers.set("Content-Disposition", "inline;filename=swagger.yaml");
            return ResponseEntity.status(HttpStatus.OK).headers(headers).body(yamlOutput);
        } catch (IOException ex) {
            Logger.getLogger(ExtensionRest.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "custom/html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    @ResponseBody
    public ResponseEntity html(@RequestParam(name = "forceUpdate", required = false) String forceUpdate) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(swaggerBI.getApiDocsHTML(forceUpdate != null, false));
        } catch (IOException ex) {
            Logger.getLogger(ExtensionRest.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "custom/pdf", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
    @ResponseBody
    public ResponseEntity pdf(@RequestParam(name = "forceUpdate", required = false) String forceUpdate) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(swaggerBI.getApiDocsPDF(forceUpdate != null, false));
        } catch (IOException ex) {
            Logger.getLogger(ExtensionRest.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "custom/template", method = RequestMethod.GET, produces = "application/zip")
    @ResponseBody
    public ResponseEntity apiMocker() {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Content-Disposition", "inline;filename=swagger-api-mocker.zip");
            return ResponseEntity.status(HttpStatus.OK).headers(headers).body(swaggerBI.getTemplateTest("swagger-api-mocker", false));
        } catch (IOException ex) {
            Logger.getLogger(ExtensionRest.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "custom/group/{nameGroup}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity group(@PathVariable("nameGroup") String nameGroup) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(swaggerBI.getApiDocsByGroup(nameGroup, false));
        } catch (IOException ex) {
            Logger.getLogger(ExtensionRest.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "custom/group/{nameGroup}/html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    @ResponseBody
    public ResponseEntity groupHtml(@PathVariable("nameGroup") String nameGroup, @RequestParam(name = "forceUpdate", required = false) String forceUpdate) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(swaggerBI.getApiDocsByGroupHTML(nameGroup, forceUpdate != null, false));
        } catch (IOException ex) {
            Logger.getLogger(ExtensionRest.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "custom/group/{nameGroup}/pdf", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
    @ResponseBody
    public ResponseEntity groupPdf(@PathVariable("nameGroup") String nameGroup, @RequestParam(name = "forceUpdate", required = false) String forceUpdate) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(swaggerBI.getApiDocsByGroupPDF(nameGroup, forceUpdate != null, false));
        } catch (IOException ex) {
            Logger.getLogger(ExtensionRest.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "custom/group/{nameGroup}/yaml", method = RequestMethod.GET, produces = "application/yaml")
    @ResponseBody
    public ResponseEntity groupYaml(@PathVariable("nameGroup") String nameGroup) {
        try {
            Swagger swagger = new SwaggerParser().read(swaggerBI.getApiDocsByGroup(nameGroup, false));
            String yamlOutput = Yaml.pretty().writeValueAsString(swagger);
            HttpHeaders headers = new HttpHeaders();
            headers.set("Content-Disposition", "inline;filename=" + nameGroup + ".yaml");
            return ResponseEntity.status(HttpStatus.OK).headers(headers).body(yamlOutput);
        } catch (IOException ex) {
            Logger.getLogger(ExtensionRest.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "custom/group/{nameGroup}/template", method = RequestMethod.GET, produces = "application/zip")
    @ResponseBody
    public ResponseEntity groupApiMocker(@PathVariable("nameGroup") String nameGroup) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Content-Disposition", "inline;filename=" + nameGroup + "-api-mocker.zip");
            return ResponseEntity.status(HttpStatus.OK).headers(headers).body(swaggerBI.getTemplateTest(nameGroup, false));
        } catch (IOException ex) {
            Logger.getLogger(ExtensionRest.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "custom/apigee", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity customApiDocsApigee() {
        try {
            ObjectNode swagger = swaggerBI.getBaseApiDocsModified(true);
            return ResponseEntity.status(HttpStatus.OK).body(swagger);
        } catch (IOException ex) {
            Logger.getLogger(ExtensionRest.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "custom/apigee/yaml", method = RequestMethod.GET, produces = "application/yaml")
    @ResponseBody
    public ResponseEntity apigeeYaml() {
        try {
            Swagger swagger = new SwaggerParser().read(swaggerBI.getBaseApiDocsModified(true));
            String yamlOutput = Yaml.pretty().writeValueAsString(swagger);
            HttpHeaders headers = new HttpHeaders();
            headers.set("Content-Disposition", "inline;filename=swagger.yaml");
            return ResponseEntity.status(HttpStatus.OK).headers(headers).body(yamlOutput);
        } catch (IOException ex) {
            Logger.getLogger(ExtensionRest.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "custom/apigee/html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    @ResponseBody
    public ResponseEntity apigeeHtml(@RequestParam(name = "forceUpdate", required = false) String forceUpdate) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(swaggerBI.getApiDocsHTML(forceUpdate != null, true));
        } catch (IOException ex) {
            Logger.getLogger(ExtensionRest.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "custom/apigee/pdf", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
    @ResponseBody
    public ResponseEntity apigeePdf(@RequestParam(name = "forceUpdate", required = false) String forceUpdate) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(swaggerBI.getApiDocsPDF(forceUpdate != null, true));
        } catch (IOException ex) {
            Logger.getLogger(ExtensionRest.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "custom/group/{nameGroup}/apigee", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity groupApigee(@PathVariable("nameGroup") String nameGroup) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(swaggerBI.getApiDocsByGroup(nameGroup, true));
        } catch (IOException ex) {
            Logger.getLogger(ExtensionRest.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "custom/group/{nameGroup}/apigee/html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    @ResponseBody
    public ResponseEntity groupApigeeHtml(@PathVariable("nameGroup") String nameGroup, @RequestParam(name = "forceUpdate", required = false) String forceUpdate) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(swaggerBI.getApiDocsByGroupHTML(nameGroup, forceUpdate != null, true));
        } catch (IOException ex) {
            Logger.getLogger(ExtensionRest.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "custom/group/{nameGroup}/apigee/pdf", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
    @ResponseBody
    public ResponseEntity groupApigeePdf(@PathVariable("nameGroup") String nameGroup, @RequestParam(name = "forceUpdate", required = false) String forceUpdate) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(swaggerBI.getApiDocsByGroupPDF(nameGroup, forceUpdate != null, true));
        } catch (IOException ex) {
            Logger.getLogger(ExtensionRest.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "custom/group/{nameGroup}/apigee/yaml", method = RequestMethod.GET, produces = "application/yaml")
    @ResponseBody
    public ResponseEntity groupApigeeYaml(@PathVariable("nameGroup") String nameGroup) {
        try {
            Swagger swagger = new SwaggerParser().read(swaggerBI.getApiDocsByGroup(nameGroup, true));
            String yamlOutput = Yaml.pretty().writeValueAsString(swagger);
            HttpHeaders headers = new HttpHeaders();
            headers.set("Content-Disposition", "inline;filename=" + nameGroup + ".yaml");
            return ResponseEntity.status(HttpStatus.OK).headers(headers).body(yamlOutput);
        } catch (IOException ex) {
            Logger.getLogger(ExtensionRest.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "custom/group/{nameGroup}/apigee/template", method = RequestMethod.GET, produces = "application/zip")
    @ResponseBody
    public ResponseEntity groupApigeeApiMocker(@PathVariable("nameGroup") String nameGroup) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Content-Disposition", "inline;filename=" + nameGroup + "-api-mocker.zip");
            return ResponseEntity.status(HttpStatus.OK).headers(headers).body(swaggerBI.getTemplateTest(nameGroup, true));
        } catch (IOException ex) {
            Logger.getLogger(ExtensionRest.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseEntity.notFound().build();
        }
    }

}
