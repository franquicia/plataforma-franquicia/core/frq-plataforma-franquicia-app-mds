/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.frq.swagger.api.mocker;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Map;

/**
 *
 * @author cescobarh
 */
public class ResourceServiceObject {

    private int latency;
    private List<String> verbs;
    @JsonProperty(value = "switch")
    private List<String> switchs;
    private Map<String, ResponsesMethodsObject> responses;
    private Map<String, SwitchesResponses> switchResponses;

    public ResourceServiceObject(int latency, List<String> verbs, List<String> switchs) {
        this.latency = latency;
        this.verbs = verbs;
        this.switchs = switchs;
    }

    public int getLatency() {
        return latency;
    }

    public void setLatency(int latency) {
        this.latency = latency;
    }

    public List<String> getVerbs() {
        return verbs;
    }

    public void setVerbs(List<String> verbs) {
        this.verbs = verbs;
    }

    public List<String> getSwitchs() {
        return switchs;
    }

    public void setSwitchs(List<String> switchs) {
        this.switchs = switchs;
    }

    public Map<String, ResponsesMethodsObject> getResponses() {
        return responses;
    }

    public void setResponses(Map<String, ResponsesMethodsObject> responses) {
        this.responses = responses;
    }

    public Map<String, SwitchesResponses> getSwitchResponses() {
        return switchResponses;
    }

    public void setSwitchResponses(Map<String, SwitchesResponses> switchResponses) {
        this.switchResponses = switchResponses;
    }

}
