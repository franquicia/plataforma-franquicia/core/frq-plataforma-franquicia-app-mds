/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.version.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.Util;
import com.gs.baz.version.dto.VersionDTO;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.stream.Stream;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author cescobarh
 */
public class Version {

    private transient final String rootPath = "/franquicia/plataforma_franquicia/version/";
    final private transient SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
    final private transient static Logger logger = LogManager.getLogger(Version.class);
    private VersionDTO version;
    private Calendar deployedDate;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private ApplicationContext applicationContext;

    public VersionDTO getVersion() {
        try {
            Path pathFile = getPathFileVersion();
            String versionJson = new String(Files.readAllBytes(pathFile));
            return objectMapper.readValue(versionJson, VersionDTO.class);
        } catch (IOException | CustomException ex) {
            logger.log(Level.ERROR, ex.getMessage() + ex);
            return null;
        }
    }

    public List<VersionDTO> getAllVersions() {
        List<VersionDTO> versionDTOs = new ArrayList<>();
        try (Stream<Path> paths = Files.walk(Paths.get(rootPath), Integer.MAX_VALUE)) {
            paths.filter(item -> {
                return item.toString().contains(applicationContext.getApplicationName());
            }).forEach(item -> {
                try {
                    if (item.toString().endsWith(".info")) {
                        String versionJson = new String(Files.readAllBytes(item));
                        VersionDTO versionDTO = objectMapper.readValue(versionJson, VersionDTO.class);
                        versionDTOs.add(versionDTO);
                    }
                } catch (IOException ex) {
                    logger.log(Level.ERROR, ex.getMessage(), ex);
                }
            });
        } catch (IOException ex) {
            logger.log(Level.ERROR, ex.getMessage() + ex);
        }
        return versionDTOs;
    }

    public VersionDTO getBuildProperties() {
        deployedDate = Calendar.getInstance();
        String pathFileBuild = "/build.properties";
        version = new VersionDTO();
        try (InputStream is = this.getClass().getResourceAsStream(pathFileBuild)) {
            URL url = this.getClass().getResource(pathFileBuild);
            long date = url.openConnection().getLastModified();
            Properties prop = new Properties();
            prop.load(is);
            deployedDate.setTimeInMillis(date);
            version.setAppName(prop.getProperty("nameApp"));
            version.setDeployedDate(formatter.format(deployedDate.getTime()));
            version.setHost(Util.getIPLocal());
        } catch (Exception ex) {
            logger.info("No se pudo encontrar o leer el archivo build.properties " + ex.getMessage());
        }
        return version;
    }

    private Path getPathFileVersion() throws CustomException {
        String hostName = Util.getHostName();
        Path path = Paths.get(rootPath + hostName + "/" + applicationContext.getApplicationName());
        try {
            if (!Files.exists(path)) {
                Files.createDirectories(path);
            }
            Path pathFile = Paths.get(path.toString() + "/deploy.info");
            if (!Files.exists(pathFile)) {
                pathFile = Files.createFile(pathFile);
            }
            return pathFile;
        } catch (IOException ex) {
            throw new CustomException(ex);
        }

    }

    public Path writeFileVersion() throws CustomException {
        try {
            Path pathFile = getPathFileVersion();
            VersionDTO versionDTO = this.getBuildProperties();
            String buildVersionJson = objectMapper.writeValueAsString(versionDTO);
            Files.write(pathFile, buildVersionJson.getBytes());
            return pathFile;
        } catch (IOException ex) {
            throw new CustomException(ex);
        }
    }

    private String calculateVersion(long buildNumber) {
        long buildNumberAux = buildNumber;
        int patchVersion = (short) (buildNumberAux % 51);
        buildNumberAux /= 51;
        int minorVersion = (short) (buildNumberAux % 21);
        buildNumberAux /= 21;
        int majorVersion = (short) (buildNumberAux % 11);
        return (majorVersion + "." + minorVersion + "." + patchVersion + "." + buildNumber);
    }
}
