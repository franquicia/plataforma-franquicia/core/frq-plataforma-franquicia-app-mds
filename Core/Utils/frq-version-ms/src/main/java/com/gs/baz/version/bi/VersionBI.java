/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.version.bi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.frq.model.commons.Util;
import com.gs.baz.version.dto.VersionDTO;
import java.util.ArrayList;
import java.util.List;
import com.gs.baz.version.util.Version;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author cescobarh
 */
public class VersionBI {

    private final ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    private Version version;

    public VersionDTO getVersion() throws CustomException {
        return version.getVersion();
    }

    public ObjectNode infoNodes(String port) {
        ObjectNode jsonOut;
        ObjectNode nodesContainer = objectMapper.createObjectNode();
        ArrayNode arrayNodes = objectMapper.createArrayNode();
        ObjectNode nodeVersion;
        List<String> versions = new ArrayList<>();
        for (VersionDTO node : version.getAllVersions()) {
            nodeVersion = objectMapper.createObjectNode();
            nodeVersion.put("node", node.getHost());
            node.setHost(null);
            ObjectNode jsonResponse = objectMapper.convertValue(node, ObjectNode.class);
            nodeVersion.set("node_version", jsonResponse);
            arrayNodes.add(nodeVersion);
            if (node.getGitInfo() != null) {
                versions.add(node.getGitInfo().get("git.closest.tag.name").asText());
            }
        }
        nodesContainer.set("nodes", arrayNodes);
        Map<String, Long> result = versions.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        nodesContainer.put("unpairing_versions", (result.isEmpty() ? null : (result.size() > 1)));
        ObjectNode current = objectMapper.createObjectNode();
        current.put("host", Util.getIPLocal());
        current.put("local_port", port);
        nodesContainer.set("current", current);
        jsonOut = objectMapper.convertValue(nodesContainer, ObjectNode.class);
        return jsonOut;
    }

}
