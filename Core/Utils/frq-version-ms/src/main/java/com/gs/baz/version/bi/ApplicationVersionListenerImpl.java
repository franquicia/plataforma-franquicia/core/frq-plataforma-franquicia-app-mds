/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.version.bi;

import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.version.util.Version;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 *
 * @author cescobarh
 */
@Component
public class ApplicationVersionListenerImpl implements ApplicationListener {

    final private Logger logger = LogManager.getLogger();

    @Autowired
    private Version version;

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof ContextRefreshedEvent) {
            try {
                version.writeFileVersion();
            } catch (CustomException ex) {
                logger.log(Level.ERROR, ex.getMessage(), ex);
            }
        }
    }
}
