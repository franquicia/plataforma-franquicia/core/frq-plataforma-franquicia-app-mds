package com.gs.baz.version.rest;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.frq.model.commons.CustomException;
import com.gs.baz.version.bi.VersionBI;
import com.gs.baz.version.dto.VersionDTO;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/service/version")
public class ServicioVersion {

    @Autowired
    private VersionBI versionBI;
    private final Logger logger = LogManager.getLogger();

    @Autowired
    HttpServletRequest httpServletRequest;

    @RequestMapping(value = "/info", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VersionDTO getVersion() throws CustomException {
        return versionBI.getVersion();
    }

    @RequestMapping(value = "/info/nodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ObjectNode getVersionNodes() {
        String port = httpServletRequest.getLocalPort() + "";
        return versionBI.infoNodes(port);
    }
}
