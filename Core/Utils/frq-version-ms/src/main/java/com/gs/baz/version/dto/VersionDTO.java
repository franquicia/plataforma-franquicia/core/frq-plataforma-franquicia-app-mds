/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.version.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 *
 * @author cescobarh
 */
public class VersionDTO {

    @JsonProperty(value = "app_name")
    private String appName;

    @JsonProperty(value = "deployed_date")
    private String deployedDate;

    @JsonProperty(value = "host")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String host;

    @JsonProperty(value = "git_info")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private ObjectNode gitInfo;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getDeployedDate() {
        return deployedDate;
    }

    public void setDeployedDate(String deployedDate) {
        this.deployedDate = deployedDate;
    }

    public ObjectNode getGitInfo() {
        return gitInfo;
    }

    public void setGitInfo(ObjectNode gitInfo) {
        this.gitInfo = gitInfo;
    }

    @Override
    public String toString() {
        return "VersionDTO{" + "appName=" + appName + ", deployedDate=" + deployedDate + ", host=" + host + ", gitInfo=" + gitInfo + '}';
    }

}
