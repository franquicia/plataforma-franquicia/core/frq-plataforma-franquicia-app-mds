package com.gs.baz.version.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.version.bi.VersionBI;
import com.gs.baz.version.util.Version;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.version")
public class ConfigVersion {

    private final Logger logger = LogManager.getLogger();

    public ConfigVersion() {
        logger.info("Loading ConfigVersion...!");
    }

    @Bean
    public Version version() {
        return new Version();
    }

    @Bean
    public VersionBI versionBI() {
        return new VersionBI();
    }

}
