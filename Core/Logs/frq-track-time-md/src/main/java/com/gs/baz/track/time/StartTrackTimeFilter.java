/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.track.time;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gs.baz.frq.model.commons.Util;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;

/**
 *
 * @author cescobarh
 */
@Component
public class StartTrackTimeFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        String requestId = this.getRequestID();
        String ipOrigin = httpServletRequest.getHeader("x-forwarded-for");
        if (ipOrigin == null) {
            ipOrigin = httpServletRequest.getRemoteAddr();
        }
        httpServletRequest.setAttribute("ipOrigin", ipOrigin);
        httpServletRequest.setAttribute("requestID", requestId);
        httpServletRequest.setAttribute("startTrackTime", System.currentTimeMillis());
        List<ObjectNode> traceTrackTimeMethods = new ArrayList<>();
        httpServletRequest.setAttribute("traceTrackTimeMethods", traceTrackTimeMethods);
        chain.doFilter(httpServletRequest, httpServletResponse);
    }

    private String getRequestID() {
        Calendar time = Calendar.getInstance();
        String serverIP = Util.getIPLocal();
        String[] ipHostSplit = serverIP.split("\\.");
        String ceroIp = "";
        String ceroMonth = "";
        String ceroDay = "";
        String ceroHour = "";
        String ceroMinute = "";
        String ceroSecond = "";
        int hostID = new Integer(ipHostSplit[3]);
        if (hostID < 100) {
            ceroIp = "0";
        }
        int year = time.get(Calendar.YEAR);
        int month = time.get(Calendar.MONTH) + 1;
        if (month < 10) {
            ceroMonth = "0";
        }
        int day = time.get(Calendar.DAY_OF_MONTH);
        if (day < 10) {
            ceroDay = "0";
        }
        int hour = time.get(Calendar.HOUR);
        if (hour < 10) {
            ceroHour = "0";
        }
        int minute = time.get(Calendar.MINUTE);
        if (minute < 10) {
            ceroMinute = "0";
        }
        int second = time.get(Calendar.SECOND);
        if (second < 10) {
            ceroSecond = "0";
        }
        int millisecond = time.get(Calendar.MILLISECOND);
        String serverId = (ceroIp + hostID) + year + (ceroMonth + month) + (ceroDay + day) + (ceroHour + hour) + (ceroMinute + minute) + (ceroSecond + second) + millisecond;
        return serverId;
    }

    @Override
    public void destroy() {
    }

}
