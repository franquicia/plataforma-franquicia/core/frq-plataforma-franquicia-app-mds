/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.track.time;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.stereotype.Component;

/**
 *
 * @author cescobarh
 */
@Component
public class EndTrackTimeFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        try {
            chain.doFilter(httpServletRequest, httpServletResponse);
        } finally {
            Long startTimeRequest = (Long) httpServletRequest.getAttribute("startTrackTime");
            List<ObjectNode> traceTrackTimeMethods = (List<ObjectNode>) httpServletRequest.getAttribute("traceTrackTimeMethods");
            if (startTimeRequest != null && traceTrackTimeMethods != null) {
                Long endTimeResponse = System.currentTimeMillis();
                Long time = (endTimeResponse - startTimeRequest);
                ThreadContext.put("totalTime", time + "");
                ThreadContext.put("traceServices", new ObjectMapper().writeValueAsString(traceTrackTimeMethods));
            }
        }
    }

    @Override
    public void destroy() {
    }

}
