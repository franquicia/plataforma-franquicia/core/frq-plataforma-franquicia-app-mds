package com.gs.baz.track.time.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.track.time.EndTrackTimeFilter;
import com.gs.baz.track.time.StartTrackTimeFilter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 *
 * @author cescobarh
 */
@Configuration
@ComponentScan("com.gs.baz.track.time")
@EnableAspectJAutoProxy
public class ConfigTrackTime implements WebMvcConfigurer {

    private final Logger logger = LogManager.getLogger();

    public ConfigTrackTime() {
        logger.info("Loading " + getClass().getName() + "...!");
    }

    @Bean
    public FilterRegistrationBean<StartTrackTimeFilter> resourcesFilterStartTrackTime() {
        FilterRegistrationBean<StartTrackTimeFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new StartTrackTimeFilter());
        registrationBean.addUrlPatterns("/*");
        registrationBean.setOrder(FilterRegistrationBean.HIGHEST_PRECEDENCE);
        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean<EndTrackTimeFilter> resourcesFilterEndTrackTime() {
        FilterRegistrationBean<EndTrackTimeFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new EndTrackTimeFilter());
        registrationBean.addUrlPatterns("/*");
        registrationBean.setOrder(FilterRegistrationBean.LOWEST_PRECEDENCE);
        return registrationBean;
    }
}
