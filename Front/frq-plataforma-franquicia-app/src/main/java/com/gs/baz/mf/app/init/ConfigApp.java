package com.gs.baz.mf.app.init;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.gs.baz.frq.data.sources.config.GenericConfig;
import com.gs.baz.frq.model.commons.Util;
import com.gs.baz.mf.app.filters.ControlCacheFilter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 *
 * @author cescobarh
 */
@Import(GenericConfig.class)
@Configuration
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
@ComponentScan("com.gs.baz.mf.app")
public class ConfigApp {

    private final Logger logger = LogManager.getLogger();

    public ConfigApp() {
        final String ipHost = Util.getIPLocal();
        logger.info("Loading Base " + getClass().getName() + "...! from " + ipHost);
    }

    @Bean
    public FilterRegistrationBean<ControlCacheFilter> resourcesFilterControlCache() {
        FilterRegistrationBean<ControlCacheFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new ControlCacheFilter());
        registrationBean.addUrlPatterns("/*");
        registrationBean.setOrder(FilterRegistrationBean.HIGHEST_PRECEDENCE);
        return registrationBean;
    }
}
