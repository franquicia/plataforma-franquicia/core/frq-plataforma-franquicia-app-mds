/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gs.baz.mf.app.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 *
 * @author cescobarh
 */
@Component
@Order(3)
public class RequestVueFilter implements Filter {

    private static final String VIEW_ROOT_PATH = "/";

    /**
     *
     * @param filterConfig
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    /**
     *
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = ((HttpServletRequest) request);
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        String requestURI = httpServletRequest.getRequestURI();
        boolean exclude = !requestURI.contains(".");
        exclude &= !requestURI.contains("session");
        exclude &= !requestURI.contains("saml");
        exclude &= !requestURI.contains("version");
        exclude &= !requestURI.contains("actuator");
        if (requestURI.startsWith(VIEW_ROOT_PATH) && exclude) {
            httpServletRequest.getRequestDispatcher(VIEW_ROOT_PATH).forward(request, response);
        } else {
            chain.doFilter(httpServletRequest, httpServletResponse);
        }
    }

    /**
     *
     */
    @Override
    public void destroy() {

    }

}
