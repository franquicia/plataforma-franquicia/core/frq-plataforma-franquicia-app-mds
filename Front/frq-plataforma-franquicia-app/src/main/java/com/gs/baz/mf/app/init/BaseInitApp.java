package com.gs.baz.mf.app.init;

import com.gs.baz.access.log.expose.EnableAccessLog;
import com.gs.baz.actuator.expose.EnableActuator;
import com.gs.baz.master.key.dsi.saml.expose.EnableMasterKeySaml;
import com.gs.baz.read.propiedades.expose.EnableReadPropiedades;
import com.gs.baz.track.time.expose.EnableTrackTime;
import com.gs.baz.version.expose.EnableVersion;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;

/**
 *
 * @author cescobarh
 */
@SpringBootApplication
@Import({ConfigApp.class})
@EnableMasterKeySaml
@EnableTrackTime
@EnableAccessLog
@EnableVersion
@EnableActuator
@EnableReadPropiedades
public class BaseInitApp extends SpringBootServletInitializer {

    /**
     *
     * @param application
     * @return
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(BaseInitApp.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(BaseInitApp.class, args);
    }

}
