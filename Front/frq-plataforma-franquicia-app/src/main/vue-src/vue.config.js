//https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-pwa#configuration
module.exports = {
  devServer: { 
    port: 9090
  },
  publicPath: "/plataforma-franquicia-app/",
  outputDir: "../webapp",
  transpileDependencies: ["vuetify"],
  productionSourceMap: false,
  configureWebpack: {
    devtool: "source-map"
  },
  pwa: {
    name: "Plataforma Franquicia",
    short_name: "Plataforma Franquicia",
    themeColor: "#ffcd00",
    msTileColor: "#ffcd00",
    workboxPluginMode: "InjectManifest",
    workboxOptions: {
      swSrc: "src/service-worker.js",
      exclude: [/^.*WEB-INF\/.*$/, /^.*css\/*.map$/, /index\.html$/]
    }
  }
};
