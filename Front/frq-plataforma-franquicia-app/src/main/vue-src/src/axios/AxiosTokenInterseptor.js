import axios from "axios";
import LocalStorage from "@/services/storage/LocalStorage";
import querystring from "querystring";
import createAuthRefreshInterceptor from "axios-auth-refresh";

// LocalstorageService
const LocalStorageService = LocalStorage.getService();

// Add a request interceptor
axios.interceptors.request.use(
  (config) => {
    let accessToken = LocalStorageService.getAccessToken();
    config.headers["Authorization"] = "Bearer " + accessToken;
    config.headers["Content-Security-Policy"] = "script-src 'self'";
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

// Function that will be called to refresh authorization
const refreshAuthLogic = (failedRequest) =>
  axios
    .request({
      method: "POST",
      data: querystring.stringify({
        grant_type: "refresh_token",
        refresh_token: LocalStorageService.getRefreshToken(),
      }),
      headers: {
        "content-type": "application/x-www-form-urlencoded;charset=utf-8",
      },
      auth: {
        username: "frontendapp",
        password: "12345",
      },
      url: "/security/oauth/token",
    })
    .then((tokenRefreshResponse) => {
      LocalStorageService.setToken(tokenRefreshResponse.data);
      let accessToken = LocalStorageService.getAccessToken();
      failedRequest.response.config.headers["Authentication"] =
        "Bearer " + accessToken;
      return Promise.resolve();
    })
    .catch((error) => {
      return Promise.reject(error);
    });
// Instantiate the interceptor (you can chain it as it returns the axios instance)
createAuthRefreshInterceptor(axios, refreshAuthLogic, {
  retryInstance: axios,
  pauseInstanceWhileRefreshing: true,
});
export default {};
