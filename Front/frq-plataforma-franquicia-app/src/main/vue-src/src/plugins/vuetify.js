import Vue from "vue";
import Vuetify from "vuetify/lib";
import colors from "vuetify/lib/util/colors";
import es from "vuetify/src/locale/es.ts";
import '@mdi/font/css/materialdesignicons.css' 
import '@fortawesome/fontawesome-free/css/all.css'

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'mdi',
  },
  lang: {
    locales: { es },
    current: "es"
  },
  theme: {
    themes: {
      light: {
        primary: "#ffcd00", // #E53935
        secondary: "#252728", // #FFCDD2
        tertiary: "#fafafa",
        accent: colors.indigo.base, // #3F51B5
        error: "#fd5151",
        warning: "#FD9A51"
      }
    }
  }
});
