import Vue from "vue";
import VueRouter from "vue-router";
import store from "@/store";
import routesMaquetado from "@/views/maquetado/router";
const serviceLocalStorage = store.state.serviceLocalStorage;
const varChecklist =
  process.env.NODE_ENV === "production"
    ? "http://10.53.33.83/checklist"
    : "http://10.53.33.83/checklist";

Vue.use(VueRouter);
let routes = [
  {
    path: "*",
    name: "http_404",
    component: () => import("@/views/errors/404.vue"),
  },
  {
    path: "/construyendo",
    name: "construyendo",
    component: () => import("@/views/errors/Construyendo.vue"),
  },
  {
    path: "/login/alternative",
    name: "login_alternative",
    component: () => import("@/views/AlternativeLogin.vue"),
  },
  {
    path: "/",
    name: "home",
    component: () => import("@/views/secure/Home.vue"),
  },
  {
    path: "/soporte",
    name: "soporte",
    component: () => import("@/views/secure/soporte/HomeSoporte.vue"),
  },
  {
    path: "/soporte/monitoreo",
    name: "soporte_monitoreo",
    component: () => import("@/views/secure/soporte/monitoreo/Monitoreo.vue"),
  },
  {
    path: "/soporte/monitoreo/registros",
    name: "soporte_monitoreo_registros",
    component: () =>
      import("@/views/secure/soporte/monitoreo/registros/Registros.vue"),
  },
  {
    path: "/modelo-franquicia/",
    name: "modelo_franquicia",
    component: () =>
      import("@/views/secure/modelo-franquicia/HomeModeloFranquicia.vue"),
  },
  {
    path: "/modelo-entrega/",
    name: "modelo_entrega",
    component: () =>
      import("@/views/secure/modelo-entrega/HomeModeloEntrega.vue"),
  },
  {
    path: "/sistema-informacion/",
    name: "sistema_informacion",
    component: () =>
      import("@/views/secure/sistema-informacion/HomeSistemaInformacion.vue"),
  },
  {
    path: "/sistema-informacion/dashboard/v2/graficas",
    name: "sistema_informacion_dashboard_v2_graficas",
    component: () =>
      import(
        "@/views/secure/sistema-informacion/dashboards/v2/graficas/Graficas.vue"
      ),
  },
  {
    path: "/sistema-informacion/dashboard/v2/graficas/nueva",
    name: "sistema_informacion_dashboard_v2_graficas_nueva",
    component: () =>
      import(
        "@/views/secure/sistema-informacion/dashboards/v2/graficas/components/NuevaGrafica.vue"
      ),
  },
  {
    path: "/sistema-informacion/dashboard/v2/graficas/:grafica",
    name: "sistema_informacion_dashboard_v2_graficas_edicion",
    component: () =>
      import(
        "@/views/secure/sistema-informacion/dashboards/v2/graficas/components/GestionGrafica.vue"
      ),
  },
  {
    path: "/sistema-informacion/dashboard/v2/semaforos",
    name: "sistema_informacion_dashboard_v2_semaforos",
    component: () =>
      import(
        "@/views/secure/sistema-informacion/dashboards/v2/semaforos/Semaforos.vue"
      ),
  },
  {
    path: "/sistema-informacion/dashboard/v2/semaforos/nuevo",
    name: "sistema_informacion_dashboard_v2_semaforos_nuevo",
    component: () =>
      import(
        "@/views/secure/sistema-informacion/dashboards/v2/semaforos/components/NuevoSemaforo.vue"
      ),
  },
  {
    path: "/sistema-informacion/dashboard/v2/semaforos/:semaforo",
    name: "sistema_informacion_dashboard_v2_semaforos_edicion",
    component: () =>
      import(
        "@/views/secure/sistema-informacion/dashboards/v2/semaforos/components/GestionSemaforo.vue"
      ),
  },
  {
    path: "/sistema-informacion/dashboard/v2/disciplinas",
    name: "sistema_informacion_dashboard_v2_disciplinas",
    component: () =>
      import(
        "@/views/secure/sistema-informacion/dashboards/v2/disciplinas/No12Disciplinas.vue"
      ),
  },
  {
    path: "/sistema-informacion/dashboard/v2/disciplinas/:disciplina",
    name: "sistema_informacion_dashboard_v2_disciplinas_detalle",
    component: () =>
      import(
        "@/views/secure/sistema-informacion/dashboards/v2/disciplinas/detalle-disciplina/DetalleDisciplina.vue"
      ),
  },
  {
    path: "/sistema-informacion/dashboard/v2/demos",
    name: "sistema_informacion_dashboard_v2_demos",
    component: () =>
      import(
        "@/views/secure/sistema-informacion/dashboards/v2/disciplinas/Demos.vue"
      ),
  },
  {
    path: "/sistema-informacion/dashboard",
    name: "sistema_informacion_dashboard",
    component: () =>
      import("@/views/secure/sistema-informacion/dashboards/HomeDashboard.vue"),
  },
  {
    path: "/sistema-informacion/dashboard/disciplinas",
    name: "sistema_informacion_dashboard_disciplinas",
    component: () =>
      import(
        "@/views/secure/sistema-informacion/dashboards/ContenedorDisciplinas.vue"
      ),
  },
  {
    path: "/sistema-informacion/dashboard/disciplinas/disciplina/:disciplina",
    name: "sistema_informacion_dashboard_disciplinas_disciplina",
    component: () =>
      import(
        "@/views/secure/sistema-informacion/dashboards/detalle-disciplina/ContenedorDetalleDisciplinaNivelNacional.vue"
      ),
  },
  {
    path: "/modelo-franquicia/protocolos/home",
    name: "modelo_franquicia_protocolos_home",
    component: () =>
      import("@/views/secure/modelo-franquicia/protocolos/HomeProtocolos.vue"),
  },
  {
    path: "/modelo-franquicia/protocolos/:tipo_protocolo",
    name: "modelo_franquicia_protocolos",
    component: () =>
      import("@/views/secure/modelo-franquicia/protocolos/Protocolos.vue"),
  },
  {
    path: "/modelo-franquicia/protocolos/:tipo_protocolo/404",
    name: "modelo_franquicia_protocolos_404",
    component: () => import("@/views/errors/404.vue"),
  },
  {
    path: "/modelo-franquicia/protocolos/:tipo_protocolo/nuevo/",
    name: "modelo_franquicia_protocolos_nuevo",
    component: () =>
      import(
        "@/views/secure/modelo-franquicia/protocolos/GestionProtocolo.vue"
      ),
  },
  {
    path: "/modelo-franquicia/protocolos/:tipo_protocolo/edicion/:protocolo",
    name: "modelo_franquicia_protocolos_edicion",
    component: () =>
      import(
        "@/views/secure/modelo-franquicia/protocolos/GestionProtocolo.vue"
      ),
  },
  {
    path: "/modelo-franquicia/protocolos/:tipo_protocolo/carga/excel",
    name: "modelo_franquicia_protocolos_carga_excel",
    component: () =>
      import(
        "@/views/secure/modelo-franquicia/protocolos/GestionProtocoloCargaExcel.vue"
      ),
  },
  {
    path: "/modelo-franquicia/revision/sitio",
    name: "modelo_franquicia_revision_sitio",
    component: () => import("@/views/errors/404.vue"),
  },
  {
    path: "/modelo-franquicia/aseguramiento/estandar",
    name: "modelo_franquicia_aseguramiento_estandar",
    component: () => import("@/views/errors/404.vue"),
  },
  {
    path: "/modelo-franquicia/puntos/contacto/home",
    name: "modelo_franquicia_home_puntos_contacto",
    component: () =>
      import(
        "@/views/secure/modelo-franquicia/puntos-contacto/HomePuntosContacto.vue"
      ),
  },
  {
    path: "/modelo-franquicia/puntos/contacto",
    name: "modelo_franquicia_puntos_contacto",
    component: () =>
      import(
        "@/views/secure/modelo-franquicia/puntos-contacto/GestionPuntosContacto.vue"
      ),
  },
  {
    path: "/modelo-franquicia/puntos/contacto/informacion",
    name: "modelo_franquicia_puntos_contacto_informacion",
    component: () =>
      import(
        "@/views/secure/modelo-franquicia/puntos-contacto/informacion/Principal.vue"
      ),
  },
  {
    path: "/modelo-franquicia/franquicias/",
    name: "modelo_franquicia_franquicias",
    component: () =>
      import("@/views/secure/modelo-franquicia/franquicias/Franquicias.vue"),
  },
  {
    path: "/modelo-franquicia/franquicias/nueva/",
    name: "modelo_franquicia_franquicias_nueva",
    component: () =>
      import(
        "@/views/secure/modelo-franquicia/franquicias/GestionFranquicia.vue"
      ),
  },
  {
    path: "/modelo-franquicia/franquicias/edicion/:franquicia/",
    name: "modelo_franquicia_franquicias_edicion",
    component: () =>
      import(
        "@/views/secure/modelo-franquicia/franquicias/GestionFranquicia.vue"
      ),
  },
  {
    path: "/modelo-franquicia/franquicias/:franquicia/",
    name: "modelo_franquicia_franquicias_franquicia_modelo",
    component: () =>
      import(
        "@/views/secure/modelo-franquicia/franquicias/modelos/ModelosFranquicia.vue"
      ),
  },
  {
    path: "/modelo-franquicia/franquicias/:franquicia/modelo/nuevo/",
    name: "modelo_franquicia_franquicias_franquicia_modelo_nuevo",
    component: () =>
      import(
        "@/views/secure/modelo-franquicia/franquicias/modelos/GestionModeloFranquicia.vue"
      ),
  },
  {
    path: "/modelo-franquicia/franquicias/:franquicia/modelo/edicion/:modelo/",
    name: "modelo_franquicia_franquicias_franquicia_modelo_edicion",
    component: () =>
      import(
        "@/views/secure/modelo-franquicia/franquicias/modelos/GestionModeloFranquicia.vue"
      ),
  },
  {
    path: "/modelo-franquicia/catalogo/negocios/",
    name: "modelo_franquicia_catalogos_negocios",
    component: () =>
      import(
        "@/views/secure/modelo-franquicia/catalogos/negocios/Negocios.vue"
      ),
  },
  {
    path: "/modelo-franquicia/catalogo/negocios/edicion/:negocio",
    name: "modelo_franquicia_catalogos_negocios_edicion",
    component: () =>
      import(
        "@/views/secure/modelo-franquicia/catalogos/negocios/GestionNegocio.vue"
      ),
  },
  {
    path: "/modelo-franquicia/catalogo/negocios/nuevo",
    name: "modelo_franquicia_catalogos_negocios_nuevo",
    component: () =>
      import(
        "@/views/secure/modelo-franquicia/catalogos/negocios/GestionNegocio.vue"
      ),
  },
  {
    path: "/infraestructura/info-recorridos/",
    name: "modelo_franquicia_info_recorridos",
    component: () =>
      import(
        "@/views/secure/infraestructura/info-recorridos/ListaRecorridos.vue"
      ),
  },
  {
    path: "/infraestructura/atencion-hallazgos/",
    name: "modelo_franquicia_atencion_hallazgos",
    component: () =>
      import(
        "@/views/secure/infraestructura/atencion-hallazgos/ListaRecorridosHallazgos.vue"
      ),
  },
  {
    path: "/infraestructura/atencion-hallazgos-calidad/",
    name: "modelo_franquicia_atencion_hallazgos_calidad",
    component: () =>
      import(
        "@/views/secure/infraestructura/atencion-hallazgos-certificacion/ListaRecorridosHallazgosCertificacion.vue"
      ),
  },
  {
    path: "/infraestructura/lista-hallazgos/:cecoFaseProyecto",
    name: "modelo_franquicia_lista_hallazgos",
    component: () =>
      import(
        "@/views/secure/infraestructura/atencion-hallazgos/ListaHallazgos.vue"
      ),
  },
  {
    path: "/infraestructura/lista-hallazgos-calidad/:cecoFaseProyecto",
    name: "modelo_franquicia_lista_hallazgos_calidad",
    component: () =>
      import(
        "@/views/secure/infraestructura/atencion-hallazgos-certificacion/ListaHallazgosCertificacion.vue"
      ),
  },
  {
    path: "/infraestructura/atencion-hallazgo/",
    name: "modelo_franquicia_atencion_hallazgo",
    component: () =>
      import(
        "@/views/secure/infraestructura/atencion-hallazgos/busqueda-atencion.vue"
      ),
  },
  {
    path: "/infraestructura/atencion-hallazgo-calidad/",
    name: "modelo_franquicia_atencion_hallazgo_calidad",
    component: () =>
      import(
        "@/views/secure/infraestructura/atencion-hallazgos-certificacion/busqueda-atencion-certificacion.vue"
      ),
  },
  {
    path: "/infraestructura/detalle-hallazgo/",
    name: "modelo_franquicia_detalle_hallazgo",
    component: () =>
      import(
        "@/views/secure/infraestructura/atencion-hallazgos/busqueda-detalle.vue"
      ),
  },
  {
    path: "/infraestructura/detalle-hallazgo-calidad/",
    name: "modelo_franquicia_detalle_hallazgo_calidad",
    component: () =>
      import(
        "@/views/secure/infraestructura/atencion-hallazgos-certificacion/busqueda-detalle-certificacion.vue"
      ),
  },
  {
    path: "/infraestructura/totales-hallazgos-negocio/:datosBusqueda",
    name: "modelo_franquicia_totales_hallazgos_negocio",
    component: () =>
      import(
        "@/views/secure/infraestructura/atencion-hallazgos/ListaHallazgosNegocio.vue"
      ),
  },
  {
    path: "/infraestructura/busqueda-hallazgos-negocio/",
    name: "modelo_franquicia_busqueda_hallazgos_negocio",
    component: () =>
      import(
        "@/views/secure/infraestructura/hallazgos-negocio/BusquedaHallazgosNegocio.vue"
      ),
  },
  {
    path: "/infraestructura/adm-asignaciones",
    name: "modelo_franquicia_asignacion_adm_asigna",
    component: () =>
      import("@/views/secure/infraestructura/asignaciones/Form_Asignacion.vue"),
  },
  {
    path: "/infraestructura/calendario-asignaciones-semana/:dataTransfer",
    name: "modelo_franquicia_calendario_semana",
    component: () =>
      import(
        "@/views/secure/infraestructura/asignaciones/AsignacionesSemana.vue"
      ),
  },
  {
    path: "/infraestructura/calendario-asignaciones-dia/:dataTransfer",
    name: "modelo_franquicia_calendario_diario",
    component: () =>
      import("@/views/secure/infraestructura/asignaciones/AsignacionesDia.vue"),
  },
  {
    path: "/infraestructura/calendario-asignaciones-infraestructura",
    name: "modelo_franquicia_calendario_infraestructura",
    component: () =>
      import(
        "@/views/secure/infraestructura/asignaciones/AdminAsignaciones.vue"
      ),
  },
  {
    path: "/test/gitlab",
    name: "test_gitlab",
    component: () => import("@/views/test/git-lab/Api.vue"),
  },
  {
    path: "/docview",
    name: "docview",
    component: () => import("@/views/webviewer/DocViewer.vue"),
  },
  {
    path: "/ws/test",
    name: "ws_test",
    component: () => import("@/views/SocketClient.vue"),
  },
  {
    path: "/v1/anonymus",
    name: "v1_anonymus",
    component: () => import("@/views/secure/anonymus/Anonymus.vue"),
  },
  {
    path: "/anonymus",
    name: "anonymus",
    component: () => import("@/views/secure/pro/Ide.vue"),
  },
  {
    path: "/rest/requests",
    name: "rest_requests",
    component: () => import("@/views/rest/Rest.vue"),
  },
  {
    path: "/modelo/entrega/informacion",
    name: "modelo_entrega_informacion",
    beforeEnter: () => {
      const user = serviceLocalStorage.userToken();
      window.location.href =
        varChecklist +
        "/central/seleccionFiltroExpansion.htm?idUsuario=" +
        user.empleado;
    },
  },
  {
    path: "/modelo/entrega/hallazgos",
    name: "modelo_entrega_hallazgos",
    beforeEnter: () => {
      window.location.href =
        varChecklist + "/central/seleccionSucursalHallazgos.htm";
    },
  },
  {
    path: "/accesos",
    name: "accesos",
    component: () => import("@/views/secure/accesos/Accesos.vue"),
  },
  {
    path: "/accesos/perfiles",
    name: "accesos_perfiles",
    component: () => import("@/views/secure/accesos/perfiles/Perfiles.vue"),
  },
  {
    path: "/accesos/perfiles/edicion/:perfil",
    name: "accesos_perfiles_edicion",
    component: () =>
      import("@/views/secure/accesos/perfiles/components/GestionPerfil.vue"),
  },
  {
    path: "/accesos/perfiles/nuevo",
    name: "accesos_perfiles_nuevo",
    component: () =>
      import("@/views/secure/accesos/perfiles/components/NuevoPerfil.vue"),
  },
  {
    path: "/accesos/sistema",
    name: "accesos_sistema",
    component: () => import("@/views/secure/accesos/sistema/Sistema.vue"),
  },
  {
    path: "/accesos/sistema/rutas",
    name: "accesos_sistema_rutas",
    component: () => import("@/views/secure/accesos/sistema/rutas/Rutas.vue"),
  },
  {
    path: "/accesos/sistema/rutas/nueva",
    name: "accesos_sistema_rutas_nueva",
    component: () =>
      import("@/views/secure/accesos/sistema/rutas/components/NuevaRuta.vue"),
  },
  {
    path: "/accesos/sistema/mapa-sitio",
    name: "accesos_sistema_mapa_sitio",
    component: () =>
      import("@/views/secure/accesos/sistema/mapa-sitio/MapaSitio.vue"),
  },
  {
    path: "/accesos/usuarios",
    name: "accesos_usuarios",
    component: () => import("@/views/secure/accesos/usuarios/Usuarios.vue"),
  },
  {
    path: "/accesos/usuarios/edicion/:usuario",
    name: "accesos_usuarios_edicion",
    component: () =>
      import("@/views/secure/accesos/usuarios/components/GestionUsuario.vue"),
  },
  {
    path: "/accesos/usuarios/nuevo",
    name: "accesos_usuarios_nuevo",
    component: () =>
      import("@/views/secure/accesos/usuarios/components/NuevoUsuario.vue"),
  },
  {
    path: "/privilegios",
    name: "privilegios",
    component: () => import("@/views/secure/privilegios/HomePrivilegios.vue"),
  },
  {
    path: "/privilegios/flujos",
    name: "privilegios_flujos",
    component: () => import("@/views/secure/privilegios/flujos/Flujos.vue"),
  },
  {
    path: "/privilegios/flujos/edicion/:flujo",
    name: "privilegios_flujos_edicion",
    component: () =>
      import("@/views/secure/privilegios/flujos/GestionFlujos.vue"),
  },
  {
    path: "/privilegios/flujos/nuevo",
    name: "privilegios_flujos_nuevo",
    component: () =>
      import("@/views/secure/privilegios/flujos/GestionFlujos.vue"),
  },
  {
    path: "/privilegios/flujos/componentes",
    name: "privilegios_flujos_componentes",
    component: () =>
      import(
        "@/views/secure/privilegios/catalogos/componentes/Componentes.vue"
      ),
  },
  {
    path: "/privilegios/flujos/componentes/edicion/:componente",
    name: "privilegios_flujos_componentes_edicion",
    component: () =>
      import(
        "@/views/secure/privilegios/catalogos/componentes/GestionComponentes.vue"
      ),
  },
  {
    path: "/privilegios/flujos/componentes/nuevo",
    name: "privilegios_flujos_componentes_nuevo",
    component: () =>
      import(
        "@/views/secure/privilegios/catalogos/componentes/GestionComponentes.vue"
      ),
  },
  {
    path: "/privilegios/flujos/modulos/:flujo",
    name: "privilegios_flujos_modulos",
    component: () =>
      import("@/views/secure/privilegios/flujos/modulos/Modulos.vue"),
  },
  {
    path: "/privilegios/flujos/modulos/:flujo/edicion/:modulo",
    name: "privilegios_flujos_modulos_edicion",
    component: () =>
      import("@/views/secure/privilegios/flujos/modulos/GestionModulos.vue"),
  },
  {
    path: "/privilegios/flujos/modulos/:flujo/nuevo",
    name: "privilegios_flujos_modulos_nuevo",
    component: () =>
      import("@/views/secure/privilegios/flujos/modulos/GestionModulos.vue"),
  },
  {
    path: "/privilegios/flujos/modulos/:flujo/submodulos/:modulo",
    name: "privilegios_flujos_modulos_submodulos",
    component: () =>
      import("@/views/secure/privilegios/flujos/modulos/SubModulos.vue"),
  },
  {
    path:
      "/privilegios/flujos/modulos/:flujo/submodulos/:modulo/edicion/:sub_modulo",
    name: "privilegios_flujos_modulos_submodulos_edicion",
    component: () =>
      import("@/views/secure/privilegios/flujos/modulos/GestionSubModulos.vue"),
  },
  {
    path: "/privilegios/flujos/modulos/:flujo/submodulos/:modulo/nuevo",
    name: "privilegios_flujos_modulos_submodulos_nuevo",
    component: () =>
      import("@/views/secure/privilegios/flujos/modulos/GestionSubModulos.vue"),
  },
  {
    path: "/privilegios/perfiles/configuracion",
    name: "privilegios_perfiles_configuracion",
    component: () =>
      import("@/views/secure/privilegios/perfiles/ConfiguracionPerfiles.vue"),
  },
  {
    path: "/privilegios/perfiles/edicion/:perfil",
    name: "privilegios_perfiles_edicion",
    component: () =>
      import("@/views/secure/privilegios/perfiles/GestionPerfiles.vue"),
  },
  {
    path: "/privilegios/perfiles/nuevo",
    name: "privilegios_perfiles_nuevo",
    component: () =>
      import("@/views/secure/privilegios/perfiles/GestionPerfiles.vue"),
  },
  {
    path: "/privilegios/flujos/perfil/:perfil",
    name: "privilegios_flujos_perfil",
    component: () =>
      import("@/views/secure/privilegios/perfiles/FlujosPerfil.vue"),
  },
  {
    path: "/privilegios/usuarios",
    name: "privilegios_usuario",
    component: () => import("@/views/secure/privilegios/usuarios/Usuarios.vue"),
  },
  {
    path: "/privilegios/usuarios/perfiles/:usuario",
    name: "privilegios_usuario_perfiles",
    component: () =>
      import("@/views/secure/privilegios/usuarios/PerfilesUsuario.vue"),
  },
  {
    path: "/privilegios/usuarios/perfiles/:usuario/personalizados",
    name: "privilegios_usuario_perfiles_personalizados",
    component: () =>
      import(
        "@/views/secure/privilegios/usuarios/PerfilUsuarioPersonalizado.vue"
      ),
  },
  {
    path: "/privilegios/usuarios/flujos/perfil/:usuario/:perfil",
    name: "privilegios_usuario_flujos_perfil",
    component: () =>
      import("@/views/secure/privilegios/usuarios/FlujosPerfilUsuario.vue"),
  },
  {
    path: "/programador/tareas",
    name: "programador_tareas",
    component: () => import("@/views/secure/scheduler/Scheduler.vue"),
  },
  {
    path: "/certificacion-calidad/director",
    name: "certificacion_calidad_director",
    component: () =>
      import(
        "@/views/secure/certificacion-calidad/HomeCertCalidadDirector.vue"
      ),
  },
  {
    path: "/certificacion-calidad/gerente",
    name: "certificacion_calidad_gerente",
    component: () =>
      import("@/views/secure/certificacion-calidad/HomeCertCalidadGerente.vue"),
  },

  {
    path: "/certificacion-calidad/director/dashboards/:negocio/:filtrosFechas",
    name: "certificacion_calidad_director_dashboards",
    component: () =>
      import("@/views/secure/certificacion-calidad/HomeDashboardDirector.vue"),
  },
  {
    path:
      "/certificacion-calidad/gerente/dashboards/:negocio/:filtrosFechas/:isDirector/:gerente",
    name: "certificacion_calidad_gerente_dashboards",
    component: () =>
      import("@/views/secure/certificacion-calidad/HomeDashboardGerente.vue"),
  },

  {
    path: "/certificacion-calidad/director/dashboard/:negocio/:filtrosFechas",
    name: "certificacion_calidad_director_dashboard",
    component: () =>
      import(
        "@/views/secure/certificacion-calidad/views/DashboardCertCalidadDirector.vue"
      ),
  },
  {
    path:
      "/certificacion-calidad/gerente/dashboard/:negocio/:filtrosFechas/:isDirector/:gerente",
    name: "certificacion_calidad_gerente_dashboard",
    component: () =>
      import(
        "@/views/secure/certificacion-calidad/views/DashboardCertCalidadGerente.vue"
      ),
  },
  {
    path: "/certificacion-calidad/punto-venta/ultima-certificacion/:puntoVenta",
    name: "certificacion_calidad_pv_ultima_certificacion",
    component: () =>
      import(
        "@/views/secure/certificacion-calidad/views/PuntoVentaUltCertificacion.vue"
      ),
  },
  {
    path: "/certificacion-calidad/punto-venta/detalle-certificacion/:visita",
    name: "certificacion_calidad_certificacion_detalle",
    component: () =>
      import(
        "@/views/secure/certificacion-calidad/views/PuntoVentaDetalleCertificacion.vue"
      ),
  },
  {
    path: "/certificacion-calidad/director/buscador",
    name: "certificacion_calidad_director_buscador",
    component: () =>
      import(
        "@/views/secure/certificacion-calidad/views/BuscadoresDirectorCertificacionCalidad.vue"
      ),
  },
  {
    path: "/certificacion-calidad/gerente/buscador",
    name: "certificacion_calidad_gerente_buscador",
    component: () =>
      import(
        "@/views/secure/certificacion-calidad/views/BuscadoresGerenteCertificacionCalidad.vue"
      ),
  },
  {
    path: "/certificacion-calidad/buscador/punto-venta",
    name: "certificacion_calidad_buscador_punto_venta",
    component: () =>
      import(
        "@/views/secure/certificacion-calidad/views/BuscadorPuntoVenta.vue"
      ),
  },
  {
    path: "/certificacion-calidad/buscador/gerentes",
    name: "certificacion_calidad_buscador_gerentes",
    component: () =>
      import("@/views/secure/certificacion-calidad/views/BuscadorGerentes.vue"),
  },
  {
    path: "/certificacion-calidad/buscador/certificadores",
    name: "certificacion_calidad_buscador_certificadores",
    component: () =>
      import(
        "@/views/secure/certificacion-calidad/views/BuscadorCertificadores.vue"
      ),
  },
  {
    path: "/certificacion-calidad/certificador/detalle/:certificador/:filtros",
    name: "certificacion_calidad_certificador_detalle",
    component: () =>
      import(
        "@/views/secure/certificacion-calidad/views/CertificadorCalidadDetalle.vue"
      ),
  },
  {
    path: "/certificacion-calidad/invitado",
    name: "certificacion_calidad_invitado",
    component: () =>
      import(
        "@/views/secure/certificacion-calidad/views/DashboardInvitado.vue"
      ),
  },
  {
    path: "/modelo-negocio",
    name: "modelo_negocio",
    component: () =>
      import("@/views/secure/modelo-negocio/HomeModeloNegocio.vue"),
  },
  {
    path: "/modelo-negocio/publicados",
    name: "modelo_negocio_publicados",
    component: () =>
      import(
        "@/views/secure/modelo-negocio/documentos/visualizador/folios/usuario/publicados/Publicados.vue"
      ),
  },
  {
    path: "/modelo-negocio/documentos",
    name: "modelo_negocio_documentos",
    component: () =>
      import("@/views/secure/modelo-negocio/documentos/HomeDocumentos.vue"),
  },
  {
    path: "/modelo-negocio/documentos/nuevo",
    name: "modelo_negocio_documentos_nuevo",
    component: () =>
      import(
        "@/views/secure/modelo-negocio/documentos/gestion-documento/GestionDocumento.vue"
      ),
  },
  {
    path: "/modelo-negocio/documentos/edicion/:folio",
    name: "modelo_negocio_documentos_edicion",
    component: () =>
      import(
        "@/views/secure/modelo-negocio/documentos/gestion-documento/GestionDocumento.vue"
      ),
  },
  {
    path: "/modelo-negocio/documentos/estados",
    name: "modelo_negocio_documentos_estados",
    component: () =>
      import(
        "@/views/secure/modelo-negocio/documentos/visualizador/folios/fechas/FechasFolios.vue"
      ),
  },
  {
    path: "/modelo-negocio/documentos/visualizador",
    name: "modelo_negocio_documentos_visualizador",
    component: () =>
      import(
        "@/views/secure/modelo-negocio/documentos/visualizador/folios/etiqueta-arbol/EtiquetaArbolFolios.vue"
      ),
  },
  {
    path: "/modelo-negocio/documentos/visualizar/resumen/folio/:folio",
    name: "modelo_negocio_documentos_visualizar_resumen_folio",
    component: () =>
      import(
        "@/views/secure/modelo-negocio/documentos/visualizador/Resumen.vue"
      ),
  },
  {
    path: "/modelo-negocio/perfilamiento",
    name: "modelo_negocio_perfilamiento",
    component: () => import("@/views/errors/Construyendo.vue"),
  },
  {
    path: "/modelo-negocio/bandeja/autorizacion",
    name: "modelo_negocio_bandeja_autorizacion",
    component: () =>
      import("@/views/secure/modelo-negocio/bandeja-autorizacion/Bandeja.vue"),
  },
  {
    path: "/modelo-negocio/bandeja/autorizacion/detalle/folio/:folio",
    name: "modelo_negocio_bandeja_autorizacion_detalle_folio",
    component: () =>
      import(
        "@/views/secure/modelo-negocio/documentos/visualizador/folios/usuario/autorizacion/components/Detalle.vue"
      ),
  },
  {
    path: "/modelo-negocio/publicados/comentarios/folio/:folio",
    name: "modelo_negocio_publicados_comentarios_folio",
    component: () =>
      import(
        "@/views/secure/modelo-negocio/documentos/visualizador/folios/usuario/publicados/components/Detalle.vue"
      ),
  },
  {
    path: "/modelo-negocio/accesos",
    name: "modelo_negocio_accesos",
    component: () =>
      import("@/views/secure/modelo-negocio/accesos/AccesosHome.vue"),
  },
  {
    path: "/modelo-negocio/accesos/grupos",
    name: "modelo_negocio_accesos_grupos",
    component: () =>
      import("@/views/secure/modelo-negocio/accesos/grupos/Grupos.vue"),
  },
  {
    path: "/modelo-negocio/accesos/grupos/edicion/:grupo",
    name: "modelo_negocio_accesos_grupos_edicion",
    component: () =>
      import(
        "@/views/secure/modelo-negocio/accesos/grupos/components/GestionGrupo.vue"
      ),
  },
  {
    path: "/modelo-negocio/accesos/grupos/nuevo/",
    name: "modelo_negocio_accesos_grupos_nuevo",
    component: () =>
      import(
        "@/views/secure/modelo-negocio/accesos/grupos/components/NuevoGrupo.vue"
      ),
  },
  {
    path: "/modelo-negocio/accesos/usuarios",
    name: "modelo_negocio_accesos_usuarios",
    component: () =>
      import("@/views/secure/modelo-negocio/accesos/usuarios/Usuarios.vue"),
  },
  {
    path: "/modelo-negocio/accesos/usuarios/edicion/:usuario",
    name: "modelo_negocio_accesos_usuarios_edicion",
    component: () =>
      import(
        "@/views/secure/modelo-negocio/accesos/usuarios/components/GestionUsuario.vue"
      ),
  },
  {
    path: "/modelo-franquicia/asignaciones/especiales",
    name: "modelo_franquicia_asignaciones_especiales",
    component: () =>
      import(
        "@/views/secure/modelo-franquicia/asignaciones/AsignacionEspecial.vue"
      ),
  },
  {
    path: "/modelo-franquicia/asignaciones/visor-asignacion",
    name: "modelo_franquicia_asignaciones_visor_asignacion",
    component: () =>
      import(
        "@/views/secure/modelo-franquicia/asignaciones/VisorAsignaciones.vue"
      ),
  },
  {
    path: "/modelo-franquicia/asignaciones/asignador/masivo/supervision",
    name: "modelo_franquicia_asignaciones_asignador_masivo_supervision",
    component: () =>
      import(
        "@/views/secure/modelo-franquicia/asignaciones/AsignadorMasivoSup"
      ),
  },
  {
    path: "/modelo-franquicia/asignaciones/home/asignacion",
    name: "modelo_franquicia_asignaciones_home_asignacion",
    component: () =>
      import(
        "@/views/secure/modelo-franquicia/asignaciones/HomeAsignacion.vue"
      ),
  },
  {
    path: "/modelo-franquicia/asignaciones/home/asignacion/director",
    name: "modelo_franquicia_home_asignaciones_director",
    component: () =>
      import(
        "@/views/secure/modelo-franquicia/asignaciones/HomeAsignacionDirector.vue"
      ),
  },
  //nuevas
  {
    path: "/supervision-mantenimiento/asignaciones/home",
    name: "supervision_mantenimiento_asignaciones_home",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/asignaciones/HomeSupervisionMtto.vue"
      ),
  },
  {
    path:
      "/supervision-mantenimiento/asignaciones/visor/asignaciones/mantenimiento",
    name:
      "supervision_mantenimiento_asignaciones_visor_asignaciones_mantenimiento",
    component: () =>
      import(
        "@/views/secure//supervision-mantenimiento/asignaciones/VisorAsignacionesMtto"
      ),
  },
  {
    path: "/supervision-mantenimiento/asignaciones",
    name: "supervision_mantenimiento_asignaciones",
    component: () =>
      import("@/views/secure/supervision-mantenimiento/Asignaciones.vue"),
  },
  {
    path: "/supervision-mantenimiento/asignacion-masiva",
    name: "supervision_mantenimiento_asignacion_masiva",
    component: () =>
      import("@/views/secure/supervision-mantenimiento/AsignacionMasiva.vue"),
  },
  {
    path:
      "/supervision-mantenimiento/asignaciones/reportes/asistencia-mantenimiento",
    name:
      "supervision_mantenimiento_asignaciones_reportes_asistencia_mantenimiento",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/asignaciones/ReporteAsistenciaMtto.vue"
      ),
  },
  {
    path:
      "/supervision-mantenimiento/asignaciones/reportes/asistencia-mantenimiento-lista",
    name:
      "supervision_mantenimiento_asignaciones_reportes_asistencia_mantenimiento_lista",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/asignaciones/ReporteAsistenciaMttoLista.vue"
      ),
  },
  //{SupMTO Director
  {
    path:
      "/supervision-mantenimiento/dashboard/director",
    name:
      "supervision_mantenimiento_dashboard_director",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/director/DashboardDirector.vue"
      ),
  },

  {
    path:
      "/supervision-mantenimiento/dashboard_regional/director/:parametros",
    name:
      "supervision_mantenimiento_dashboard_regional_director",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/director/DashboardRegional.vue"
      ),
  },

  {
    path: "/supervision-mantenimiento/dashboard/territorio/director/:parametros",
    name: "supervision_mantenimiento_dashboard_territorio_director",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/director/DashboardTerritorioDirector.vue"
      ),
  },

  {

    path: "/supervision-mantenimiento/dashboard_supervisor/director/:parametros",
    name: "supervision_mantenimiento_dasboard_supervisor_director",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/director/DashboardSupervisorDirector.vue"
      ),
  },

  {
    path:
      "/supervision-mantenimiento/dashboard/supervision_semanal/director",
    name:
      "supervision_mantenimiento_dashboard_supervision_semanal_director",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/director/DashboardSupervisionSemanalDirector.vue"
      ),
  },

  {
    path:
      "/supervision-mantenimiento/dashboard/supervision_semanal_detalle/director",
    name:
      "supervision_mantenimiento_dashboard_supervision_semanal_detalle_director",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/director/DashboardSupervisionSemanalDetalleDirector.vue"
      ),
  },

  {
    path:
      "/supervision-mantenimiento/asistencia-mantenimiento/director",
    name:
      "supervision_mantenimiento_asistencia_mantenimiento_director",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/director/ReporteAsistenciaMttoDirector.vue"
      ),
  },

  //}SupMTO Director 


  //{SupMTO Coordinador
  {
    path:
      "/supervision-mantenimiento/dashboard/coordinador",
    name:
      "supervision_mantenimiento_dashboard_coordinador",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/coordinador/DashboardCoordinador.vue"
      ),
  },

  {
    path: "/supervision-mantenimiento/dashboard/territorio/coordinador/:parametros",
    name: "supervision_mantenimiento_dashboard_territorio_coordinador",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/coordinador/DashboardTerritorioCoordinador.vue"
      ),
  },

  {

    path: "/supervision-mantenimiento/dashboard_supervisor/coordinador/:parametros",
    name: "supervision_mantenimiento_dasboard_supervisor_coordinador",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/coordinador/DashboardSupervisorCoordinador.vue"
      ),
  },

  {
    path:
      "/supervision-mantenimiento/dashboard/supervision_semanal/coordinador",
    name:
      "supervision_mantenimiento_dashboard_supervision_semanal_coordinador",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/coordinador/DashboardSupervisionSemanalCoordinador.vue"
      ),
  },

  {
    path:
      "/supervision-mantenimiento/dashboard/supervision_semanal_detalle/coordinador",
    name:
      "supervision_mantenimiento_dashboard_supervision_semanal_detalle_coordinador",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/coordinador/DashboardSupervisionSemanalDetalleCoordinador.vue"
      ),
  },

  {
    path:
      "/supervision-mantenimiento/asistencia-mantenimiento/coordinador",
    name:
      "supervision_mantenimiento_asistencia_mantenimiento_coordinador",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/coordinador/ReporteAsistenciaMttoCoordinador.vue"
      ),
  },

  //}SupMTO Coordinador 

  //{SupMTO Regional
  {
    path:
      "/supervision-mantenimiento/dashboard/regional",
    name:
      "supervision_mantenimiento_dashboard_regional",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/regional/DashboardRegional.vue"
      ),
  },

  {
    path: "/supervision-mantenimiento/dashboard/territorio/regional/:parametros",
    name: "supervision_mantenimiento_dashboard_territorio_regional",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/regional/DashboardTerritorioRegional.vue"
      ),
  },

  {

    path: "/supervision-mantenimiento/dashboard_supervisor/regional/:parametros",
    name: "supervision_mantenimiento_dasboard_supervisor_regional",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/regional/DashboardSupervisorRegional.vue"
      ),
  },

  {
    path:
      "/supervision-mantenimiento/dashboard/supervision_semanal/regional",
    name:
      "supervision_mantenimiento_dashboard_supervision_semanal_regional",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/regional/DashboardSupervisionSemanalRegional.vue"
      ),
  },
  {
    path:
      "/supervision-mantenimiento/dashboard/supervision_semanal_detalle/regional",
    name:
      "supervision_mantenimiento_dashboard_supervision_semanal_detalle_regional",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/regional/DashboardSupervisionSemanalDetalleRegional.vue"
      ),
  },

  {
    path:
      "/supervision-mantenimiento/asistencia-mantenimiento/regional",
    name:
      "supervision_mantenimiento_asistencia_mantenimiento_regional",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/regional/ReporteAsistenciaMttoRegional.vue"
      ),
  },

  //}SupMTO Regional 

  //{SupMTO Supervisor

  {

    path: "/supervision-mantenimiento/dashboard_supervisor/supervisor/",
    name: "supervision_mantenimiento_dasboard_supervisor_supervisor",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/supervisor/DashboardSupervisorSupervisor.vue"
      ),
  },

  {
    path:
      "/supervision-mantenimiento/dashboard/supervision_semanal/supervisor",
    name:
      "supervision_mantenimiento_dashboard_supervision_semanal_supervisor",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/supervisor/DashboardSupervisionSemanalSupervisor.vue"
      ),
  },

  {
    path:
      "/supervision-mantenimiento/dashboard/supervision_semanal_detalle/supervisor",
    name:
      "supervision_mantenimiento_dashboard_supervision_semanal_detalle_supervisor",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/supervisor/DashboardSupervisionSemanalDetalleSupervisor.vue"
      ),
  },

  {
    path:
      "/supervision-mantenimiento/asistencia-mantenimiento/supervisor",
    name:
      "supervision_mantenimiento_asistencia_mantenimiento_supervisor",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/supervisor/ReporteAsistenciaMttoSupervisor.vue"
      ),
  },

  //}SupMTO Supervisor

  {
    path:
      "/supervision-mantenimiento/dashboard/primer-nivel/mantenimiento-supervision",
    name:
      "supervision_mantenimiento_dashboard_primer_nivel_mantenimiento_supervision",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/dashboard/DashboardPrimerNivelMtoSup.vue"
      ),
  },
  {
    path: "/supervision-mantenimiento/dashboard/segundo-nivel/:parametros",
    name: "supervision_mantenimiento_dashboard_segundo_nivel",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/dashboard/DashboardSegundoNivelMtoSup.vue"
      ),
  },
  {
    path:
      "/supervision-mantenimiento/asignaciones/reportes/asistencia-mantenimiento-detalle",
    name:
      "supervision_mantenimiento_asignaciones_reportes_asistencia_mantenimiento_detalle",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/asignaciones/ReporteAsistenciaMttoDetalle.vue"
      ),
  },
  {
    path:
      "/supervision-mantenimiento/asignaciones/reportes/asistencia-mantenimiento-modulo",
    name:
      "supervision_mantenimiento_asignaciones_reportes_asistencia_mantenimiento_modulo",
    component: () =>
      import(
        "@/views/secure/supervision-mantenimiento/asignaciones/ReporteAsistenciaMttoModulo.vue"
      ),
  },
  {
    path: "/franquicia/detalle/visitas",
    name: "franquicia_detalle_visitas",
    beforeEnter: () => {
      const user = serviceLocalStorage.userToken();
      window.location.href =
        varChecklist + "/central/inicio.htm?idUsuario=" + user.empleado;
    },
  },
  {
    path: "/infraestructura/layouts/documento/nuevo",
    name: "infraestructura_layouts_documento_nuevo",
    component: () =>
      import(
        "@/views/secure/infraestructura/visualizador-documentos/AltaDocumento.vue"
      ),
  },
  {
    path: "/infraestructura/layouts/documento/actualiza",
    name: "infraestructura_layouts_actualiza_documento",
    component: () =>
      import(
        "@/views/secure/infraestructura/visualizador-documentos/ActualizaDocumento.vue"
      ),
  },
  {
    path: "/infraestructura/layouts/version/documentos",
    name: "infraestructura_layouts_version_documentos",
    component: () =>
      import(
        "@/views/secure/infraestructura/visualizador-documentos/listaVersiones.vue"
      ),
  },

  {
    path: "/infraestructura/layouts/documentos",
    name: "infraestructura_layouts_documentos",
    component: () =>
      import(
        "@/views/secure/infraestructura/visualizador-documentos/ListaArchivos.vue"
      ),
  },

  {
    path: "/infraestructura/layouts/documentos/geografia/negocio",
    name: "infraestructura_layouts_documentos_geografia_negocio",
    component: () =>
      import(
        "@/views/secure/infraestructura/visualizador-documentos/ListaArchivosGeografiayNegocio.vue"
      ),
  },
  {
    path: "/infraestructura/layouts/documentos/certificacion-calidad",
    name: "infraestructura_layouts_documentos_certificacion_calidad",
    component: () =>
      import(
        "@/views/secure/infraestructura/visualizador-documentos/ListaArchivosCertificadoresCalidad.vue"
      ),
  },
  {
    path: "/infraestructura/layouts/documentos/proyectos-construccion",
    name: "infraestructura_layouts_documentos_proyectos_construccion",
    component: () =>
      import(
        "@/views/secure/infraestructura/visualizador-documentos/ListaArchivosProyectosyCOnstruccion.vue"
      ),
  },

  {
    path: "/infraestructura/layouts/visualizador/pdf",
    name: "infraestructura_layouts_visualizador_pdf",
    component: () =>
      import(
        "@/views/secure/infraestructura/visualizador-documentos/VisualizadorPDF.vue"
      ),
  },
  {
    path: "/infraestructura/layouts/nueva-version/documento",
    name: "infraestructura_layouts_nueva_version_documento",
    component: () =>
      import(
        /* webpackChunkName: "about" */
        "@/views/secure/infraestructura/visualizador-documentos/NuevaVersionDocumento.vue"
      ),
  },
  {
    path: "/infraestructura/layouts/administrador/layouts",
    name: "infraestructura_layouts_administrador_layouts",
    component: () =>
      import(
        /* webpackChunkName: "about" */
        "@/views/secure/infraestructura/visualizador-documentos/HomeAdmLayout.vue"
      ),
  },
  {
    path: "/infraestructura/layouts/consulta/envios-documentos",
    name: "infraestructura_layouts_consulta_envio_documento",
    component: () =>
      import(
        /* webpackChunkName: "about" */
        "@/views/secure/infraestructura/visualizador-documentos/ConsultaEnvioDocumento.vue"
      ),
  },
];
const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [...routes, ...routesMaquetado],
});
router.beforeEach((to, _from, _next) => {
  if (to.name === "http_404") {
    store.state.notFound = true;
    store.state.component = "notFound404";
  } else {
    store.state.notFound = false;
  }
  _next();
});
export default router;
