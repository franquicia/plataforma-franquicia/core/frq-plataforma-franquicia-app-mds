import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import axios from "axios";
import vueAxios from "vue-axios";
import axiosTokenInterseptor from "@/axios/AxiosTokenInterseptor";
import JSONView from "vue-json-component";
import JsonViewer from "vue-json-viewer";
import VTooltip from "v-tooltip";
import draggable from "vuedraggable";
import * as VueWindow from "@hscmap/vue-window";
import Vuelidate from "vuelidate";
import vuetify from "./plugins/vuetify";
import "@charbytex/vue-quartz-cron/vue-quartz-cron.css";
import * as CronQuartz from "@charbytex/vue-quartz-cron";
import XLSX from "xlsx";
import VueFileAgent from "vue-file-agent";
import VueFileAgentStyles from "vue-file-agent/dist/vue-file-agent.css";
import { SlickList, SlickItem } from "vue-slicksort";
import panZoom from "vue-panzoom";
import LiquorTree from "liquor-tree";

Vue.use({ install: CronQuartz.install });
Vue.use(draggable);
Vue.use(JSONView);
Vue.use(JsonViewer);
Vue.use(VTooltip);
Vue.use(VueWindow);
Vue.use(Vuelidate);
Vue.use(vueAxios, axios, axiosTokenInterseptor);
Vue.use(XLSX);
Vue.use(VueFileAgent, VueFileAgentStyles);
Vue.use(panZoom);
Vue.use(LiquorTree);

Vue.component("draggable", draggable);
Vue.component("vfa-sortable-list", SlickList);
Vue.component("vfa-sortable-item", SlickItem);

const serviceCommons = store.state.serviceCommons;
axios.defaults.baseURL = store.state.urlServicesPlataformaFranquicia;

Vue.config.productionTip = false;

let onlyNumberFilters = (el, binding) => {
  let policies = [
    {
      name: "number",
    },
  ];
  if (binding.value !== undefined && binding.arg && binding.arg === "props") {
    if (typeof binding.value === "object") {
      if (binding.value.min !== undefined) {
        policies = [
          ...policies,
          {
            name: "min",
            value: binding.value.min,
          },
        ];
      }
      if (binding.value.max !== undefined) {
        policies = [
          ...policies,
          {
            name: "max",
            value: binding.value.max,
          },
        ];
      }
      if (binding.value.length !== undefined) {
        policies = [
          ...policies,
          {
            name: "length",
            value: binding.value.length,
          },
        ];
      }
    }
  }
  const elementInput = el.getElementsByTagName("input")[0];
  serviceCommons.onlyNumberFilters(elementInput, policies);
};
Vue.directive("number", {
  update(el, binding, vnode) {
    onlyNumberFilters(el, binding);
  },
  inserted(el, binding) {
    onlyNumberFilters(el, binding);
  },
});

Vue.directive("less-spaces", {
  inserted: function(el) {
    const elementInput = el.getElementsByTagName("input")[0];
    serviceCommons.lessSpaces(elementInput);
  },
});

Vue.directive("lower-case", {
  inserted: function(el) {
    const elementInput = el.getElementsByTagName("input")[0];
    serviceCommons.loweCase(elementInput);
  },
});

Vue.directive("number-decimal", {
  inserted: function(el) {
    const elementInput = el.getElementsByTagName("input")[0];
    serviceCommons.onlyNumberDecimal(elementInput);
  },
});

export default new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
