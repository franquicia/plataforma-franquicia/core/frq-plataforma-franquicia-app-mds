import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import Util from "@/services/commons/Util";
import LocalStorage from "@/services/storage/LocalStorage";
import endpoints from "/public/hosts.json";
const serviceCommons = Util.getService();
const serviceLocalStorage = LocalStorage.getService();
Vue.use(Vuex);
if (process.env.NODE_ENV !== "production") {
  let host = "10.89.85.170";
  endpoints["plataforma-franquicia-core"].protocol = "http";
  endpoints["plataforma-franquicia-core"].host = host;
  endpoints["plataforma-franquicia-core"].port = "8080";

  endpoints["plataforma-franquicia-app"].protocol = "http";
  endpoints["plataforma-franquicia-app"].host = host;
  endpoints["plataforma-franquicia-app"].port = "8080";

  endpoints["migestion"].protocol = "http";
  endpoints["migestion"].host = host;
  endpoints["migestion"].port = "8080";

  endpoints["checklist"].protocol = "http";
  endpoints["checklist"].host = host;
  endpoints["checklist"].port = "8080";
}
let coreProtocol = endpoints["plataforma-franquicia-core"].protocol;
let coreHost = endpoints["plataforma-franquicia-core"].host;
let corePort = endpoints["plataforma-franquicia-core"].port;

let appProtocol = endpoints["plataforma-franquicia-app"].protocol;
let appHost = endpoints["plataforma-franquicia-app"].host;
let appPort = endpoints["plataforma-franquicia-app"].port;

let gestionProtocol = endpoints["migestion"].protocol;
let gestionHost = endpoints["migestion"].host;
let gestionPort = endpoints["migestion"].port;

let checklistProtocol = endpoints["checklist"].protocol;
let checklistHost = endpoints["checklist"].host;
let checklistPort = endpoints["checklist"].port;

const appContextName = process.env.BASE_URL;
const urlServicesPlataformaFranquicia =
  (coreProtocol ? coreProtocol + "://" : "") +
  (coreHost ? coreHost : "") +
  (corePort ? ":" + corePort : "") +
  "/plataforma-franquicia-core/service";
const urlApiPlataformaFranquicia =
  (coreProtocol ? coreProtocol + "://" : "") +
  (coreHost ? coreHost : "") +
  (corePort ? ":" + corePort : "") +
  "/plataforma-franquicia-core/api-local/";
const urlWebSocketPlataformaFranquicia =
  (coreProtocol ? coreProtocol + "://" : "") +
  (coreHost ? coreHost : "") +
  (corePort ? ":" + corePort : "") +
  "/plataforma-franquicia-core/websocket";
const axiosGestion = axios.create({
  baseURL:
    (gestionProtocol ? gestionProtocol + "://" : "") +
    (gestionHost ? gestionHost : "") +
    (gestionPort ? ":" + gestionPort : "") +
    "/migestion",
});
const axiosChecklist = axios.create({
  baseURL:
    (checklistProtocol ? checklistProtocol + "://" : "") +
    (checklistHost ? checklistHost : "") +
    (checklistPort ? ":" + checklistPort : "") +
    "/checklist",
});
const axiosPFA = axios.create({
  baseURL:
    (appProtocol ? appProtocol + "://" : "") +
    (appHost ? appHost : "") +
    (appPort ? ":" + appPort : "") +
    appContextName +
    "service",
});
const axiosApiLocal = axios.create({
  baseURL: urlApiPlataformaFranquicia,
});

const periodoSupervisionMtto = {
  periodo: "trimestre",
  periodicidad: "trimestral",
  rango1: ["0101", "3103"],
  rango2: ["0104", "3006"],
  rango3: ["0107", "3009"],
  rango4: ["0110", "3112"],

  divisor: 3,
  index: 4,
  alPeriodo: false,
  alDia: true,
};
//valores1.filtros.datos[1]= "supervisiones programadas al "+periodo.periodo;
export default new Vuex.Store({
  state: {
    production: process.env.NODE_ENV === "production",
    baseUrl: process.env.BASE_URL,
    refCount: 0,
    isLogged: false,
    isLoading: false,
    isLoginSaml: false,
    dialogErrorServer: false,
    dialogSessionFinish: false,
    urlServicesPlataformaFranquicia,
    urlWebSocketPlataformaFranquicia,
    notFound: false,
    urlLogout: appContextName + "service/session/logout",
    serviceCommons,
    serviceLocalStorage,
    component: "ValidandoAccesos",
    modulos: [],
    modulosTree: [],
    perfiles: [],
    axiosGestion,
    axiosChecklist,
    axiosPFA,
    axiosApiLocal,
    urlApiPlataformaFranquicia,
    periodoSupervisionMtto,
  },
  mutations: {
    loading(state, isLoading) {
      if (isLoading) {
        state.refCount++;
        state.isLoading = true;
      } else if (state.refCount > 0) {
        state.refCount--;
        state.isLoading = state.refCount > 0;
      }
    },
  },
  actions: {},
  modules: {},
});
