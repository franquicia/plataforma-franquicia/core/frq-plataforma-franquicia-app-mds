'use strict'
let watermark = {}
let setWatermark = (canvasOriginal, str) => {
    let parentElement = canvasOriginal.parentElement;
    let heightCanvas = canvasOriginal.height;
    let widthCanvas =canvasOriginal.width;
    console.log("heightCanvas", heightCanvas, "widthCanvas", widthCanvas);
    let diagonal = Math.sqrt(Math.pow(heightCanvas,2) + Math.pow(widthCanvas, 2));
    var elem = parentElement.querySelector("[type=watermark]");
    if(elem){
        elem.remove();
    }
    var canvas = canvasOriginal.cloneNode(true);
    canvas.setAttribute("type" , "watermark");
    canvas.width=widthCanvas;
    canvas.height=heightCanvas;
    canvas.setAttribute("style","position:absolute; left:0; width: 100%;")
    console.log(canvas);
    var wmContext=canvas.getContext('2d');
    parentElement.appendChild(canvas, canvasOriginal);
    wmContext.globalAlpha=0.5;
    // height is font size
    var height = canvasOriginal.width /10;
    console.log("height",height);
    // setup text for filling
    wmContext.font = "1000% Arial" ;
    console.log("font", (diagonal/10)-120);
    wmContext.fillStyle = "grey";
    var metrics = wmContext.measureText(str);
    var widthText = metrics.width;
    console.log("widthText", widthText);
    console.log("diagonal", diagonal);
    // change the origin coordinate to the middle of the context
    wmContext.translate(canvas.width/2, canvas.height/2);
    // rotate the context (so it's rotated around its center)
    wmContext.rotate(-Math.atan(canvas.height/canvas.width));
    wmContext.textAlign="end";
    // as the origin is now at the center, just need to center the text
    wmContext.fillText(str,diagonal/2,height/2);
}
 // This method is only allowed to be called once
watermark.set = (el, str) => {
    setWatermark(el, str);
}
export default watermark