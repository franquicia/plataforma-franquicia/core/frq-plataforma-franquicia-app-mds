import colors from "vuetify/lib/util/colors";
const util = (function() {
  var jsonPath = require("jsonpath");
  var _service;
  function _getService() {
    if (!_service) {
      _service = this;
      return _service;
    }
    return _service;
  }
  function _btoaFromString(str) {
    return window.btoa(unescape(encodeURIComponent(str)));
  }
  function _btoaFromObject(object) {
    object = JSON.stringify(object);
    return window.btoa(unescape(encodeURIComponent(object)));
  }
  function _atobToObject(str) {
    return JSON.parse(decodeURIComponent(escape(window.atob(str))));
  }
  function _onlyNumber(input) {
    _inputFilter(input, _numberPositive);
  }
  function _onlyNumberDecimal(input) {
    _inputFilter(input, _floatNumber);
  }
  function _lessSpaces(input) {
    _changeInputFilter("_lessSpaces", input, _valueSpaces);
  }
  function _loweCase(input) {
    _changeInputFilter("_loweCase", input, _valueLoweCase);
  }
  function _onlyNumberFilters(input, policies) {
    let filters = [];
    policies.forEach((policy) => {
      let filter = function(value) {
        return true;
      };
      if (policy.name === "number") {
        filter = _numberPositive;
      }
      if (policy.name === "min") {
        filter = function(value) {
          return (
            /^\d*$/.test(value) &&
            (value === "" || parseInt(value) >= policy.value)
          );
        };
      }
      if (policy.name === "max") {
        filter = function(value) {
          return (
            /^\d*$/.test(value) &&
            (value === "" || parseInt(value) <= policy.value)
          );
        };
      }
      if (policy.name === "length") {
        filter = function(value) {
          return (
            /^\d*$/.test(value) &&
            (value === "" || value.length <= policy.value)
          );
        };
      }
      filters.push(filter);
    });
    input.filters = filters;
    _inputFilters(input);
  }
  function _mimeType(headerString) {
    let type;
    switch (headerString) {
      case "89504e47":
        type = "image/png";
        break;
      case "47494638":
        type = "image/gif";
        break;
      case "ffd8ffe0":
      case "ffd8ffe1":
      case "ffd8ffe2":
        type = "image/jpeg";
        break;
      default:
        type = "unknown";
        break;
    }
    return type;
  }
  function _getLogoImageByIdNegocio(idNegocio) {
    let image;
    if (idNegocio == 0) {
      image = require("@/assets/img/logoBancoAzteca.svg");
    }
    if (idNegocio == 1) {
      image = require("@/assets/img/logoBancoAzteca.svg");
    }
    if (idNegocio == 2) {
      image = require("@/assets/img/logoElektra.svg");
    }
    if (idNegocio == 3) {
      image = require("@/assets/img/logoPrestaPrenda.svg");
    }
    if (idNegocio == 4) {
      image = require("@/assets/img/logoGrupoElektra.svg");
    }
    if (idNegocio == 5) {
      image = require("@/assets/img/logoGCC.svg");
    }
    if (idNegocio == 6) {
      image = require("@/assets/img/logoCyF.svg");
    }
    if (idNegocio == 7) {
      image = require("@/assets/img/logoBancoAzteca.svg");
    }
    if (idNegocio == 9) {
      image = require("@/assets/img/logoRU.svg");
    }
    return image;
  }
  // Install input filters.
  function _number(value) {
    return /^-?\d*$/.test(value);
  }
  function _valueSpaces(value) {
    let valid = /\s/.test(value);
    return !valid;
  }
  function _valueLoweCase(value) {
    let valid = /^[a-z0-9_/-]+$/.test(value);
    return valid;
  }
  function _numberPositive(value) {
    return /^\d*$/.test(value);
  }
  function _floatNumber(value) {
    return /^\d*[.,]?\d*$/.test(value);
  }
  function _numberCurrency(value) {
    return /^-?\d*[.,]?\d{0,2}$/.test(value);
  }
  function _latinText(value) {
    return /^[a-z]*$/i.test(value);
  }
  function _hexText(value) {
    return /^[0-9a-f]*$/i.test(value);
  }
  function _inputFilters(target) {
    var custom_event = new Event("input", { bubbles: true });
    let handlerEvent = function() {
      let resultFilters = true;
      let self = this;
      target.filters.forEach((test) => {
        let val = test(self.value);
        resultFilters &= val;
      });
      if (resultFilters) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        this.dispatchEvent(custom_event);
      } else {
        this.value = "";
      }
    };
    if (!target.eventsRegistered) {
      [
        "input",
        "keydown",
        "keyup",
        "mousedown",
        "mouseup",
        "select",
        "contextmenu",
        "drop",
      ].forEach(function(event) {
        target.addEventListener(event, handlerEvent);
        target.eventsRegistered = true;
      });
    }
  }
  function _inputFilter(target, filter) {
    var custom_event = new Event("input", { bubbles: true });
    [
      "input",
      "keydown",
      "keyup",
      "mousedown",
      "mouseup",
      "select",
      "contextmenu",
      "drop",
    ].forEach(function(event) {
      target.addEventListener(event, function() {
        if (filter(this.value)) {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
          this.value = this.oldValue;
          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
          this.dispatchEvent(custom_event);
        } else {
          this.value = "";
        }
      });
    });
  }
  function _changeInputFilter(name, target, filter) {
    var custom_event = new Event("input", { bubbles: true });
    [
      "input",
      "keydown",
      "keyup",
      "mousedown",
      "mouseup",
      "select",
      "contextmenu",
      "drop",
      "focusout",
      "focusin",
    ].forEach(function(event) {
      target.addEventListener(event, function() {
        if (filter(this.value)) {
          this["oldValue" + name] = this.value;
          this["oldSelectionStart" + name] = this.selectionStart;
          this["oldSelectionEnd" + name] = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue" + name)) {
          this.value = this["oldValue" + name];
          this.setSelectionRange(
            this["oldSelectionStart" + name],
            this["oldSelectionEnd" + name]
          );
          this.dispatchEvent(custom_event);
        } else {
          this.value = "";
        }
      });
    });
  }
  function _remaningCharacters(value, total) {
    value = value ? "" + value : "";
    return total - value.length;
  }
  function _titleCase(str) {
    if (str) {
      var splitStr = str.toLowerCase().split(" ");
      for (var i = 0; i < splitStr.length; i++) {
        // You do not need to check if i is larger than splitStr length, as your for does that for you
        // Assign it back to the array
        splitStr[i] =
          splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
      }
      // Directly return the joined string
      return splitStr.join(" ");
    }
  }
  function _initialLetters(str) {
    var array1 = str.split(" ");
    var newarray1 = [];
    for (var x = 0; x < array1.length; x++) {
      newarray1.push(array1[x].charAt(0).toUpperCase());
    }
    return newarray1.join(" ");
  }
  function _getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(";");
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == " ") {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
  function _setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    const expires = "expires=" + d.toUTCString();
    let base_url = process.env.BASE_URL + "";
    base_url = base_url.replace("/", "").replace("/", "");
    document.cookie =
      cname + "=" + cvalue + ";" + expires + ";path=/" + base_url;
  }
  function _deleteCookie(cname) {
    let base_url = process.env.BASE_URL + "";
    base_url = base_url.replace("/", "").replace("/", "");
    document.cookie =
      cname +
      "= ; expires = Thu, 01 Jan 1970 00:00:00 GMT" +
      ";path=/" +
      base_url;
  }
  function _isUrlValid(str) {
    const regexp = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
    return regexp.test(str);
  }
  function _groupBy(key) {
    return function group(array) {
      return array.reduce((acc, obj) => {
        const property = jsonPath.query(obj, key);
        acc[property] = acc[property] || [];
        acc[property].push(obj);
        return acc;
      }, {});
    };
  }
  JSON.flatten = function(data) {
    var result = [];
    data.forEach(function(o) {
      var obj = {};
      for (var e in o) {
        Array.isArray(o[e])
          ? result.push(...JSON.flatten(o[e]))
          : (obj[e] = o[e]);
      }
      result.push(obj);
    });
    return result;
  };
  JSON.unflatten = function(data, attrId, attrParentId) {
    // create a name: node map
    var dataMap = data.reduce(function(map, node) {
      map[node[attrId]] = node;
      return map;
    }, {});
    // create the tree array
    var tree = [];
    data.forEach(function(node) {
      // add to parent
      var parent = dataMap[node[attrParentId]];
      if (parent) {
        // create child array if it doesn't exist
        (parent.children || (parent.children = [])).push(node);
        // add node to child array
      } else {
        // parent is null or missing
        tree.push(node);
      }
    });
    return tree;
  };
  JSON.ancestors = function(root, idFind) {
    var ancestors = [];
    const pathToTemp = (localArray, localTarget) => {
      var result;
      localArray.some(({ id, children = [] }) => {
        if (id === localTarget) {
          result = id;
          return result;
        }
        var temp = pathToTemp(children, localTarget);
        if (temp) {
          ancestors.push(id);
          result = id + "." + temp;
          return result;
        }
      });
      return result;
    };
    pathToTemp(root, idFind);
    return ancestors;
  };
  Array.unique = function(data) {
    var a = data.concat();
    for (var i = 0; i < a.length; ++i) {
      for (var j = i + 1; j < a.length; ++j) {
        if (a[i] === a[j]) a.splice(j--, 1);
      }
    }
    return a;
  };
  let getHexFromColor = function(color) {
    var colours = {
      aliceblue: "#f0f8ff",
      antiquewhite: "#faebd7",
      aqua: "#00ffff",
      aquamarine: "#7fffd4",
      azure: "#f0ffff",
      beige: "#f5f5dc",
      bisque: "#ffe4c4",
      black: "#000000",
      blanchedalmond: "#ffebcd",
      blue: "#0000ff",
      blueviolet: "#8a2be2",
      brown: "#a52a2a",
      burlywood: "#deb887",
      cadetblue: "#5f9ea0",
      chartreuse: "#7fff00",
      chocolate: "#d2691e",
      coral: "#ff7f50",
      cornflowerblue: "#6495ed",
      cornsilk: "#fff8dc",
      crimson: "#dc143c",
      cyan: "#00ffff",
      darkblue: "#00008b",
      darkcyan: "#008b8b",
      darkgoldenrod: "#b8860b",
      darkgray: "#a9a9a9",
      darkgreen: "#006400",
      darkkhaki: "#bdb76b",
      darkmagenta: "#8b008b",
      darkolivegreen: "#556b2f",
      darkorange: "#ff8c00",
      darkorchid: "#9932cc",
      darkred: "#8b0000",
      darksalmon: "#e9967a",
      darkseagreen: "#8fbc8f",
      darkslateblue: "#483d8b",
      darkslategray: "#2f4f4f",
      darkturquoise: "#00ced1",
      darkviolet: "#9400d3",
      deeppink: "#ff1493",
      deepskyblue: "#00bfff",
      dimgray: "#696969",
      dodgerblue: "#1e90ff",
      firebrick: "#b22222",
      floralwhite: "#fffaf0",
      forestgreen: "#228b22",
      fuchsia: "#ff00ff",
      gainsboro: "#dcdcdc",
      ghostwhite: "#f8f8ff",
      gold: "#ffd700",
      goldenrod: "#daa520",
      gray: "#808080",
      green: "#008000",
      greenyellow: "#adff2f",
      honeydew: "#f0fff0",
      hotpink: "#ff69b4",
      "indianred ": "#cd5c5c",
      indigo: "#4b0082",
      ivory: "#fffff0",
      khaki: "#f0e68c",
      lavender: "#e6e6fa",
      lavenderblush: "#fff0f5",
      lawngreen: "#7cfc00",
      lemonchiffon: "#fffacd",
      lightblue: "#add8e6",
      lightcoral: "#f08080",
      lightcyan: "#e0ffff",
      lightgoldenrodyellow: "#fafad2",
      lightgrey: "#d3d3d3",
      lightgreen: "#90ee90",
      lightpink: "#ffb6c1",
      lightsalmon: "#ffa07a",
      lightseagreen: "#20b2aa",
      lightskyblue: "#87cefa",
      lightslategray: "#778899",
      lightsteelblue: "#b0c4de",
      lightyellow: "#ffffe0",
      lime: "#00ff00",
      limegreen: "#32cd32",
      linen: "#faf0e6",
      magenta: "#ff00ff",
      maroon: "#800000",
      mediumaquamarine: "#66cdaa",
      mediumblue: "#0000cd",
      mediumorchid: "#ba55d3",
      mediumpurple: "#9370d8",
      mediumseagreen: "#3cb371",
      mediumslateblue: "#7b68ee",
      mediumspringgreen: "#00fa9a",
      mediumturquoise: "#48d1cc",
      mediumvioletred: "#c71585",
      midnightblue: "#191970",
      mintcream: "#f5fffa",
      mistyrose: "#ffe4e1",
      moccasin: "#ffe4b5",
      navajowhite: "#ffdead",
      navy: "#000080",
      oldlace: "#fdf5e6",
      olive: "#808000",
      olivedrab: "#6b8e23",
      orange: "#ffa500",
      orangered: "#ff4500",
      orchid: "#da70d6",
      palegoldenrod: "#eee8aa",
      palegreen: "#98fb98",
      paleturquoise: "#afeeee",
      palevioletred: "#d87093",
      papayawhip: "#ffefd5",
      peachpuff: "#ffdab9",
      peru: "#cd853f",
      pink: "#ffc0cb",
      plum: "#dda0dd",
      powderblue: "#b0e0e6",
      purple: "#800080",
      rebeccapurple: "#663399",
      red: "#ff0000",
      rosybrown: "#bc8f8f",
      royalblue: "#4169e1",
      saddlebrown: "#8b4513",
      salmon: "#fa8072",
      sandybrown: "#f4a460",
      seagreen: "#2e8b57",
      seashell: "#fff5ee",
      sienna: "#a0522d",
      silver: "#c0c0c0",
      skyblue: "#87ceeb",
      slateblue: "#6a5acd",
      slategray: "#708090",
      snow: "#fffafa",
      springgreen: "#00ff7f",
      steelblue: "#4682b4",
      tan: "#d2b48c",
      teal: "#008080",
      thistle: "#d8bfd8",
      tomato: "#ff6347",
      turquoise: "#40e0d0",
      violet: "#ee82ee",
      wheat: "#f5deb3",
      white: "#ffffff",
      whitesmoke: "#f5f5f5",
      yellow: "#ffff00",
      yellowgreen: "#9acd32",
    };
    if (typeof colours[color.toLowerCase()] != "undefined")
      return colours[color.toLowerCase()];
    return false;
  };
  function _getContrastColorByBaseColor(color) {
    let getColor = function(baseColor) {
      let hexcolor;
      if (!baseColor.startsWith("#") || !baseColor.startsWith("rgb")) {
        let vuetifyColorSplit = baseColor
          .trim()
          .replace("-", "")
          .split(" ");
        if (vuetifyColorSplit.length == 2) {
          hexcolor = colors[vuetifyColorSplit[0]][vuetifyColorSplit[1]];
        } else if (colors[vuetifyColorSplit[0]]) {
          hexcolor = colors[vuetifyColorSplit[0]]["base"];
        }
        if (!hexcolor) {
          hexcolor = getHexFromColor(baseColor);
        }
      }
      if (!hexcolor) {
        hexcolor = baseColor;
      }
      if (hexcolor.slice(0, 1) === "#") {
        hexcolor = hexcolor.slice(1);
      }
      if (hexcolor.length === 3) {
        hexcolor = hexcolor
          .split("")
          .map(function(hex) {
            return hex + hex;
          })
          .join("");
      }

      // Convert to RGB value
      var r = parseInt(hexcolor.substr(0, 2), 16);
      var g = parseInt(hexcolor.substr(2, 2), 16);
      var b = parseInt(hexcolor.substr(4, 2), 16);

      // Get YIQ ratio
      var yiq = (r * 299 + g * 587 + b * 114) / 1000;

      // Check contrast
      return yiq >= 128 ? "black" : "white";
    };
    return getColor(color);
  }
  function _getColorVuetifyByName(color) {
    let hexcolor;
    if (!color.startsWith("#") || !color.startsWith("rgb")) {
      let vuetifyColorSplit = color
        .trim()
        .replace("-", "")
        .split(" ");
      if (vuetifyColorSplit.length == 2) {
        hexcolor = colors[vuetifyColorSplit[0]][vuetifyColorSplit[1]];
      } else if (colors[vuetifyColorSplit[0]]) {
        hexcolor = colors[vuetifyColorSplit[0]]["base"];
      }
      if (!hexcolor) {
        hexcolor = getHexFromColor(color);
      }
    } else {
      hexcolor = color;
    }
    return hexcolor;
  }
  return {
    getService: _getService,
    stringToBase64: _btoaFromString,
    objectToBase64: _btoaFromObject,
    base64ToObject: _atobToObject,
    onlyNumber: _onlyNumber,
    lessSpaces: _lessSpaces,
    loweCase: _loweCase,
    onlyNumberFilters: _onlyNumberFilters,
    onlyNumberDecimal: _onlyNumberDecimal,
    remaningCharacters: _remaningCharacters,
    titleCase: _titleCase,
    initialLetters: _initialLetters,
    getCookie: _getCookie,
    setCookie: _setCookie,
    deleteCookie: _deleteCookie,
    mimeType: _mimeType,
    getLogoImageByIdNegocio: _getLogoImageByIdNegocio,
    isUrlValid: _isUrlValid,
    groupBy: _groupBy,
    getContrastColorByBaseColor: _getContrastColorByBaseColor,
    getColorVuetifyByName: _getColorVuetifyByName,
  };
})();
export default util;
