const LocalStorageService = (function() {
  var _service;
  function _getService() {
    if (!_service) {
      _service = this;
      return _service;
    }
    return _service;
  }
  function _setToken(tokenObj) {
    localStorage.setItem("access_token", tokenObj.access_token);
    localStorage.setItem("refresh_token", tokenObj.refresh_token);
    const user = {
      ceco: tokenObj.tokenObj,
      empleado: tokenObj.empleado,
      nombre: tokenObj.nombre
    };
    localStorage.setItem(
      "user",
      window.btoa(unescape(encodeURIComponent(JSON.stringify(user))))
    );
  }
  function _getAccessToken() {
    return localStorage.getItem("access_token");
  }
  function _getRefreshToken() {
    return localStorage.getItem("refresh_token");
  }
  function _getUserToken() {
    if (localStorage.getItem("user")) {
      return JSON.parse(
        decodeURIComponent(escape(window.atob(localStorage.getItem("user"))))
      );
    }
    return null;
  }
  function _clearToken() {
    localStorage.removeItem("access_token");
    localStorage.removeItem("refresh_token");
    localStorage.removeItem("user");
  }
  return {
    getService: _getService,
    setToken: _setToken,
    getAccessToken: _getAccessToken,
    getRefreshToken: _getRefreshToken,
    clearToken: _clearToken,
    userToken: _getUserToken
  };
})();
export default LocalStorageService;
