export default [
  {
    path: "/maquetado/disciplinas",
    name: "maquetado_disciplinas",
    component: () =>
      import(/* webpackChunkName: "about" */ "@/views/maquetado/Disciplinas.vue")
  },
  {
    path: "/maquetado/metodologia",
    name: "maquetado_metodologia",
    component: () =>
      import(/* webpackChunkName: "about" */ "@/views/maquetado/Metodologia.vue")
  },
  /*Hallazgos*/
  {
    path: "/maquetado/hallazgos/busqueda",
    name: "maquetado_hallazgos-busqueda",
    component: () =>
      import(/* webpackChunkName: "about" */ "@/views/maquetado/hallazgos/Busqueda.vue")
  },
  {
    path: "/maquetado/hallazgos/busqueda-resultados",
    name: "busqueda-resultados",
    component: () =>
      import(/* webpackChunkName: "about" */ "@/views/maquetado/hallazgos/busqueda-resultados.vue")
  },
  {
    path: "/maquetado/hallazgos/busqueda-resumen",
    name: "busqueda-resumen",
    component: () =>
      import(/* webpackChunkName: "about" */ "@/views/maquetado/hallazgos/busqueda-resumen.vue")
  },
  {
    path: "/maquetado/hallazgos/busqueda-detalle",
    name: "busqueda-detalle",
    component: () =>
      import(/* webpackChunkName: "about" */ "@/views/maquetado/hallazgos/busqueda-detalle.vue")
  },
  {
    path: "/maquetado/hallazgos/busqueda-atencion",
    name: "busqueda-atencion",
    component: () =>
      import(/* webpackChunkName: "about" */ "@/views/maquetado/hallazgos/busqueda-atencion.vue")
  },
  //{
    //path: "/maquetado/Region",
    //name: "maquetado_Region",
    //component: () =>
      //import(/* webpackChunkName: "about" */ "@/views/maquetado/Region.vue")
  //},
  
];
